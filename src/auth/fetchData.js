import axios from 'axios';
import getConfig from "next/config";
const InstanceApi  = async (url, token,method,payload) => {
  try {
    const { serverRuntimeConfig } = getConfig();
    if(method === "get"){
      const responses = await axios.get(`${process.env.NEXTAUTH_URL}/${url}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }).then((response)=> response.data).catch((val)=> val);
      return responses;
    }else if(method === "post"){
      const response = await axios.post(`${process.env.NEXTAUTH_URL}/${url}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },payload);
      return response.data;
    }else if(method === "put"){
      const response = await axios.put(`${process.env.NEXTAUTH_URL}/${url}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },payload);
      return response.data;
    }else{
     console.log("method not found!") 
    }
  } catch (error) {
    console.error('Error fetching data:', error);
    throw error;
  }

};

export default InstanceApi




