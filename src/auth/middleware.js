import jwt from "jsonwebtoken";
import getConfig from "next/config";

export default function withMiddleware(handler) {
  return async (req, res) => {
    const token = req.headers.authorization?.replace("Bearer ", "");
    // const { serverRuntimeConfig } = getConfig();
    try {
      await jwt.verify(token, process.env.secretKey, (err, decoded) => {
        if (err) {
          return res.status(401).json({ message: 'Invalid token' });
        }
        return handler(req, res);
      });
    } catch (error) {
      return res.status(500).json("Unauthorized");
    }
  };
}