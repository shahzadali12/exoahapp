import jwt from "jsonwebtoken";
// import getConfig from "next/config";

// export const { serverRuntimeConfig } = getConfig();

export const generateToken = async (payload) => {
  
  const token = await jwt.sign(payload, process.env.secretKey, {
    expiresIn: "15m",
  });
  return token;
};

// Function to save token in local storage
export const saveTokenToLocal = (token) => {

  context.res.setHeader('Set-Cookie', `token=${token}; Path=/; HttpOnly; Secure`);

  localStorage.setItem("ghq", token);
};

export const setCookie = (name, value, days) => {
  let expires = '';
  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = `; expires=${date.toUTCString()}`;
  }
  document.cookie = `${name}=${value || ''}${expires}; path=/`;
}
export const getCookie = (name)=> {
  const cookieValue = document.cookie.match(`(^|;)\\s*${name}\\s*=\\s*([^;]+)`);
  return cookieValue ? cookieValue.pop() : '';
}
export const getTokenFromLocal = (name) => {
  return localStorage.getItem(name);
};

export const verifyToken = async (token) => {
  try {
    const decodedToken = await jwt.verify(token,process.env.secretKey);
    return decodedToken;
  } catch (error) {
    console.error("Token verification failed:", error.message);
    return null;
  }
};

export function isTokenExpired(token) {
  try {
    const decoded = jwt.decode(token);
    if (!decoded || !decoded.exp) {
      // Token is invalid or doesn't have expiration
      return true;
    }

    // Check if the current time is after the expiration time
    return Date.now() >= decoded.exp * 1000;
  } catch (error) {
    console.error('Error decoding JWT:', error);
    return true; // Treat as expired if there's an error decoding
  }
}
