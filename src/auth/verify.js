// import jwt from 'jsonwebtoken';
// import next from "next";
// const secretKey = 'XH6bp/TkLvnUkQiPDEZNyHc0CV+VV5RL/n+HdVHoHN0=sh'; // Replace with your actual secret key

// export default async function jwtVerify(req, res) {
//   // Get the token from the request headers
//   const token = await req?.headers?.authorization?.split(' ')[1];

//   // Verify the token
//   if (token) {
//     await jwt.verify(token, secretKey, (err, decoded) => {
//       if (err) {
//         return res.status(404).json({ message: 'Token is expair' });;
//       }else{
//         next()
//       }
//     });
//   } else {
//     return res.status(401).json({ message: 'Token is not provided' });
//   }
// }


import jwt from 'jsonwebtoken';
import getConfig from 'next/config';

const { serverRuntimeConfig } = getConfig();
const secretKey = serverRuntimeConfig.secretKey;

export default function jwtVerify(req, res, next) {
  // Get the token from the request headers
  const token = req.headers.authorization?.split(' ')[1];

  // Verify the token
  if (token) {
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.status(403).json({ message: 'Token verification failed' });
      } else {
        // req.user = decoded;
        next(); // Move to the next middleware or route handler
      }
    });
  } else {
    return res.status(401).json({ message: 'Token is not provided' });
  }
}