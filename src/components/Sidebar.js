import DashboardIcon from "@mui/icons-material/Dashboard";
import GridViewIcon from "@mui/icons-material/GridView";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import Link from "next/link";
import { useSession } from "next-auth/react";
import { useContext } from "react";
import { Store } from "../store/AppStore";

function SideBAR() {
  const { data } = useSession();
  const { userdata } = useContext(Store);

  return (
    <nav className="pc-sidebar">
      <div className="navbar-wrapper">
        <div className="m-header">
          <Link href="/" className="b-brand">
            <img
              src="../assets/images/logo-dark.svg"
              alt=""
              className="logo logo-lg"
            />
          </Link>
        </div>
        <div className="navbar-content">
          <ul className="pc-navbar">
            {/* <li className="pc-item pc-caption">
              <label>Dashboard</label>
              <i className="ti"><DashboardIcon/></i>
            </li> */}
            <li className="pc-item">
              <Link href="/dashboard" className="pc-link ">
                <span className="pc-micon">
                  <i className="ti">
                    <GridViewIcon />
                  </i>
                </span>
                <span className="pc-mtext">Dashboard</span>
              </Link>
            </li>
            {data?.user?.role == "admin" ? (
              <li className="pc-item pc-hasmenu">
                <a className="pc-link">
                  <span className="pc-micon">
                    <i className="ti">
                      <SettingsOutlinedIcon />
                    </i>
                  </span>
                  <span className="pc-mtext">Operations</span>
                  <span className="pc-arrow">
                    <i className="ti">
                      <ChevronRightIcon />
                    </i>
                  </span>
                </a>
                <ul className="pc-submenu">
                  <li className="pc-item">
                    <Link className="pc-link" href="/users">
                      Registered Users
                    </Link>
                  </li>
                  {/* <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Creater Users
                  </Link>
                </li> */}
                  {/* <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    chats
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    E-Meetings
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Set Schedule
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Pricing
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Intro Video
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Reviews
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Support a Cause
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Connected Accounts
                  </Link>
                </li> */}
                  <hr />
                </ul>
              </li>
            ) : data?.user?.role == "user" ? (
              <li className="pc-item pc-hasmenu">
                
                <a className="pc-link">
                <span className="pc-micon">
                  <i className="ti">
                    <SettingsOutlinedIcon />
                  </i>
                </span>
                <span className="pc-mtext">Meet Celebrities</span>
                <span className="pc-arrow">
                  <i className="ti">
                    <ChevronRightIcon />
                  </i>
                </span>
              </a>
                <ul className="pc-submenu">
                  <>
                    <li className="pc-item">
                      <Link className="pc-link" href="/dashboard/mymeetings">
                        My Meetings
                      </Link>
                    </li>
                    <li className="pc-item">
                      <Link className="pc-link" href="/dashboard/chats">
                        Chat Rooms
                      </Link>
                    </li>
                    <li className="pc-item">
                      <Link className="pc-link" href="/users">
                        Transition history
                      </Link>
                    </li>
                  </>
                </ul>
              </li>
            ) : (
              <li className="pc-item pc-hasmenu">
                <a className="pc-link">
                  <span className="pc-micon">
                    <i className="ti">
                      <SettingsOutlinedIcon />
                    </i>
                  </span>
                  <span className="pc-mtext">Meet fans</span>
                  <span className="pc-arrow">
                    <i className="ti">
                      <ChevronRightIcon />
                    </i>
                  </span>
                </a>
                <ul className="pc-submenu">
                  <li className="pc-item">
                    <Link className="pc-link" href="/connect">
                      Connect With Zoom
                    </Link>
                  </li>
                  {/* {userdata?.zoom && ( */}
                    <>
                      {/* <li className="pc-item">
                        <Link className="pc-link" href="/rooms">
                          Meeting rooms
                        </Link>
                      </li> */}
                      {/* <li className="pc-item">
                      <Link className="pc-link" href="/dashboard/mymeetings">
                        My Meetings
                      </Link>
                      </li> */}
                      <li className="pc-item">
                      <Link className="pc-link" href="/dashboard/bookedmeetings">
                        Meetings with My Fan's
                      </Link>
                      </li>
                      <li className="pc-item">
                        <Link className="pc-link" href="/dashboard/chats">
                          Chat Rooms
                        </Link>
                      </li>
                      {/* <li className="pc-item">
                        <Link className="pc-link" href="/users">
                          Intro Video
                        </Link>
                      </li> */}
                      {/* <li className="pc-item">
                        <Link className="pc-link" href="/users">
                          Reviews
                        </Link>
                      </li>
                      <li className="pc-item">
                        <Link className="pc-link" href="/users">
                          Support a Cause
                        </Link>
                      </li> */}
                    </>
                  {/* )} */}
                </ul>
              </li>
            )}

            <li className="pc-item pc-hasmenu">
              <a className="pc-link">
                <span className="pc-micon">
                  <i className="ti">
                    <SettingsOutlinedIcon />
                  </i>
                </span>
                <span className="pc-mtext">Settings</span>
                <span className="pc-arrow">
                  <i className="ti">
                    <ChevronRightIcon />
                  </i>
                </span>
              </a>
              <ul className="pc-submenu">
                {/* <li className="pc-item">
                      <Link className="pc-link" href="/users">
                        Switch to Fan
                      </Link>
                    </li>
                    <li className="pc-item">
                      <Link className="pc-link" href="/users">
                        Notifications
                      </Link>
                    </li> */}
                <li className="pc-item">
                  <Link className="pc-link" href="/profile">
                    Edit profile
                  </Link>
                </li>
                <li className="pc-item">
                  <Link className="pc-link" href="/users">
                    Sign out
                  </Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
export default SideBAR;
