
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

function ForgetPassword() {
  // const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <div className="auth-main">
      <div className="auth-wrapper v3">
        <div className="auth-form">
          <div className="card my-5">
            <div className="card-body">
              <a href="#" className="d-flex justify-content-center">
                <img src={logo.src} />
              </a>
              <div className="row">
                <div className="d-flex justify-content-center">
                  <div className="auth-header">
                    <h2 className="text-secondary mt-4">
                      <b>Hi, Welcome Back</b>
                    </h2>
                    <p className="f-16 mt-2">Enter your credentials to continue</p>
                  </div>
                </div>
              </div>
              {/* <div className="d-grid">
                <button type="button" className="btn mt-2 btn-light-primary bg-light text-muted">
                  <img src="../assets/images/authentication/google-icon.svg" />Sign In With Google
                </button>
              </div>
              <div className="saprator mt-3">
                <span>or</span>
              </div> */}
              {/* <h5 className="my-4 d-flex justify-content-center">Sign in with Email address</h5> */}
              <div className="form-floating mb-3">
                <input
                  type="email"
                  className="form-control"
                  id="floatingInput"
                  placeholder="Email address / Username"
                />
                <label htmlFor="floatingInput">Email address / Username</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  type="email"
                  className="form-control"
                  id="floatingInput"
                  placeholder="Password"
                />
                <label htmlFor="floatingInput">Password</label>
              </div>
              <div className="d-flex mt-1 item-center justify-content-between">
                <FormControlLabel
                  control={<Checkbox name="Remember me" />}
                  label="Remember me"
                />
                <FormControlLabel
                  control={<h5 className="text-secondary">Forgot Password?</h5>}
                />
              </div>
              <div className="d-grid mt-4">
                <Stack>
                  <Button
                    variant="text"
                    type="button"
                    className="btn bg-secondary text-white"
                  >
                    Sign In
                  </Button>
                </Stack>
              </div>
              <hr />
              <h5 className="d-flex justify-content-center">
                Don't have an account?
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ForgetPassword;
