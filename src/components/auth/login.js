import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import { toast } from "react-toastify";
import { useForm, SubmitHandler } from "react-hook-form";
import { schemas } from "../schemas/login";
import { yupResolver } from "@hookform/resolvers/yup";
import { getSession, useSession, signIn } from "next-auth/react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import GoogleIcon from "@mui/icons-material/Google";
import HeaderLayout from "../home/header";
import MainLayout from "../../layout/main";
import HeaderSimple from "../wegets/header";
import FooterLayout from "../home/footer";
import LoginComponent from "./loginCom";

function Login() {
  const [loading, setLoading] = useState(false);
  const [SmallDevices, setScreen] = useState(false);
  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);
 

 

  return (
    <>
      <HeaderSimple />

      <div className="content_area">
        <div className="container">
          <div
            className={
              SmallDevices
                ? "flex justify-around items-center py-3 flex-col"
                : "flex justify-around items-center px-5"
            }
          >
            {SmallDevices ? (
              ""
            ) : (
              <div className="w-3/5">
                <img
                  className="h-full w-[50%] px-30"
                  src="../daigrm.png"
                  alt="logo here"
                />
              </div>
            )}
           <LoginComponent/>
          </div>
        </div>
      </div>

      <FooterLayout p="pt-5" />
    </>
    // <div className="auth-main">
    //   <div className="auth-wrapper v3">
    //     <div className="auth-form">
    //       <div className="card my-5">
    //         <div className="card-body">
    //           <a href="#" className="d-flex justify-content-center">
    //             <img src={logo.src} />
    //           </a>
    //           <div className="row">
    //             <div className="d-flex justify-content-center">
    //               <div className="auth-header">
    //                 <h2 className="text-secondary mt-4">
    //                   <b>Hi, Welcome Back</b>
    //                 </h2>
    //                 <p className="f-16 mt-2">
    //                   Enter your credentials to continue
    //                 </p>
    //               </div>
    //             </div>
    //           </div>
    //           <div className="d-grid">
    //             <button type="button" onClick={LoginWithGoogle} className="btn mt-2 flex justify-center items-center btn bg-orange-500 hover:bg-orange-600 text-white">
    //               <GoogleIcon className="mr-2"/>Sign In With Google
    //             </button>
    //           </div>
    //           <div className="saprator mt-3">
    //             <span>or</span>
    //           </div>
    //           {/* <h5 className="my-4 d-flex justify-content-center">Sign in with Email address</h5> */}
    //           <form onSubmit={handleSubmit(onSubmit)}>
    //             <div className="form-floating mb-3">
    //               <input
    //                 type="email"
    //                 className="form-control"
    //                 id="floatingInput"
    //                 {...register("email", { required: true })}
    //                 placeholder="Email address / Username"
    //               />
    //               <label htmlFor="floatingInput">
    //                 Email address / Username
    //               </label>
    //             </div>
    //             <div className="form-floating mb-3">
    //               <input
    //                 type="password"
    //                 className="form-control"
    //                 id="floatingInput"
    //                 {...register("password", { required: true })}
    //                 placeholder="Password"
    //               />
    //               <label htmlFor="floatingInput">Password</label>
    //             </div>
    //             <div className="d-flex mt-1 item-center justify-content-between">
    //               <FormControlLabel
    //                 control={<Checkbox onChange={(e)=> setValue("remember", e.target.checked)} />}
    //                 label="Remember me"
    //               />
    //               <FormControlLabel
    //                 control={
    //                   <h5 className="text-secondary">Forgot Password?</h5>
    //                 }
    //               />
    //             </div>
    //             <div className="d-grid mt-4">

    //               <Stack>
    //                 {loading ? <div className="flex justify-center items-center"><CircularProgress /></div> : <Button
    //                   variant="text"
    //                   type="submit"
    //                   disabled={loading}
    //                   className="btn bg-secondary text-white"
    //                 >
    //                   Sign In
    //                 </Button> }
    //               </Stack>
    //             </div>
    //           </form>
    //           <hr />
    //           <h5 className="d-flex justify-content-center">
    //             <Link href="/signup">Don't have an account?</Link>
    //           </h5>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // </div>
  );
}

export default Login;


