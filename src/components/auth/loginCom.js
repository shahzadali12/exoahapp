import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";
import { useForm, SubmitHandler } from "react-hook-form";
import { schemas } from "../schemas/login";
import { yupResolver } from "@hookform/resolvers/yup";
import { getSession, useSession, signIn } from "next-auth/react";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import { useState, useEffect, useContext } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Store } from "../../store/AppStore";


function LoginComponent({full}) {
    const [loading, setLoading] = useState(false);
    const [SmallDevices, setScreen] = useState(false);
    useEffect(() => {
      setScreen(window.innerWidth > 768 ? false : true);
    }, []);
   
    const {setlogins} = useContext(Store)
  
  
    const {
        handleSubmit,
        register,
        reset,
        setValue,
        control,
        watch,
        getValues,
        setError,
        formState: { errors },
      } = useForm({
        resolver: yupResolver(schemas),
        mode: "onChange",
      });
    
      const route = useRouter();

      const getlocation = route.pathname == "/login" ? "/dashboard/" : route.pathname;

      const onSubmit = async (data) => {
        setLoading(true);
        const responce = await signIn("credentials", {
          redirect: false,
          email: data.email,
          password: data.password,
        });
    
        const Error = responce.error && responce.error && await JSON.parse(responce.error && responce.error);
        // setScreen_error(Error);
    
       
        
    
        if (responce?.status === 200) {
        
          if(route.pathname == "/login"){
            route.push(getlocation);
            setLoading(false);
            toast.success("Login success");
            setlogins(false)
          }
          setlogins(false)
          setLoading(false);
        //   route.push("/dashboard");
          toast.success("Login success");
        } else if (Error.status === 404) {
          toast.error(Error.message);
          route.push("/signup");
          setLoading(false);
        } else if (Error.status === 401) {
          toast.error(Error.message);
          setLoading(false);
        } else {
          setLoading(false);
          toast.error(Error.message);
          route.push("/signup");
        }
      };
      const LoginWithGoogle = async () => {
        await signIn("google");
      };
  return (

   
    <div className={SmallDevices ? "w-full" : full ? "w-full" : "w-2/5"}>
    <h2 className="text-center font-bold text-4xl mb-4">
      Welcome Back!
    </h2>
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="inputFelids mb-3">
        <input
          type="email"
          className="form-control"
          id="floatingInput"
          {...register("email", { required: true })}
          placeholder="Enter Username"
        />
      </div>
      <div className="inputFelids mb-3">
        <input
          type="password"
          className="form-control"
          id="pass"
          {...register("password", { required: true })}
          placeholder="Enter password"
        />
      </div>
      <div className="reset float-left w-full mb-4">
        <Link href="/" className="float-right text-gray-900 text-lg">
          Forget Password?
        </Link>
      </div>
      <div className="float-left w-full">
        <Stack>
          {loading ? (
            <div className="flex justify-center items-center">
              <CircularProgress />
            </div>
          ) : (
            <Button
              variant="text"
              type="submit"
              disabled={loading}
              className="btn bg-secondary text-white font-bold py-2"
            >
              Sign In
            </Button>
          )}
        </Stack>
      </div>
      <div className="saprator_list text-center mb-1">
        <span className="">or log in with</span>
      </div>
      <ul className="flex justify-between items-center p-0">
        <li>
          <button
            type="button"
            onClick={LoginWithGoogle}
            className="bg-[#e62b32] text-white py-2 text-xl px-4 rounded-md"
          >
            <i className="fa fa-google" aria-hidden="true"></i>
          </button>
        </li>
        <li>
          <button
            type="button"
            onClick={LoginWithGoogle}
            className="bg-[#6081c4] text-white py-2 text-xl px-4 rounded-md"
          >
            <i className="fa fa-facebook" aria-hidden="true"></i>
          </button>
        </li>
        <li>
          <button
            type="button"
            onClick={LoginWithGoogle}
            className="bg-black text-white py-2 text-xl px-4 rounded-md"
          >
            <i className="fa fa-apple" aria-hidden="true"></i>
          </button>
        </li>
      </ul>

      <h5 className="text-base font-light text-center mb-0">
        Already Have An Account?{" "}
        <Link href="/signup" className="font-extrabold text-black">
          signup
        </Link>
      </h5>
    </form>
  </div>
  )
}

export default LoginComponent