import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

import { toast } from "react-toastify";

import { schemas } from "../schemas/signup";
import { yupResolver } from "@hookform/resolvers/yup";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";


import { useForm, Controller } from "react-hook-form";

import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import GoogleIcon from '@mui/icons-material/Google';
import { signIn } from "next-auth/react";

function Register() {
  // const label = { inputProps: { "aria-label": "Checkbox demo" } };
  const [loading, setLoading] = useState(false);
  const [role, setRole] = useState(true);
  const [selectionRole, setselectionRole] = useState(false);
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const watchItem = watch();

  const route = useRouter();
  const onSubmit = async (data) => {
    setLoading(true);

    const regis = {
      profilePhoto: "",
      password: data.password,
      role: data?.role,
      phone: '',
      email: data.email,
      name: data.name,
    };
    const response = await fetch(`/api/authenticated/login/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(regis),
    });

    if (response.status === 201) {
      setLoading(false);
      // SetTimerButton(true);
      route.push("/login");
      toast.success("Congratulations your account has been created!");
    } else if (response.status === 409) {
      toast.error("This Email account already exist!");
      // SetTimerButton(true);
      // route.push("/login");
      setLoading(false);
    } else {
      toast.error("Something was wrong");
      // SetTimerButton(true);
      route.push("/login");
    }
  };
  const LoginWithGoogle = async () => {
    await signIn("google"); 
  }

  return (
    <div className="auth-main">
      <div className="auth-wrapper v3">
        <div className="auth-form">
          <div className="card mt-5">
            {watchItem?.role && (
              <button
                type="button"
                onClick={() => setValue("role", "")}
                className="mr-2 text-black absolute left-2 top-4"
              >
                <ArrowBackIosNewIcon />
                Back to menu
              </button>
            )}
            <div className="card-body">
              <a href="#" className="d-flex justify-content-center mt-3">
                <img src={logo.src} />
              </a>

              <div className="row">
                <div className="d-flex justify-content-center">
                  <div className="auth-header">
                    <h2 className="text-secondary mt-4">
                      <b>
                        Sign up{" "}
                        {!watchItem?.role
                          ? "as a ?"
                          : `as a ${watchItem?.role}`}
                      </b>
                    </h2>
                    <p className="f-16 mt-2">
                      {role ? null : "Enter your credentials to continue"}
                    </p>
                  </div>
                </div>
              </div>
             {!watchItem?.role ? null : <div className="d-grid mb-3">
                <button type="button" onClick={LoginWithGoogle} className="btn mt-2 flex justify-center items-center btn bg-orange-500 hover:bg-orange-600 text-white">
                  <GoogleIcon className="mr-2"/>Sign Up With Google
                </button>
              </div>}
              <div className="saprator mt-3">
                <span>or</span>
              </div>
              <form onSubmit={handleSubmit(onSubmit)}>
                {!watchItem?.role ? (
                  <div className="flex justify-between items-center">
                    <div className="flex items-center  border border-gray-200 rounded dark:border-gray-700 style-rdio">
                      <input
                        id="bordered-radio-1"
                        type="radio"
                        value="creator"
                        {...register("role", { required: true })}
                        // name="bordered-radio"
                        className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500  dark:ring-offset-gray-800 focus:rounded-full focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                      <label
                        htmlFor="bordered-radio-1"
                        className="w-full text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                      >
                        <div className="flex justify-start items-center text-white">
                          <img
                            className="w-14 invert grayscale"
                            src="../icons/vip.png"
                            alt="img here"
                          />
                          <span>Creator</span>
                        </div>
                      </label>
                    </div>
                    <div className="flex items-center border border-gray-200 rounded dark:border-gray-700 style-rdio">
                      <input
                        id="bordered-radio-2"
                        type="radio"
                        value="user"
                        // name="bordered-radio"
                        {...register("role", { required: true })}
                        className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                      />
                      <label
                        htmlFor="bordered-radio-2"
                        className="w-full  text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                      >
                        <div className="flex justify-start items-center text-white">
                          <img
                            className="w-14 invert grayscale"
                            src="../icons/user-account.png"
                            alt="img here"
                          />
                          <span>User</span>
                        </div>
                      </label>
                    </div>
                  </div>
                ) : (
                  <>
                    <div className="row">
                      <div className="col-md-12">
                        <div
                          className={`form-floating mb-3 ${
                            errors.name ? "error" : ""
                          }`}
                        >
                          <input
                            type="text"
                            className="form-control"
                            id="floatingInput"
                            {...register("name", { required: true })}
                            placeholder="First Name"
                          />
                          <label htmlFor="floatingInput">Name</label>
                        </div>
                      </div>

                    </div>
                    <div
                      className={`form-floating mb-3 ${
                        errors.email ? "error" : ""
                      }`}
                    >
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInput"
                        {...register("email", { required: true })}
                        placeholder="Email Address / Username"
                      />
                      <label htmlFor="floatingInput">Email Address </label>
                    </div>

                    <div
                      className={`form-floating mb-3 ${
                        errors.password ? "error" : ""
                      }`}
                    >
                      <input
                        type="password"
                        className="form-control"
                        id="floatingInput"
                        {...register("password", { required: true })}
                        placeholder="Password"
                      />
                      <label htmlFor="floatingInput">Password</label>
                    </div>
                    
                    {/* <div className="form-check mt-3s"> */}
                    <FormControlLabel
                      control={
                        <Checkbox
                          onChange={(e) => setValue("agree", e.target.checked)}
                        />
                      }
                      label="Agree with Terms & Condition."
                    />

                    <div className="d-grid mt-4">
                      <Stack>
                        {loading ? (
                          <div className="flex justify-center items-center">
                            <CircularProgress />
                          </div>
                        ) : (
                          <Button
                            variant="text"
                            type="submit"
                            disabled={loading}
                            className="btn bg-secondary text-white"
                          >
                            Sign Up
                          </Button>
                        )}
                      </Stack>
                    </div>
                  </>
                )}
              </form>
              <hr />
              <h5 className="d-flex justify-content-center">
                <Link href="/login">Already have an account?</Link>
              </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
