import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

import { toast } from "react-toastify";

import { schemas } from "../schemas/signup";
import { yupResolver } from "@hookform/resolvers/yup";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";

import { useForm, Controller } from "react-hook-form";

import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import GoogleIcon from "@mui/icons-material/Google";
import { signIn } from "next-auth/react";
import HeaderSimple from "../wegets/header";
import FooterLayout from "../home/footer";
import InstanceApi from "../../auth/fetchData"
import { getCookie } from "../../auth/token";
function Register() {
  // const label = { inputProps: { "aria-label": "Checkbox demo" } };
  const [loading, setLoading] = useState(false);
  const [role, setRole] = useState(true);
  const [selectionRole, setselectionRole] = useState(false);

  const [SmallDevices, setScreen] = useState(false);
  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);

  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const watchItem = watch();

  const route = useRouter();
  const onSubmit = async (data) => {
    setLoading(true);

    const regis = {
      info: {
        password: data?.password,
        role: data?.role,
        email: data?.email,
        name: data?.name,
      }
    }
    const token = await getCookie("ghq")
    // url, token,method,payload
    const response = await InstanceApi(`/api/authenticated/login/signup`, token, "post",regis)
    // const response = await fetch(`/api/authenticated/login/signup`, {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify(regis),
    // });

    if (response.status === 201) {
      setLoading(false);
      // SetTimerButton(true);
      route.push("/login");
      toast.success("Congratulations your account has been created!");
    } else if (response.status === 409) {
      toast.error("This Email account already exist!");
      // SetTimerButton(true);
      // route.push("/login");
      setLoading(false);
    } else {
      toast.error("Something was wrong");
      // SetTimerButton(true);
      route.push("/login");
    }
  };
  const LoginWithGoogle = async () => {
    await signIn("google");
  };

  return (
    <>
      <HeaderSimple />

      <div className="content_area">
        <div className="container">
          <div
            className={
              SmallDevices
                ? "flex justify-around items-center py-3 flex-col"
                : "flex justify-around items-center px-5"
            }
          >
            {SmallDevices ? (
              ""
            ) : (
              <div className="w-3/5">
                <img
                  className="h-full w-[50%] px-30"
                  src="../daigrm3.png"
                  alt="logo here"
                />
              </div>
            )}
            <div className={SmallDevices ? "w-full" : "w-2/5"}>
              <h2
                className={
                  SmallDevices
                    ? "text-center font-bold text-2xl mb-4 relative"
                    : "text-center font-bold text-4xl mb-4 relative"
                }
              >
                {watchItem?.role && (
                  <>
                    <button
                      type="button"
                      onClick={() => setValue("role", "")}
                      className="mr-2 text-black absolute left-0 text-base top-1/2 -mt-3"
                    >
                      <ArrowBackIosNewIcon /> Back
                    </button>
                    Welcome {watchItem?.role}
                  </>
                )}
              </h2>
              <form onSubmit={handleSubmit(onSubmit)}>
                {!watchItem?.role ? (
                  <div className="float-left h-[40vh] w-full">
                    <h4 className="text-center font-bold text-2xl mb-4 relative w-full">
                      Welcome
                    </h4>
                    <div className="flex justify-between items-center w-full">
                      <div className="flex items-center  border border-gray-200 rounded dark:border-gray-700 style-rdio">
                        <input
                          id="bordered-radio-1"
                          type="radio"
                          value="creator"
                          {...register("role", { required: true })}
                          // name="bordered-radio"
                          className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500  dark:ring-offset-gray-800 focus:rounded-full focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                        />
                        <label
                          htmlFor="bordered-radio-1"
                          className="w-full text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                        >
                          <div className="flex justify-start items-center text-white">
                            <img
                              className="w-14 invert grayscale"
                              src={process.env.NODE_ENV === "development" ? "../icons/vip.png" : "../public/icons/vip.png"}
                              alt="img here"
                            />
                            <span>Creator</span>
                          </div>
                        </label>
                      </div>
                      <div className="flex items-center border border-gray-200 rounded dark:border-gray-700 style-rdio">
                        <input
                          id="bordered-radio-2"
                          type="radio"
                          value="user"
                          // name="bordered-radio"
                          {...register("role", { required: true })}
                          className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                        />
                        <label
                          htmlFor="bordered-radio-2"
                          className="w-full  text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                        >
                          <div className="flex justify-start items-center text-white">
                            <img
                              className="w-14 invert grayscale"
                              src={process.env.NODE_ENV === "development" ? "../icons/user-account.png" : "../public/icons/user-account.png"}
                              alt="img here"
                            />
                            <span>User</span>
                          </div>
                        </label>
                      </div>
                    </div>
                  </div>
                ) : (
                  <>
                    <div className="inputFelids mb-3">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInput"
                        {...register("name", { required: true })}
                        placeholder="First Name"
                      />
                    </div>
                    <div className="inputFelids mb-3">
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInput"
                        {...register("email", { required: true })}
                        placeholder="Email Address / Username"
                      />
                    </div>
                    <div className="inputFelids mb-3">
                      <input
                        type="password"
                        className="form-control"
                        id="floatingInput"
                        {...register("password", { required: true })}
                        placeholder="Password"
                      />
                    </div>
                    <div className="inputFelids mb-4">
                      <FormControlLabel
                        control={
                          <Checkbox
                            onChange={(e) =>
                              setValue("agree", e.target.checked)
                            }
                          />
                        }
                        label="I agree with Terms and Privacy"
                      />
                    </div>
                    <div className="float-left w-full">
                      <Stack>
                        {loading ? (
                          <div className="flex justify-center items-center">
                            <CircularProgress />
                          </div>
                        ) : (
                          <Button
                            variant="text"
                            type="submit"
                            disabled={loading}
                            className="btn bg-secondary text-white font-bold py-2"
                          >
                            Sign Up
                          </Button>
                        )}
                      </Stack>
                    </div>

                    <div className="saprator_list text-center mb-1">
                      <span className="">or Sign Up with</span>
                    </div>
                    <ul className="flex justify-between items-center p-0">
                      <li>
                        <button
                          type="button"
                          onClick={LoginWithGoogle}
                          className="bg-[#e62b32] text-white py-2 text-xl px-4 rounded-md"
                        >
                          <i className="fa fa-google" aria-hidden="true"></i>
                        </button>
                      </li>
                      <li>
                        <button
                          type="button"
                          onClick={LoginWithGoogle}
                          className="bg-[#6081c4] text-white py-2 text-xl px-4 rounded-md"
                        >
                          <i className="fa fa-facebook" aria-hidden="true"></i>
                        </button>
                      </li>
                      <li>
                        <button
                          type="button"
                          onClick={LoginWithGoogle}
                          className="bg-black text-white py-2 text-xl px-4 rounded-md"
                        >
                          <i className="fa fa-apple" aria-hidden="true"></i>
                        </button>
                      </li>
                    </ul>
                    <h5 className="text-base font-light text-center mb-0">
                      Already Have An Account?{" "}
                      <Link href="/login" className="font-extrabold text-black">
                        Login
                      </Link>
                    </h5>
                  </>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>

      <FooterLayout p="pt-5" />
    </>
  );
}

export default Register;
