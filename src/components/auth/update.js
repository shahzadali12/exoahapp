import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

import { toast } from "react-toastify";

import { schemas } from "../schemas/updateSignup";
import { yupResolver } from "@hookform/resolvers/yup";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import CircularProgress from "@mui/material/CircularProgress";

import { useForm, Controller } from "react-hook-form";

import { useContext, useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import PhoneInput from "react-phone-input-2";
import { signOut, useSession } from "next-auth/react";
import { Store } from "../../store/AppStore";

function UpdateMissing({ setShow, show }) {
  // const label = { inputProps: { "aria-label": "Checkbox demo" } };
  const [loading, setLoading] = useState(false);
  const [role, setRole] = useState(true);
  const [selectionRole, setselectionRole] = useState(false);

  const { setApiload } = useContext(Store);
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const watchItem = watch();

  const userdata = useSession();
  const route = useRouter();

  useEffect(() => {
    if (userdata?.data?.user?.name) {
      setValue("name", userdata?.data?.user?.name);
      setValue("email", userdata?.data?.user?.email);
    }
  }, [userdata?.data?.user?.email]);

  // console.log(userdata, "here is user data")

  const onSubmit = async (data) => {
    setLoading(true);

    const regis = {
      role: data.role,
      email: data.email,
      name: data.name,
    };

    const response = await fetch(`/api/authenticated/login/role`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(regis),
    });

    if (response.status === 201) {
      setLoading(false);

      toast.success("Your role has been successfully registered!");
      setApiload(response);

      setLoading(true);
      setShow({
        ...show,
        role: false,
      });
    } else if (response.status === 409) {
      setLoading(false);

      setShow({
        ...show,
        role: false,
      });
    } else {
      toast.error("Something was wrong");
    }
  };

  return (
   
      <div className="auth-wrapper v3">
        <div className="auth-form">
          {/* {watchItem?.role && (
              <button
                type="button"
                onClick={() => setValue("role", "")}
                className="mr-2 text-black left-0 top-0 mb-3"
              >
                <ArrowBackIosNewIcon />
                Back
              </button>
            )} */}
          <div className="card-body">
            {/* <div className="row">
                <div className="d-flex justify-content-center">
                  <div className="auth-header">
                    <h2 className="text-secondary mt-4">
                      <b>
                       {!watchItem?.role ? "i am a ?" : `i'm ${watchItem?.role}`}
                      </b>
                    </h2>
                  </div>
                </div>
              </div> */}
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex justify-between items-center ">
                <div className="flex items-center  border border-gray-200 rounded dark:border-gray-700 style-rdio">
                  <input
                    id="bordered-radio-1"
                    type="radio"
                    value="creator"
                    {...register("role", { required: true })}
                    // name="bordered-radio"
                    className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500  dark:ring-offset-gray-800 focus:rounded-full focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                  />
                  <label
                    htmlFor="bordered-radio-1"
                    className="w-full text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                  >
                    <div className="flex justify-start items-center text-white">
                      <img
                        className="w-14 invert grayscale"
                        src="../icons/vip.png"
                        alt="img here"
                      />
                      <span>i'm a Creator</span>
                    </div>
                  </label>
                </div>
                <div className="flex items-center border border-gray-200 rounded dark:border-gray-700 style-rdio">
                  <input
                    id="bordered-radio-2"
                    type="radio"
                    value="user"
                    // name="bordered-radio"
                    {...register("role", { required: true })}
                    className="w-4 h-4 hidden text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                  />
                  <label
                    htmlFor="bordered-radio-2"
                    className="w-full  text-sm font-medium text-gray-900 dark:text-gray-300 bg-slate-400"
                  >
                    <div className="flex justify-start items-center text-white">
                      <img
                        className="w-14 invert grayscale"
                        src="../icons/user-account.png"
                        alt="img here"
                      />
                      <span>i'm a User</span>
                    </div>
                  </label>
                </div>
              </div>
              <div className="d-grid mt-4">
                <Stack>
                  {loading ? (
                    <div className="flex justify-center items-center">
                      <CircularProgress />
                    </div>
                  ) : (
                    <Button
                      variant="text"
                      type="submit"
                      disabled={loading}
                      className="btn bg-secondary text-white"
                    >
                      Submit
                    </Button>
                  )}
                </Stack>
              </div>
            </form>
          </div>
        </div>
      </div>
   
  );
}

export default UpdateMissing;
