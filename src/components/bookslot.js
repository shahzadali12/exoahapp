import { jwt } from "jsonwebtoken";
import moment from "moment";
import Link from "next/link";
import React, { useContext, useEffect, useState } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Store } from "../store/AppStore";
import axios from "axios";
import { toast } from "react-toastify";

function BookingSlot({ slots, nofication, setnofication,creator }) {

  const {userdata,Selected_creator,filterMenu,setlogins,indexing, setindexing, slotbooked, setslotbooked,paymentSlot,setpaymentSlot, filtering, setFilters, selectedTabIndex, setSelectedTabIndex,cnt, setCnt,prices, setslots,looked,states, setlooked } = useContext(Store);
 
  

 
  
  

  
 

 
 

  const handleChangeTab = (event, newValue) => {
    setSelectedTabIndex(newValue);
  };

  const handleClick = async (
    dayIndex,
    eventIndex,
    slotIndex,
    sslots,
    starttime
  ) => {



    const currentTimes = new Date(starttime)


    if (userdata?.info?.name) {
      
      setindexing({...indexing, dayIndex, eventIndex, slotIndex, sslots,starttime });

      // if (slots?.zoom) {
      const newData = JSON.parse(JSON.stringify(filterMenu));

      newData.forEach((day, dIndex) => {
        if (dIndex === dayIndex) {
          day.events.forEach((event, eIndex) => {
            if (eIndex === eventIndex) {
              event.result.forEach((slot, sIndex) => {
                slot.booked = slot?.date === starttime ? true : slot?.booked;

                 slot.name = slot?.date === starttime ? userdata?.info?.name : null;
                slot.email =
                  slot?.date === starttime ? userdata?.info?.email : null;
                slot.slug = slot?.date === starttime ? userdata?.info?.slug : null;
                slot.price =
                  slot?.date === starttime && sslots == 15
                    ? Selected_creator?.prices?.call?.video?.min15?.price
                    : sslots == 30
                    ? Selected_creator?.prices?.call?.video?.min30?.price
                    : sslots == 45
                    ? Selected_creator?.prices?.call?.video?.min45?.price
                    : 0;
                slot?.date === starttime && setCnt(slot?.date);



                // slot.name = sIndex === slotIndex ? userdata?.info?.name : null;
                // slot.email =
                //   sIndex === slotIndex ? userdata?.info?.email : null;
                // slot.slug = sIndex === slotIndex ? userdata?.info?.slug : null;
                // slot.price =
                //   sIndex === slotIndex && sslots == 15
                //     ? Selected_creator?.prices?.call?.video?.min15?.price
                //     : sslots == 30
                //     ? Selected_creator?.prices?.call?.video?.min30?.price
                //     : sslots == 45
                //     ? Selected_creator?.prices?.call?.video?.min45?.price
                //     : 0;
                // sIndex === slotIndex && setCnt(slot?.date);
                
              });
            }
          });
        }
      });
      
      setFilters(newData);

     
      setslots(sslots);


      // }
    } else {
      // toast.error("please login your account!");
      setlogins(true)
    }
  };
  // useEffect(() => {
  //   handleClick(0, 0, 0, 0);
  // }, []);


  const currentDate = new Date();


// const filteredEvents = (results) => results.filter(event => {
//     // Check each event's start time
//     const startTime = new Date(event.date);
//     if (startTime > currentDate) {
//         return true; // Exclude this event
//     }

//     // // Check each event's end time
//     // const endTime = new Date(event.endTime);
//     // if (endTime > currentDate) {
//     //     return false; // Exclude this event
//     // }

//     // If the event passes both checks, include it


//     return false;
// });


const renderRecord = (dataArrasy) => {
  const currentDate = new Date();


  const futureDates = dataArrasy.filter((date) => new Date(date?.date) > currentDate);

  
  futureDates.sort((a, b) => a - b);

  const current = new Date(futureDates[0].date)


  const filters = futureDates.filter(
    (date) => new Date(date?.date).getDate() === current.getDate()
  );

  return filters
}



  // const generatemeetingSlots = async () => {
  //   await axios
  //     .post(`${process.env.NEXTAUTH_URL}/api/zoom/create`, {
  //       token: slots?.zoom,
  //       topic: "Scheduled Meeting",
  //       duration: parseInt(prices),
  //       start_time: cnt,
  //     })
  //     .then((val) => {
  //       const data = {
  //         start: val?.data?.start_url,
  //         join: val?.data?.join_url,
  //         password: val?.data?.password,
  //         timezone: val?.data?.timezone,
  //         status: val?.data?.status,
  //         meetingid: val?.data?.id,
  //       };

  //       // setFilter(newData);

  //       // if(val?.status == 200){
  //       //   await handleClick(indexing?.dayIndex, indexing?.eventIndex, indexing?.slotIndex, indexing?.sslots, data);
  //       // }
  //     })
  //     .catch((err) => {
  //       console.error(err);
  //     });
  // };


  const ProcessingPaymentFunction = async () => {
    setpaymentSlot(true)
  }
 
  return (
    <div className="w-full float-left">
      <Tabs
        value={selectedTabIndex}
        onChange={handleChangeTab}
        className="mb-4"
      >
        {filtering[0] &&
          filtering[0].events.map((event, eventIndex) => (
            <Tab
              key={eventIndex}
              className={`py-0 px-3 rounded-md  mr-2`}
              label={`${event.slots.toString()} minutes`}
            />
          ))}
      </Tabs>

      {filtering && filtering.map((day, dayIndex) => (
        <div
          className="flex justify-start items-center flex-wrap w-full"
          key={day.id}
        >
          <h3 className="w-full bg-slate-700 text-white float-left rounded-md px-2 py-2 text-center">
            {day.name} {day.events[0].result && moment(renderRecord(day.events[0].result)[0].date).format("L")}
          </h3>
          {day.events.map((event, eventIndex) => (
            <div
              key={eventIndex}
              className="flex justify-between items-center flex-col w-full"
            >
              <div
                style={{
                  display: selectedTabIndex === eventIndex ? "block" : "none",
                }}
              >
                <div className="flex justify-between items-center py-1 px-2 rounded-md mb-2 text-white mr-2 flex-wrap w-full">
                  {renderRecord(event.result).map((slot, slotIndex) => (
                    <button
                      key={slotIndex}
                      type="button"
                      disabled={slot.booked}
                      onClick={() =>
                        handleClick(
                          dayIndex,
                          eventIndex,
                          slotIndex,
                          event?.slots,
                          slot?.date
                        )
                      }
                      className={`flex justify-between items-center ${
                        slot.booked ? "cursor-not-allowed" : "cursor-pointer"
                      } ${
                         
                        slot.booked ? indexing.starttime == slot?.date ? "bg-blue-500" : " bg-slate-400" : "bg-slate-700"
                      } py-1 mb-3 pl-2 pr-2 rounded-md text-white mr-1`}
                    >
                      <div className="flex justify-start items-center w-full">
                        <span className="">
                          {moment(slot.date).format("hh:mm")}
                        </span>
                        <span className="px-1">-</span>
                        <span>
                          {moment(slot.date)
                            .add(event.slots, "minutes")
                            .format("hh:mm")}
                        </span>
                      </div>
                    </button>
                  ))}
                </div>
              </div>
            </div>
          ))}
        </div>
      ))}
      <button
        type="button"
        onClick={ProcessingPaymentFunction}
        className="py-2 px-4 w-full font-medium bg-blue-500 text-white rounded-md text-2xl capitalize"
      >
        Proceed to pay $
        {prices == 15
          ? Selected_creator?.prices?.call?.video?.min15?.price
          : prices == 30
          ? Selected_creator?.prices?.call?.video?.min30?.price
          : prices == 45
          ? Selected_creator?.prices?.call?.video?.min45?.price
          : 0}
        .00 USD
      </button>
    </div>
  );
}

export default BookingSlot;
