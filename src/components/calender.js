import React from "react";


export default async function CalenderEvent() {
  const { google } = require("googleapis");
//   const calendar = google.calendar("v3");

  // Set up OAuth2 client
  const oauth2Client = new google.auth.OAuth2(
    "115573965766-3ec28sm7lh78ec9blfkrv64cb90f6p24.apps.googleusercontent.com",
    "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",
    "http://www.insiderinteract.com/"
  );

  // Set the credentials

  const authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: 'https://www.googleapis.com/auth/calendar', // Specify required scope(s)
  });

  
  const { tokens } = await oauth2Client.getToken(authUrl); // Use the authorization code received from the callback
const accessToken = tokens.access_token;
const refreshToken = tokens.refresh_token;
// Store these tokens securely for future use



  oauth2Client.setCredentials({
    access_token: accessToken,
    refresh_token: refreshToken,
    // Optionally, set expiry_date if available
  });

  // Create a new instance of Google Calendar API
  const calendar = google.calendar({ version: "v3", auth: oauth2Client });

  // Example: List upcoming 10 events
  calendar.events.list(
    {
      calendarId: "primary",
      timeMin: new Date().toISOString(),
      maxResults: 10,
      singleEvents: true,
      orderBy: "startTime",
    },
    (err, res) => {
      if (err) return console.error("The API returned an error:", err);
      const events = res.data.items;
      if (events.length) {
        console.log("Upcoming events:");
        events.map((event, i) => {
          const start = event.start.dateTime || event.start.date;
          console.log(`${start} - ${event.summary}`);
        });
      } else {
        console.log("No upcoming events found.");
      }
    }
  );

  // Example: Create a new event
  const event = {
    summary: "Sample Event",
    start: {
      dateTime: "2024-01-03T10:00:00",
      timeZone: "YOUR_TIME_ZONE",
    },
    end: {
      dateTime: "2024-01-03T12:00:00",
      timeZone: "YOUR_TIME_ZONE",
    },
  };
  calendar.events.insert(
    {
      calendarId: "primary",
      resource: event,
    },
    (err, res) => {
      if (err) return console.error("Error creating event:", err);
      console.log("Event created: %s", res.data.htmlLink);
    }
  );

  return <div>CalenderEvent</div>;
}
