import { jwt } from "jsonwebtoken";
import Link from "next/link";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Store } from "../store/AppStore";
import axios from "axios";
import { toast } from "react-toastify";
import moment from "moment";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import KeyboardTabOutlinedIcon from "@mui/icons-material/KeyboardTabOutlined";
function ChatSystem() {
  const { userdata, findDuplicates, Selected_creator } = useContext(Store);
  // var jwt = require('jsonwebtoken');
  // var token = jwt.sign({ foo: 'bar' }, "jahskdjhaskdhaklsjdhkla", { algorithm: 'RS256' });
  const divRef = useRef(null);

  const [messages, setMessage] = useState([]);

  const [loader, setLoader] = useState({
    loaderStarting: true,
    loaderOnclick: false,
    loaderSentmessage: false,
  });

  const [users, setusers] = useState([]);
  // const uniqueValue = Selected_creator?.messages && userdata?.messages && findDuplicates(userdata?.messages,Selected_creator?.messages)

  const uniqueValue = userdata?.messages.filter((val) => val !== userdata?._id);

  const [user, setuser] = useState({});

  const [MessageSend, setSendMessage] = useState("");

  useEffect(() => {
    async function chatSM() {
      await axios
        .post(`/api/chat/messager`, {
          message: userdata?.messages,
          id: userdata?._id,
        })
        .then((rp) => {
          setusers(rp?.data);
          setLoader({
            ...loader,
            loaderStarting: false,
          });
        })
        .catch((error) => {
          toa.loaderst.error(error);
          setLoader({
            ...loader,
            loaderStarting: false,
          });
        });
    }
    if (userdata?._id) {
      chatSM();
    }
  }, [userdata?._id]);

  //   const filtered = messages.filter(
  //     (message) =>
  //       message.sender === userdata?._id || message.receiver === userdata?._id
  //   );

  useEffect(() => {
    if (divRef.current) {
      divRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "nearest",
      });
    }
  }, [messages]);

  const ChatwithUser = async (usera) => {
    setLoader({
      ...loader,
      loaderOnclick: true,
    });

    await axios
      .get(`/api/chat/${usera?.messId}`)
      .then(async (resp) => {
        const datas = resp?.data[0].messages.map((val) => {
          return {
            id: val.id,
            data: val.date,
            text: val.text,
            user: val.id !== userdata._id ? usera.info : null,
          };
        });
        setLoader({
          ...loader,
          loaderOnclick: false,
        });
        setMessage(datas);
        setuser({ user: usera.info, _id: usera?._id, messId: usera?.messId });
      })
      .catch((error) => {
        toast.error(error);
      });
  };

  const ChatMessage = async (e) => {
    e.preventDefault();

    await axios
      .post(`/api/chat/chat`, {
        id: userdata?._id,
        receiver: user?._id,
        message: MessageSend,
        date: new Date(),
        chatid: user?.messId,
      })
      .then(async (resps) => {
        if (resps.status == 201) {
          setSendMessage("");
          setLoader({
            ...loader,
            loaderOnclick: true,
          });

          (await user?.messId) &&
            axios
              .get(`/api/chat/${user?.messId}`)
              .then(async (resp) => {
                const datas = resp?.data[0].messages.map((val) => {
                  return {
                    id: val.id,
                    data: val.date,
                    text: val.text,
                    user: val.id !== userdata._id ? user.info : null,
                  };
                });
                setLoader({
                  ...loader,
                  loaderOnclick: false,
                });
                setMessage(datas);
              })
              .catch((error) => {
                toast.error(error);
              });
        }
      })
      .catch((error) => {
        toast.error(error);
      });
  };



  return (
    <>
      {loader.loaderStarting ? (
        <Box className="flex justify-center items-center w-full h-screen">
          <CircularProgress />
          <span className="ml-4">Please wait...</span>
        </Box>
      ) : (
        <div className="h-screen">
          <div className="flex border border-grey rounded shadow-lg h-full">
            <div className="w-1/3 border flex flex-col">
              <div className="py-2 px-3 bg-grey-lighter flex flex-row justify-between items-center">
                <div>
                  <img
                    className="w-10 h-10 rounded-full"
                    src={
                      userdata?.info?.profilePhoto
                        ? `${
                            process.env.NODE_ENV === "development"
                              ? process.env.NEXTAUTH_URL
                              : process.env.NEXTAUTH_URL + "/public"
                          }/images/users/${userdata?.info?.profilePhoto}`
                        : "../current.png"
                    }
                  />
                </div>

                <div className="flex">
                  <div>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path
                        fill="#727A7E"
                        d="M12 20.664a9.163 9.163 0 0 1-6.521-2.702.977.977 0 0 1 1.381-1.381 7.269 7.269 0 0 0 10.024.244.977.977 0 0 1 1.313 1.445A9.192 9.192 0 0 1 12 20.664zm7.965-6.112a.977.977 0 0 1-.944-1.229 7.26 7.26 0 0 0-4.8-8.804.977.977 0 0 1 .594-1.86 9.212 9.212 0 0 1 6.092 11.169.976.976 0 0 1-.942.724zm-16.025-.39a.977.977 0 0 1-.953-.769 9.21 9.21 0 0 1 6.626-10.86.975.975 0 1 1 .52 1.882l-.015.004a7.259 7.259 0 0 0-5.223 8.558.978.978 0 0 1-.955 1.185z"
                      ></path>
                    </svg>
                  </div>
                  <div className="ml-4">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path
                        opacity=".55"
                        fill="#263238"
                        d="M19.005 3.175H4.674C3.642 3.175 3 3.789 3 4.821V21.02l3.544-3.514h12.461c1.033 0 2.064-1.06 2.064-2.093V4.821c-.001-1.032-1.032-1.646-2.064-1.646zm-4.989 9.869H7.041V11.1h6.975v1.944zm3-4H7.041V7.1h9.975v1.944z"
                      ></path>
                    </svg>
                  </div>
                  <div className="ml-4">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      width="24"
                      height="24"
                    >
                      <path
                        fill="#263238"
                        fillOpacity=".6"
                        d="M12 7a2 2 0 1 0-.001-4.001A2 2 0 0 0 12 7zm0 2a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 9zm0 6a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 15z"
                      ></path>
                    </svg>
                  </div>
                </div>
              </div>

              <div className="py-2 px-2 bg-grey-lightest">
                <input
                  type="text"
                  className="w-full px-2 py-2 text-sm"
                  placeholder="Search or start new chat"
                  disabled
                />
              </div>

              <div className="bg-grey-lighter flex-1 overflow-auto">
                {users.map((val, index) => {
                  return (
                    <div
                      onClick={() => ChatwithUser(val)}
                      key={index}
                      className="bg-white px-3 flex items-center hover:bg-grey-lighter cursor-pointer"
                    >
                      <div className="relative">
                        <img
                          className="h-12 w-12 rounded-full"
                          src={
                            val?.info?.profilePhoto
                              ? process.env.NODE_ENV === "development"
                                ? process.env.NEXTAUTH_URL +
                                  "/images/users/" +
                                  val?.info?.profilePhoto
                                : process.env.NEXTAUTH_URL +
                                  "/public/images/users/" +
                                  val?.info?.profilePhoto
                              : "../current.png"
                          }
                        />
                        {loader?.loaderOnclick && (
                          <span className="absolute top-0 left-0 right-0 bottom-0">
                            <CircularProgress />
                          </span>
                        )}
                      </div>
                      <div className="ml-4 flex-1 border-b border-grey-lighter py-4">
                        <div className="flex items-bottom justify-between">
                          <p className="text-grey-darkest">{val?.info?.name}</p>
                          <p className="text-xs text-grey-darkest">
                            {moment(val?.updatedAt).format("hh:mm A")}
                          </p>
                        </div>
                        <p className="text-grey-dark mt-1 text-sm">
                          {val?.info?.designation}
                        </p>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>

            {
              <div className="w-2/3 border flex flex-col">
                <div className="py-2 px-3 bg-grey-lighter flex flex-row justify-between items-center">
                  {user?.user?.name && (
                    <div className="flex items-center">
                      <div>
                        <img
                          className="w-10 h-10 rounded-full"
                          src={
                            user?.user?.profilePhoto
                              ? process.env.NODE_ENV === "development"
                                ? process.env.NEXTAUTH_URL +
                                  "/images/users/" +
                                  user?.user?.profilePhoto
                                : process.env.NEXTAUTH_URL +
                                  "/public/images/users/" +
                                  user?.user?.profilePhoto
                              : "../current.png"
                          }
                        />
                      </div>
                      <div className="ml-4">
                        <p className="text-grey-darkest">{user?.user?.name}</p>
                        <p className="text-grey-darker text-xs mt-1">
                          {user?.user?.designation}
                        </p>
                      </div>
                    </div>
                  )}

                  {/* <div className="flex">
                                <div>
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="#263238" fillOpacity=".5" d="M15.9 14.3H15l-.3-.3c1-1.1 1.6-2.7 1.6-4.3 0-3.7-3-6.7-6.7-6.7S3 6 3 9.7s3 6.7 6.7 6.7c1.6 0 3.2-.6 4.3-1.6l.3.3v.8l5.1 5.1 1.5-1.5-5-5.2zm-6.2 0c-2.6 0-4.6-2.1-4.6-4.6s2.1-4.6 4.6-4.6 4.6 2.1 4.6 4.6-2 4.6-4.6 4.6z"></path></svg>
                                </div>
                                <div className="ml-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="#263238" fillOpacity=".5" d="M1.816 15.556v.002c0 1.502.584 2.912 1.646 3.972s2.472 1.647 3.974 1.647a5.58 5.58 0 0 0 3.972-1.645l9.547-9.548c.769-.768 1.147-1.767 1.058-2.817-.079-.968-.548-1.927-1.319-2.698-1.594-1.592-4.068-1.711-5.517-.262l-7.916 7.915c-.881.881-.792 2.25.214 3.261.959.958 2.423 1.053 3.263.215l5.511-5.512c.28-.28.267-.722.053-.936l-.244-.244c-.191-.191-.567-.349-.957.04l-5.506 5.506c-.18.18-.635.127-.976-.214-.098-.097-.576-.613-.213-.973l7.915-7.917c.818-.817 2.267-.699 3.23.262.5.501.802 1.1.849 1.685.051.573-.156 1.111-.589 1.543l-9.547 9.549a3.97 3.97 0 0 1-2.829 1.171 3.975 3.975 0 0 1-2.83-1.173 3.973 3.973 0 0 1-1.172-2.828c0-1.071.415-2.076 1.172-2.83l7.209-7.211c.157-.157.264-.579.028-.814L11.5 4.36a.572.572 0 0 0-.834.018l-7.205 7.207a5.577 5.577 0 0 0-1.645 3.971z"></path></svg>
                                </div>
                                <div className="ml-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="#263238" fillOpacity=".6" d="M12 7a2 2 0 1 0-.001-4.001A2 2 0 0 0 12 7zm0 2a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 9zm0 6a2 2 0 1 0-.001 3.999A2 2 0 0 0 12 15z"></path></svg>
                                </div>
                            </div> */}
                </div>

                <div
                  className="flex-1 overflow-auto"
                  style={{ backgroundColor: "#DAD3CC" }}
                >
                  {user?.user?.name ? (
                    <div className="py-2 px-3" ref={divRef}>
                      {/* <div className="flex justify-center mb-2">
                                    <div className="rounded py-2 px-4" style={{backgroundColor: "#DDECF2"}}>
                                        <p className="text-sm uppercase">
                                            February 20, 2018
                                        </p>
                                    </div>
                                </div> */}

                      <div className="flex justify-center mb-4">
                        <div
                          className="rounded py-2 px-4"
                          style={{ backgroundColor: " #FCF4CB" }}
                        >
                          <p className="text-xs">
                            Messages to this chat and calls are now secured with
                            end-to-end encryption. Tap for more info.
                          </p>
                        </div>
                      </div>
                      {messages &&
                        messages.map((val, index) => {
                          return (
                            <div
                              className={
                                val.id == userdata?._id
                                  ? "flex justify-end mb-2"
                                  : "flex mb-2"
                              }
                              key={index}
                            >
                              <div
                                className="rounded py-2 px-3"
                                style={{
                                  backgroundColor:
                                    val.id == userdata?._id
                                      ? "#E2F7CB"
                                      : "#F2F2F2",
                                }}
                              >
                                <p
                                  className={
                                    val.id == userdata?._id
                                      ? "text-sm mt-1"
                                      : "text-sm text-teal"
                                  }
                                >
                                  {val.id == userdata?._id
                                    ? userdata?.info?.name
                                    : val?.user?.name}
                                </p>
                                <p className="text-sm mt-1">{val?.text}</p>
                                <p className="text-right text-xs text-grey-dark mt-1">
                                  {moment(val.date).format("hh:mm A")}
                                </p>
                              </div>
                            </div>
                          );
                        })}
                    </div>
                  ) : (
                    <h2 className="flex justify-center items-center h-screen font-medium text-gray-800 flex-col">
                      Welcome to Chat Room{" "}
                      {loader?.loaderOnclick && <CircularProgress />}
                    </h2>
                  )}
                </div>

                {user?.user?.name && (
                  <div className="bg-grey-lighter px-4 py-4 flex items-center">
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        width="24"
                        height="24"
                      >
                        <path
                          opacity=".45"
                          fill="#263238"
                          d="M9.153 11.603c.795 0 1.439-.879 1.439-1.962s-.644-1.962-1.439-1.962-1.439.879-1.439 1.962.644 1.962 1.439 1.962zm-3.204 1.362c-.026-.307-.131 5.218 6.063 5.551 6.066-.25 6.066-5.551 6.066-5.551-6.078 1.416-12.129 0-12.129 0zm11.363 1.108s-.669 1.959-5.051 1.959c-3.505 0-5.388-1.164-5.607-1.959 0 0 5.912 1.055 10.658 0zM11.804 1.011C5.609 1.011.978 6.033.978 12.228s4.826 10.761 11.021 10.761S23.02 18.423 23.02 12.228c.001-6.195-5.021-11.217-11.216-11.217zM12 21.354c-5.273 0-9.381-3.886-9.381-9.159s3.942-9.548 9.215-9.548 9.548 4.275 9.548 9.548c-.001 5.272-4.109 9.159-9.382 9.159zm3.108-9.751c.795 0 1.439-.879 1.439-1.962s-.644-1.962-1.439-1.962-1.439.879-1.439 1.962.644 1.962 1.439 1.962z"
                        ></path>
                      </svg>
                    </div>
                    <div className="flex-1 mx-4">
                      <form onSubmit={ChatMessage}>
                        <div className="typeer_msg">
                          <input
                            type="text"
                            className="w-full border rounded px-2 py-2"
                            onChange={(e) => setSendMessage(e.target.value)}
                            name="message"
                            value={MessageSend}
                            placeholder="Type your message Here..."
                            required
                          />
                          <button type="submit">
                            <KeyboardTabOutlinedIcon fontSize="large" />
                          </button>
                        </div>
                      </form>
                    </div>
                    <div>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        width="24"
                        height="24"
                      >
                        <path
                          fill="#263238"
                          fillOpacity=".45"
                          d="M11.999 14.942c2.001 0 3.531-1.53 3.531-3.531V4.35c0-2.001-1.53-3.531-3.531-3.531S8.469 2.35 8.469 4.35v7.061c0 2.001 1.53 3.531 3.53 3.531zm6.238-3.53c0 3.531-2.942 6.002-6.237 6.002s-6.237-2.471-6.237-6.002H3.761c0 4.001 3.178 7.297 7.061 7.885v3.884h2.354v-3.884c3.884-.588 7.061-3.884 7.061-7.885h-2z"
                        ></path>
                      </svg>
                    </div>
                  </div>
                )}
              </div>
            }
          </div>
        </div>
      )}

      {/*     
    
    <div className="flex flex-row justify-between bg-white rounded-md">
      <div className="flex flex-col w-2/5 border-r-2 overflow-y-auto">
        <div className="border-b-2 py-4 px-2">
          <input
            type="text"
            placeholder="search chatting"
            className="py-2 px-2 border-2 border-gray-200 rounded-2xl w-full"
          />
        </div>
        <div
          className="flex flex-row py-4 px-2 justify-center items-center border-b-2"
        >
          <div className="w-1/3">
            <img
              src="https://source.unsplash.com/_7LbC5J-jw4/600x600"
              className="object-cover h-10 w-10 rounded-full"
              alt=""
            />
          </div>
          <div className="w-full">
            <div className="text-base font-semibold leading-none">Luis1994</div>
            <span className="text-gray-500 text-sm">Pick me at 9:00 Am</span>
          </div>
        </div>
       
      </div>
      <div className="w-full px-5 flex flex-col justify-between">
        <div className="flex flex-col mt-5">
          <div className="flex justify-end mb-4">
            <div
              className="mr-2 py-3 px-4 bg-blue-400 rounded-bl-3xl rounded-tl-3xl rounded-tr-xl text-white"
            >
              Welcome to group everyone !
            </div>
            <img
              src="https://source.unsplash.com/vpOeXr5wmR4/600x600"
              className="object-cover h-8 w-8 rounded-full"
              alt=""
            />
          </div>
          <div className="flex justify-start mb-4">
            <img
              src="https://source.unsplash.com/vpOeXr5wmR4/600x600"
              className="object-cover h-8 w-8 rounded-full"
              alt=""
            />
            <div
              className="ml-2 py-3 px-4 bg-gray-400 rounded-br-3xl rounded-tr-3xl rounded-tl-xl text-white"
            >
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat
              at praesentium, aut ullam delectus odio error sit rem. Architecto
              nulla doloribus laborum illo rem enim dolor odio saepe,
              consequatur quas?
            </div>
          </div>
          <div className="flex justify-end mb-4">
            <div>
              <div
                className="mr-2 py-3 px-4 bg-blue-400 rounded-bl-3xl rounded-tl-3xl rounded-tr-xl text-white"
              >
                Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                Magnam, repudiandae.
              </div>

              <div
                className="mt-4 mr-2 py-3 px-4 bg-blue-400 rounded-bl-3xl rounded-tl-3xl rounded-tr-xl text-white"
              >
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Debitis, reiciendis!
              </div>
            </div>
            <img
              src="https://source.unsplash.com/vpOeXr5wmR4/600x600"
              className="object-cover h-8 w-8 rounded-full"
              alt=""
            />
          </div>
          <div className="flex justify-start mb-4">
            <img
              src="https://source.unsplash.com/vpOeXr5wmR4/600x600"
              className="object-cover h-8 w-8 rounded-full"
              alt=""
            />
            <div
              className="ml-2 py-3 px-4 bg-gray-400 rounded-br-3xl rounded-tr-3xl rounded-tl-xl text-white"
            >
              happy holiday guys!
            </div>
          </div>
        </div>
        <div className="py-5">
          <input
            className="w-full bg-gray-300 py-5 px-3 rounded-xl"
            type="text"
            placeholder="type your message here..."
          />
        </div>
      </div>
      <div className="w-2/5 border-l-2 px-5">
        <div className="flex flex-col">
          <div className="font-semibold text-xl py-4">Mern Stack Group</div>
          <img
            src="https://source.unsplash.com/L2cxSuKWbpo/600x600"
            className="object-cover rounded-xl h-64"
            alt=""
          />
          <div className="font-semibold py-4">Created 22 Sep 2021</div>
          <div className="font-light">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt,
            perspiciatis!
          </div>
          </div>
        </div>
      </div> */}
    </>
  );
}

export default ChatSystem;
