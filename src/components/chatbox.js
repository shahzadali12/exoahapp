import React, { useContext, useEffect, useState,useRef } from "react";
import KeyboardTabOutlinedIcon from "@mui/icons-material/KeyboardTabOutlined";
import moment from "moment";
import { toast } from "react-toastify";
import { useSession } from "next-auth/react";
import { Store } from "../store/AppStore";
import axios from "axios";
import StripePayment from "./payment/stripe";

function Chatbox({ creator, show, setShow }) {
  const { userdata, paymentSuccess, setPaymentuccess,message, setMessage,messages, setmessages,findDuplicates,setlogins } = useContext(Store);

  const divRef = useRef(null);
  function timeDifference(previous) {
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;
    var elapsed = new Date() - previous;
    if (elapsed < msPerMinute) {
      return Math.round(elapsed / 1000) + " seconds ago";
    } else if (elapsed < msPerHour) {
      return Math.round(elapsed / msPerMinute) + " minutes ago";
    } else if (elapsed < msPerDay) {
      return Math.round(elapsed / msPerHour) + " hours ago";
    } else if (elapsed < msPerMonth) {
      return "" + Math.round(elapsed / msPerDay) + " days ago";
    } else if (elapsed < msPerYear) {
      return "" + Math.round(elapsed / msPerMonth) + " months ago";
    } else {
      return "" + Math.round(elapsed / msPerYear) + " years ago";
    }
  }

  const { data } = useSession();

 



  const ChatMessage = async (e) => {
    e.preventDefault();
    if (!userdata?._id) {
      // toast.error("please login your account First");
      setlogins(true)
    } else {
      setShow(true);
    }
  };
  

 



  const uniqueValue = userdata?.messages && findDuplicates(userdata?.messages,creator?.messages)


  useEffect(() => {
    async function chatSM() {
      await axios
        .get(`/api/chat/${uniqueValue[0]}`)
        .then((rp) => {
          setmessages(rp.data);
        })
        .catch((error) => {
          toast.error(error);
        });
    }
    if(uniqueValue && uniqueValue[0]){
      chatSM();
    }
  }, []);

  //   useEffect(() => {

  //    console.log(filteredByDateTime,filtered, "here is ")
  //   }, [userdata, messages]);

  // Filter messages based on sender or receiver ID matching current user ID
  const filtered = messages.filter(
    (message) =>
      message.sender === userdata?._id || message.receiver === userdata?._id
  );

  //  // Get current date and time
  //  const currentDateTime = new Date();

  //  // Filter messages based on the current date, hour, minute, and second
  //  const filteredByDateTime = filtered.filter(message => {
  //    const messageDateTime = new Date(message?.createdAt);
  //    return messageDateTime.getFullYear() === currentDateTime.getFullYear() &&
  //      messageDateTime.getMonth() === currentDateTime.getMonth() &&
  //      messageDateTime.getDate() === currentDateTime.getDate() &&
  //      messageDateTime.getHours() === currentDateTime.getHours() &&
  //      messageDateTime.getMinutes() === currentDateTime.getMinutes() &&
  //      messageDateTime.getSeconds() === currentDateTime.getSeconds();
  //  });
  
  useEffect(() => {
    if (divRef.current) {
        divRef.current.scrollIntoView(
        {
          behavior: 'smooth',
          block: 'end',
          inline: 'nearest'
        })
    }
  },
  [messages])


  return (
    <>
      <StripePayment creator={creator} show={show} setShow={setShow} />


      <div className="card border-none">
        <div className="card-body overflow-auto min-h-[50px] max-h-[250px] mb-3 chat_scroller">
          {!messages?.length > 0 && (
            <div className="checkpoints bg-[#bba7ed] pl-8 py-3 float-left w-full rounded-md">
              <ul className="m-0 px-0  list-decimal float-left w-full">
                <li>
                  <p>Simply type your question. (150 words limit)</p>
                </li>
                <li>
                  <p>
                    You cannot edit or delete the message so choose your words
                    wisely.
                  </p>
                </li>
                <li>
                  <p>
                    Send your finalized message and wait for creator's response.
                  </p>
                </li>
                <li>
                  <p>Response time can vary from 2-7 days.</p>
                </li>
                <li>
                  <p>
                    You won't be charged until the creator sends you a response.
                  </p>
                </li>
              </ul>
            </div>
          )}

{/* <span key={index}>{user.id == userdata?._id ? "me" : "other user"}{user.id == userdata?._id ? user.text : user.text}</span> */}

{/* 
creator?._id == userdata?._id
                        ? val?.sender == userdata?._id
                          ? "in client flex-row-reverse"
                          : "in admin flex-row-reverse"
                        : val?.sender == "admin"
                        ? "in admin flex-row-reverse"
                        : val?.sender == "admin"
                        ? "in admin flex-row-reverse" 
                        
                        val?.id == userdata?._id
                              ? userdata?.info?.profilePhoto
                                ? `${
                                    process.env.NODE_ENV === "development"
                                      ? process.env.NEXTAUTH_URL
                                      : process.env.NEXTAUTH_URL + "/public"
                                  }/images/users/${
                                    userdata?.info?.profilePhoto
                                  }`
                                : "https://bootdey.com/img/Content/avatar/avatar6.png"
                              : userdata?.info?.profilePhoto
                              ? `${
                                  process.env.NODE_ENV === "development"
                                    ? process.env.NEXTAUTH_URL
                                    : process.env.NEXTAUTH_URL + "/public"
                                }/images/users/${userdata?.info?.profilePhoto}`
                              : "https://cdni.iconscout.com/illustration/premium/thumb/male-customer-care-agent-6472059-5359857.png"
                            : val?.receiver == "admin"
                            ? "https://cdni.iconscout.com/illustration/premium/thumb/male-customer-care-agent-6472059-5359857.png"
                            : val?.receiver == "admin"
                            ? "https://cdni.iconscout.com/illustration/premium/thumb/male-customer-care-agent-6472059-5359857.png"
                            : userdata?.info?.profilePhoto
                        
                        
                        */}
          <ul className="chat-list" ref={divRef}>
            {messages &&
              messages.map((val, indx) => {


                return val?.messages.map((user,index)=>{
                  return <li
                    key={index}
                    
                    className={`base-class ${
                      user?.id != userdata?._id
                        ? "in client flex-row-reverse"
                        : "out pub "
                    }`}
                  >
                    <div className="chat-img">
                      <img
                        alt="Avtar"
                        src={`${
                          user?.id == userdata?._id ? userdata?.info?.profilePhoto ?    
                           `${
                                process.env.NODE_ENV === "development"
                                  ? process.env.NEXTAUTH_URL
                                  : process.env.NEXTAUTH_URL + "/public"
                              }/images/users/${userdata?.info?.profilePhoto}`
                              : "https://bootdey.com/img/Content/avatar/avatar6.png"
                            : creator?.infos?.profilePhoto
                            ? `${
                                process.env.NODE_ENV === "development"
                                  ? process.env.NEXTAUTH_URL
                                  : process.env.NEXTAUTH_URL + "/public"
                              }/images/users/${creator?.infos?.profilePhoto}`
                            : "https://bootdey.com/img/Content/avatar/avatar2.png"
                         
                          
                        }`}
                      />
                    </div>
                    <div className={`chat-body`}>
                      <div className={`chat-message`}>
                        <h5>
                          {user?.id === userdata?._id ? userdata?.info?.name
                            : creator?.infos?.name}
                        </h5>
                        <p>{user?.text}</p>
                        <small>
                          {timeDifference(new Date(user?.date))}
                        </small>
                      </div>
                    </div>
                 
                  </li>
                  
                
                 
                 
                  
                }) 
              })}
          </ul>
        </div>
      </div>

      <form onSubmit={ChatMessage}>
        <div className="typeer_msg">
          <textarea
            type="text"
            onChange={(e) => setMessage(e.target.value)}
            name="message"
            value={message}
            placeholder="Type your message Here..."
            required
          />
          <button type="submit">
            <KeyboardTabOutlinedIcon fontSize="large" />
          </button>
        </div>
      </form>
    </>
  );
}

export default Chatbox;
