import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";
import { useForm, SubmitHandler } from "react-hook-form";
import { schemas } from "./schemas/Create";
import { yupResolver } from "@hookform/resolvers/yup";
import { getSession, useSession } from "next-auth/react";
import CircularProgress from '@mui/material/CircularProgress';
import { useState } from "react";
import { useRouter } from "next/router";
function CreateAmeeting({access_token,setUpdate,setShow}) {

  const [loading,setLoading] = useState(false)
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const route = useRouter()
  const onSubmit = async (data) => {
    setLoading(true)
    
    const payload = {
        token: access_token,
        topic: data.topic,
        start_time: data.start_time,
        password: data.password
    }



    const response = await fetch(`/api/zoom/create`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });

      const final = await response.json()
      if(final.id){
        setLoading(false)
        setUpdate(final)
        setShow(false)
      }
    
  };




  return (

        <div className="auth-form">
          <div className="card my-5">
            <div className="card-body">

              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-floating mb-3">
                  <input
                    type="text"
                    className="form-control"
                    id="floatingInput"
                    {...register("topic", { required: true })}
                    placeholder="Topic .."
                  />
                  <label htmlFor="floatingInput">
                  Type Topic
                  </label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="date"
                    className="form-control"
                    id="start_time"
                    {...register("start_time", { required: true })}
                    placeholder="start time .."
                  />
                  <label htmlFor="start_time">
                  Start time
                  </label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    type="password"
                    className="form-control"
                    id="meetingpassword"
                    {...register("password", { required: true })}
                    placeholder="Password"
                  />
                  <label htmlFor="meetingpassword">Meeting Password</label>
                </div>
                
             
                <div className="d-grid mt-4">
               
      
   
                  <Stack>
                    {loading ? <div className="flex justify-center items-center"><CircularProgress /></div> : <Button
                      variant="text"
                      type="submit"
                      disabled={loading}
                      className="btn bg-secondary text-white"
                    >
                      Create a Meeting
                    </Button> }
                  </Stack>
                </div>
              </form>
              
            </div>
          </div>
        </div>

  );
}

export default CreateAmeeting;


