import React,{ useContext, useState } from 'react'
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Select from "react-select";
import { Store } from '../store/AppStore';
function FilterMenu() {
    const {Selection,setSelection,options} = useContext(Store)
    const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  
  const customstyles = {
    control: (base) => ({
      ...base,
      height: 54,
      minheight: 54,
    //   background: "transparent",
    //   border: "none",
    //   borderColor: base.isFocused ? '' : '',

    }),
  };

  const handleChange = (event) => {
    setSelection(event);
    setShow(false)
  };

  return (
    <>
      {/* <Button variant="primary"  className="me-2">
        {name}
      </Button> */}
      <h4 onClick={handleShow} className="mb-0 text-white text-md cursor-pointer">Filter <i className="fa fa-filter" aria-hidden="true"></i></h4>
      <Offcanvas show={show} onHide={handleClose} placement="end" >
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Search Celebrities</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
             <Select
            options={options}
            styles={customstyles}
            onChange={handleChange}
            value={Selection}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}


export default FilterMenu;

{/* <FilterMenu key={idx} placement={placement} name={placement} /> */}

// function Example() {
//   return (
//     <>
//       {['start', 'end', 'top', 'bottom'].map((placement, idx) => (
//         <FilterMenu key={idx} placement={placement} name={placement} />
//       ))}
//     </>
//   );
// }

// render(<Example />);