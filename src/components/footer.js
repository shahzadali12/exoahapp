function Footer(){
    return(
        <footer className="pc-footer">
        <div className="footer-wrapper container-fluid">
          <div className="row">
            <div className="col my-1">
              <p className="m-0">
                Copyright &copy;{" "}
                <a href="https://itsmydesigns.com/" target="_blank">
                  Shahzad Ali
                </a>
              </p>
            </div>
            <div className="col-auto my-1">
              <ul className="list-inline footer-link mb-0">
                <li className="list-inline-item">
                  <a href="https://itsmydesigns.com/" target="_blank">
                    Home
                  </a>
                </li>
                <li className="list-inline-item">
                  <a
                    href="https://itsmydesigns.com/privacy-policy/"
                    target="_blank"
                  >
                    Privacy Policy
                  </a>
                </li>
                <li className="list-inline-item">
                  <a href="https://itsmydesigns.com/contact/" target="_blank">
                    Contact us
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    )
}

export default Footer;