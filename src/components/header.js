import React, { useContext } from "react";
import logo from "../../public/logo2.png";
import MenuIcon from "@mui/icons-material/Menu";
import SearchIcon from "@mui/icons-material/Search";
import CropFreeIcon from "@mui/icons-material/CropFree";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import user from "../../public/images/user/avatar-1.jpg";
import SettingsSuggestIcon from "@mui/icons-material/SettingsSuggest";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { getSession, useSession, signIn, signOut } from "next-auth/react";
import { useRouter } from "next/router";
import { Store } from "../store/AppStore";
import Link from "next/link";

const Header = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const {userdata,setShow} = useContext(Store);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const route = useRouter()
  function handleSignOut() {
    signOut({
      redirect: false,
      // callbackUrl: `${process.env.NEXTAUTH_URL}/login`
    });
    localStorage.removeItem("meeting");
    localStorage.removeItem("token");
    setShow(false);

    route.push(`/login`);
   
  }







  return (
    <header className="pc-header">
      <div className="m-header">
        <a href="index.html" className="b-brand">
          <img src={logo.src} alt="log" className="logo logo-lg w-2/3" />
        </a>

        <div className="pc-h-item">
          <a
            href="#"
            className="pc-head-link head-link-secondary m-0"
            id="sidebar-hide"
          >
            <MenuIcon />
          </a>
        </div>
      </div>
      <div className="header-wrapper">
        <div className="me-auto pc-mob-drp">
          <ul className="list-unstyled">
            <li className="pc-h-item header-mobile-collapse">
              <a
                href="#"
                className="pc-head-link head-link-secondary ms-0"
                id="mobile-collapse"
              >
                <i><MenuIcon /></i>
              </a>
            </li>
            
            <li className="pc-h-item d-none d-md-inline-flex">
              <form className="header-search">
                <div className="flex justify-center items-center">
                <i className="absolute left-3"><SearchIcon/></i>
                <input
                  type="search"
                  className="form-control"
                  placeholder="Search here..."
                />
                </div>
                {/* <button className="btn btn-light-secondary btn-search">
                  <SearchIcon />
                </button> */}
              </form>
            </li>
          </ul>
        </div>
        <div className="ms-auto">
          <ul className="list-unstyled">
            <li className="dropdown pc-h-item header-user-profile">
              <Button
                id="basic-button"
                className="dropdown-toggle arrow-none me-0"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
              >
                
                {/* <div className="pc-head-link head-link-primary dropdown-toggle arrow-none me-0"> */}
                  <img src={userdata?.info?.profilePhoto ? `${process.env.NODE_ENV === "development" ? process.env.NEXTAUTH_URL : process.env.NEXTAUTH_URL + "/public"}/images/users/${userdata?.info?.profilePhoto}` : user.src} alt="user-image" className="user-avtar " />
                  <div className="flex flex-col justify-center text-left">
                  <span className="text-lg capitalize leading-none">
                    {/* <i className="ti"><SettingsSuggestIcon/></i> */}
                    {userdata?.info?.name && userdata?.info?.name}
                  </span>
                  {userdata?.info?.role && <p className="text-sm mb-0 capitalize">{userdata?.info?.role}</p>}
                  </div>
                {/* </div> */}
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                {/* <MenuItem onClick={handleClose}>
                  <div className="dropdown-header">
                    <h4>
                      Good Morning,{" "}
                      <span className="small text-muted"> John Doe</span>
                    </h4>
                    <p className="text-muted">Project Admin</p>
                    <hr />
                  </div>
                </MenuItem> */}
                {/* <MenuItem onClick={handleClose}>Account Settings</MenuItem> */}
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard">Dashboard</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard/chats">Chat Room</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard/bookedmeetings">Call Room</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/profile">Edit profile</Link></MenuItem>
                <MenuItem onClick={handleSignOut}>Logout</MenuItem>
              </Menu>
            </li>
          </ul>
        </div>
      </div>
    </header>
  );
};
export default Header;
