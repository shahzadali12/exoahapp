import { useEffect, useState } from "react";
import { Dialog } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";
const navigation = [
  { name: "Find Creators", href: "/" },
  { name: "For Fans", href: "/" },
  { name: "About Us", href: "/" },
  { name: "Company", href: "/" },
];

export default function Banner() {
 
  // const SmallDevices = useMediaQuery({ query: '(max-width: 1224px)' })

  // const SmallDevices = useMediaQuery(
  //   { maxDeviceWidth: 991 },
  //   { deviceWidth: 480 } // `device` prop
  // )
  const [SmallDevices, setScreen] = useState(false)

  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  },[] );
 
  


  return (
    <div>
      <div className="relative">
        
        <div
          className={`relative left-0 top-0 right-0 bottom-0  ${SmallDevices ? "px-0" : "px-24"}`}
          aria-hidden="true"
        >
          {/* <div
            className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
            style={{
              clipPath:
                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
            }}
          /> */}
          <img
            src="../images/banner01.png"
            className={SmallDevices ? "h-[25rem] w-full rounding_custom" : "w-full h-full rounded-[60px]"}
            alt="img here"
          />
        </div>
        <div className={SmallDevices ? "absolute container top-2/4 -mt-28 left-0 right-0 text-left mx-auto" : "absolute container top-2/4 -mt-96 left-0 right-0 text-left mx-auto py-32 sm:py-48 lg:py-56"}>
          
          <div className={SmallDevices ? "text-left px-2" :"text-left"}>
            <h1 className={SmallDevices ? "text-white text-4xl" : "text-9xl font-light tracking-tight text-white sm:text-6xl"}>
              Interact <b>Smart</b> <br/>Get <b>Smart</b>
            </h1>
            <p className={SmallDevices ? "text-gray-600 font-light text-justify text-xl leading-normal" : "mt-6 text-3xl w-[40%] font-light leading-8 text-white"}>
              This is your Path to direct interaction with leading experts and
              your favorite creators around the world.
            </p>
            <div className={SmallDevices ? "mt-3 flex items-center justify-start gap-x-6" : "mt-10 flex items-center justify-start gap-x-6"}>
              <a
                href="#"
                className="rounded-full bg-indigo-600 px-7 py-2 text-xl font-medium text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Get started
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
