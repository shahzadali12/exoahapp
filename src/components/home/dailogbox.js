import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";
import { XMarkIcon } from "@heroicons/react/24/outline";

export default function POPUP({ open, setOpen }) {
  const callouts = [
    {
      name: "Ashley Porter",
      description: "@ashleyporter",
      imageSrc:
        "https://images.unsplash.com/photo-1534528741775-53994a69daeb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=1024&h=1024&q=80",
      imageAlt:
        "Desk with leather desk pad, walnut desk organizer, wireless keyboard and mouse, and porcelain mug.",
      href: "#",
    },
  ];

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={setOpen}>
        <Transition.Child
          as={Fragment}
          enter="ease-in-out duration-500"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in-out duration-500"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-hidden">
          <div className="absolute inset-0 overflow-hidden">
            <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
              <Transition.Child
                as={Fragment}
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <Dialog.Panel className="pointer-events-auto relative w-screen max-w-md">
                  <Transition.Child
                    as={Fragment}
                    enter="ease-in-out duration-500"
                    enterFrom="opacity-0"
                    enterTo="opacity-100"
                    leave="ease-in-out duration-500"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <div className="absolute left-0 top-0 -ml-8 flex pr-2 pt-4 sm:-ml-10 sm:pr-4">
                      <button
                        type="button"
                        className="relative rounded-md text-gray-300 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
                        onClick={() => setOpen(false)}
                      >
                        <span className="absolute -inset-2.5" />
                        <span className="sr-only">Close panel</span>
                        <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                      </button>
                    </div>
                  </Transition.Child>
                  <div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
                    <Dialog.Title className="text-base font-semibold leading-6 text-gray-900">
                      <div className="flex justify-between items-center py-3 px-4 sm:px-6">
                        <h2 className="text-xl font-medium text-gray-900 mb-0">
                          Profile
                        </h2>
                        <button
                          type="button"
                          className="relative rounded-md text-gray-500 hover:text-black focus:outline-none focus:ring-2 focus:ring-white"
                          onClick={() => setOpen(false)}
                        >
                          <span className="sr-only">Close panel</span>
                          <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                      </div>
                      </Dialog.Title>

                     

                    {/* <div className="relative mt-6 flex-1 px-4 sm:px-6"> */}
                    {callouts.map((callout) => (
                        <div key={callout.name} className="group relative">
                          <div className="relative h-80 w-full overflow-hidden bg-white sm:aspect-h-1 sm:aspect-w-2 lg:aspect-h-1 lg:aspect-w-1 group-hover:opacity-75 sm:h-64">
                            <img
                              src={callout.imageSrc}
                              alt={callout.imageAlt}
                              className="h-full w-full object-cover object-center"
                            />
                          </div>
                          <div className="px-4 float-left w-full">
                            <h3 className="mt-6 text-2xl font-bold text-gray-500">
                              <a
                                href={callout.href}
                                className="text-black relative leading-none"
                              >
                                {callout.name}
                                <span className="w-2 h-2 rounded-full bg-green-600 absolute -right-4 top-2/4 -mt-1"></span>
                              </a>
                              <p className="text-base font-normal text-gray-500 leading-none">
                                {callout.description}
                              </p>
                            </h3>
                            <div className="flex justify-between items-center">
                              <button type="button" className="bg-blue-500 text-white w-5/12 rounded-md text-1xl font-medium py-1">Message</button>
                              <button type="button" className="bg-white border text-black w-5/12 rounded-md text-1xl font-medium py-1 hover:bg-blue-500 hover:text-white">Call</button>
                              <button type="button" className="bg-white border text-gray-600 rounded-md text-1xl font-medium w-7 text-center py-[7px]">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  viewBox="0 0 16 16"
                                  fill="currentColor"
                                  className="w-full h-full px-1"
                                >
                                  <path d="M8 2a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM8 6.5a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3ZM9.5 12.5a1.5 1.5 0 1 0-3 0 1.5 1.5 0 0 0 3 0Z" />
                                </svg>
                              </button>
                            </div>
                          </div>
                        </div>
                      ))}
                    
                    {/* </div> */}
                      <div className="py-3 px-4">
                    <h6>Bio</h6>
                    <p>Enim feugiat ut ipsum, neque ut. Tristique mi id elementum praesent. Gravida in tempus feugiat netus enim aliquet a, quam scelerisque. Dictumst in convallis nec in bibendum aenean arcu.</p>
                    <h6>Location</h6>
                    <p>New York, NY, USA</p>
                    <h6>Website</h6>
                    <p>ashleyporter.com</p>
                    <h6>Birthday</h6>
                    <p>June 23, 1988</p>
                    </div>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}
