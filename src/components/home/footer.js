import Link from 'next/link';
import React,{useState,useEffect} from 'react'

import { useMediaQuery } from "react-responsive";
function FooterLayout({p}) {
  const [SmallDevices, setScreen] = useState(false)

  useEffect(()=>{
    setScreen(window.innerWidth > 768 ? false : true)
  },[])
  return (
    <footer className={`bg-black ${p ? p : ""} overlay_bg`}>
        <div className="container">
          <div className={`flex justify-around ${SmallDevices && SmallDevices ? "" : "items-center"}`}>
          <div className="footer_left">
            <div className="mb-4 w-full">
              <img src="./images/logo.png" className={SmallDevices && SmallDevices ? "px-2 w-full" : "w-72"} alt="here is logo" />
            </div>
            <Link href="" className={`bg-white ml-2 inline-block mb-3 rounded-full text-black font-medium ${SmallDevices ? "text-sm px-1 py-1" : "text-lg px-3 py-2"}`}>Contact Support</Link>
            <div className="titles ml-2">
              <h5 className={SmallDevices ? "text-white text-base leading-normal font-light mb-0" : "text-white text-6xl leading-normal font-light mb-0"}>Interact <b>Smart</b><br/> Get <b>Smart</b></h5>
            </div>
          </div>

          <div className="footer_right">
            <ul>
              <li>
                <h5>Explore</h5>
              </li>
              <li>
                <Link href="">About Us</Link>
              </li>
              <li>
                <Link href="">Vision</Link>
              </li>
              <li>
                <Link href="">Find Creators</Link>
              </li>
              <li>
                <Link href="">Contacts</Link>
              </li>
            </ul>
            

            <ul>
              <li><h5>Socials</h5></li>
              <li>
                <Link href="">Twitter</Link>
              </li>
              <li>
                <Link href="">Facebook</Link>
              </li>
              <li>
                <Link href="">Instagram</Link>
              </li>
              <li>
                <Link href="">Linkedin</Link>
              </li>
              <li>
                <Link href="">Github</Link>
              </li>
            </ul>
          </div>
          </div>
        </div>
        <div className="flex justify-center items-center text-white capitalize py-4">All rights reserved 2024 Insider Interact</div>
      </footer>
  )
}

export default FooterLayout