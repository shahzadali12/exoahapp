import { useState, useEffect,useContext } from "react";
import { Dialog } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";
import { Button, Menu, MenuItem } from "@mui/material";
import { Store } from "../../store/AppStore";
import { useRouter } from "next/router";
import user from "../../../public/images/user/avatar-1.jpg";
import { signOut } from "next-auth/react";
function HeaderLayout({relative,con}) {
  const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
  const navigation = [
    { name: "Find Creators", href: "/creators" },
    { name: "For Fans", href: "/" },
    { name: "About Us", href: "/" },
    { name: "Company", href: "/" },
  ];
  const [SmallDevices, setScreen] = useState(false)

  useEffect(()=>{
    setScreen(window.innerWidth > 768 ? false : true)
  },[])


  const [anchorEl, setAnchorEl] = useState(null);
  const {userdata,setShow} = useContext(Store);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const route = useRouter()
  function handleSignOut() {
    signOut({
      redirect: false,
      // callbackUrl: `${process.env.NEXTAUTH_URL}/login`
    });
    localStorage.removeItem("meeting");
    localStorage.removeItem("token");
    setShow(false);

    route.push(`/login`);
   
  }

  return (
    <header className={`container ${relative ? relative : con ? "relative" : "absolute" }  inset-x-0 top-0 z-50`}>
      
      <nav
        className="flex items-center justify-between py-6"
        aria-label="Global"
      >
        <div className="flex lg:flex-1">
          <Link href="/" className="-m-1.5 p-1.5">
            {/* <span className="sr-only">Your Company</span> */}
            <img
            className={SmallDevices ? "w-28" : `w-48`}
              src={relative ? "../logo2.png" : "../images/logo.png"}
              alt="logo here"
            />
          </Link>
        </div>

        <div className="flex lg:hidden">
          <button
            type="button"
            className={`-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 ${relative ? "text-gray-300" : "text-gray-700"}`}
            onClick={() => setMobileMenuOpen(true)}
          >
            <span className="sr-only">Open main menu</span>
            <Bars3Icon className={`h-6 w-6 ${relative ? "text-black" : "text-gray-200"} `} aria-hidden="true" />
          </button>
        </div>
        <div className="hidden lg:flex lg:gap-x-12">
          <div className={`border rounded-full ${relative ? "border-black" : "border-white"}  px-1 py-1`}>
            {navigation.map((item,indexx) => (
              <Link
                onClick={() => setMobileMenuOpen(false)}
                key={indexx}
                href={item.href}
                className={`text-lg capitalize font-light leading-6 mr-3 ml-3 ${relative ? "text-black" : "text-white"}`}
              >
                {item.name}
              </Link>
            ))}
          </div>
        </div>
        {userdata?.info?.name ?  <div className="hidden lg:flex lg:flex-1 lg:justify-end items-center">
          <ul className="list-unstyled mb-0">
            <li className="dropdown pc-h-item header-user-profile">
              
              <Button
                id="basic-button"
                className="dropdown-toggle arrow-none me-0"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
              >
                
                {/* <div className="pc-head-link head-link-primary dropdown-toggle arrow-none me-0"> */}
                  <img src={userdata?.info?.profilePhoto ? `${process.env.NODE_ENV === "development" ? process.env.NEXTAUTH_URL : process.env.NEXTAUTH_URL + "/public"}/images/users/${userdata?.info?.profilePhoto}` : user.src} alt="user-image" className="user-avtar w-9 rounded-full object-cover h-9" />
                  <div className="flex flex-col justify-center text-left pl-2">
                  <span className={`text-lg capitalize leading-none ${relative ? "text-black" : "text-white"}`}>
                    {/* <i className="ti"><SettingsSuggestIcon/></i> */}
                    {userdata?.info?.name && userdata?.info?.name}
                  </span>
                  {userdata?.info?.role && <p className={`text-sm mb-0 capitalize ${relative ? "text-black" : "text-white"}`}>{userdata?.info?.role}</p>}
                  </div>
                {/* </div> */}
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                {/* <MenuItem onClick={handleClose}>
                  <div className="dropdown-header">
                    <h4>
                      Good Morning,{" "}
                      <span className="small text-muted"> John Doe</span>
                    </h4>
                    <p className="text-muted">Project Admin</p>
                    <hr />
                  </div>
                </MenuItem> */}
                {/* <MenuItem onClick={handleClose}>Account Settings</MenuItem> */}
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard">Dashboard</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard/chats">Chat Room</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/dashboard/bookedmeetings">Call Room</Link></MenuItem>
                <MenuItem onClick={() => setAnchorEl(null)}><Link className="text-gray-700" href="/profile">Edit profile</Link></MenuItem>
                <MenuItem onClick={handleSignOut}>Logout</MenuItem>
              </Menu>
            </li>
          </ul>
        </div> : <div className="hidden lg:flex lg:flex-1 lg:justify-end items-center">
          <Link
            href="/login"
            className={`text-lg capitalize font-light leading-6 ${relative ? "text-black" : "text-white"}  mr-2 px-4 py-1 rounded-2xl`}
          >
            Log in

          </Link>
          <Link
            href="/signup"
            className={`text-lg capitalize font-medium leading-6 ${relative ? "bg-black text-white" : "text-black bg-white"}  px-4 py-1 rounded-2xl`}
          >
            Sign up
          </Link>

          
        </div>}
       
      </nav>
      <Dialog
        as="div"
        className="lg:hidden"
        open={mobileMenuOpen}
        onClose={setMobileMenuOpen}
      >
        <div className="fixed inset-0 z-50" />
        <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
          <div className="flex items-center justify-between">
            <a href="#" className="-m-1.5 p-1.5">
              <span className="sr-only">Your Company</span>
              <img
                className="h-8 w-auto"
                src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                alt=""
              />
            </a>
            <button
              type="button"
              className="-m-2.5 rounded-md p-2.5 text-gray-700"
              onClick={() => setMobileMenuOpen(false)}
            >
              <span className="sr-only">Close menu</span>
              <XMarkIcon className="h-6 w-6" aria-hidden="true" />
            </button>
          </div>
          <div className="mt-6 flow-root">
            <div className="-my-6 divide-y divide-gray-500/10">
              <div className="space-y-2 py-6">
                {navigation.map((item) => (
                  <a
                    key={item.name}
                    href={item.href}
                    className={`-mx-3 block rounded-lg px-3 py-2 text-base  leading-7 ${ relative ? "text-gray-300 hover:bg-gray-900" : "text-gray-900 hover:bg-gray-50"}`}
                  >
                    {item.name}
                  </a>
                ))}
              </div>
              <div className="py-6">
                <Link
                  href="/login"
                  className={`-mx-3 block rounded-lg px-3 py-2.5 text-base  leading-7 ${ relative ? "text-gray-60 hover:bg-gray-900" : "text-gray-900 hover:bg-gray-50" } `}
                >
                  Log in
                </Link>
              </div>
            </div>
          </div>
        </Dialog.Panel>
      </Dialog>
    </header>
  );
}

export default HeaderLayout;
