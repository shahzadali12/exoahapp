import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function Models({show, setShow,heading,data,setShowlog}) {
//   const [show, setShow] = useState(false);

//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);

  return (
    <>
      

      <Modal
        backdrop="static"
        className={setShowlog ? "z-[999]" : ""}
        keyboard={false} show={show} onHide={()=> setShow({show:false})}>
          {setShowlog && <span className='text-2xl absolute top-1 right-2 cursor-pointer' onClick={()=> setShowlog(false)}><i className='fa fa-close'></i></span>}
        <Modal.Header>
          <Modal.Title>{heading}</Modal.Title>
          
        </Modal.Header>

        <Modal.Body>{data}</Modal.Body>
        {/* <Modal.Footer>
          <Button variant="secondary" onClick={()=> setShow(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={()=> setShow(false)}>
            Save Changes
          </Button>
        </Modal.Footer> */}
      </Modal>
    </>
  );
}

export default Models;