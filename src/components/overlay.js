import React,{useRef,useEffect, useState} from 'react'
import Overlay from "react-bootstrap/Overlay";
import Chatbox from './chatbox';
import { toast } from "react-toastify";
import BookingSlot from './bookslot';
function Overlays({states, setStates,target,creator}) {
   
  const [show, setShow] = useState(false);
  const [nofication, setnofication] = useState(false);

  const [SmallDevices, setScreen] = useState(false);
  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);


  return (
    <Overlay
    target={target?.current}
    show={states?.menuStatus === "close" ? false : true}
  >
    {({ show: _show, ...props }) => (
      <div
        className={`RTL_menu ${states?.avaliable ? "extraclass" : "min-h-40 max-h-[450px]"} ${show ? '-z-0' : "z-[999]"}
          ${states?.menuStatus}`}
      >
        {nofication && <h3 className='bg-green-400 text-white w-full text-center py-2 flex justify-center items-center relative -top-5'>Successfully your slot booked!</h3>}
        <div className='relative flex justify-center items-center mb-4'>
        <button
          className={`absolute left-0 ${SmallDevices ? "text-base" : "text-xl"} font-bold text-blue-950`}
          onClick={() => setStates({...states, menuStatus: "close" })}
        >
          Go Back <i className="fa fa-chevron-left"></i>
        </button>

       {!states?.avaliable ? <div className="text-xl border-2 px-3 py-1 rounded-md font-bold border-gray-300">Start Chatting</div> : <div className={`${SmallDevices ? "text-lg" : "text-xl"} border-2 px-3 py-1 rounded-md font-bold border-gray-300`}>Book 1 On 1 eMeeting</div>}
        </div>
        <div className="content_area">
         {states?.avaliable ? <>
          <h4>Standard Time <span>Meeting Duration</span></h4>
          </> : null}

          {states?.avaliable ? <BookingSlot creator={creator} states={states} setStates={setStates} slots={states?.data} nofication={nofication} setnofication={setnofication} /> : <Chatbox creator={creator} show={show} setShow={setShow}  />}
          
        </div>
        
        
      </div>
    )}
  </Overlay>
  )
}

export default Overlays