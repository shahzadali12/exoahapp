import { Button, CircularProgress, Stack } from "@mui/material";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { useState, useEffect, useContext } from "react";

import { Store } from "../../store/AppStore";

function MessagePayment({ setShow,creator }) {

  const {paymentSuccess, setPaymentuccess,PaymentDetection, Selected_creator} = useContext(Store)

  const stripe = useStripe();
  const elements = useElements();
  const [error, setError] = useState(null);
  const [loader, setLoader] = useState(false);
  const handleCardChange = (event) => {
    if (event.error) {
      setError(event.error.message);
    } else {
      setError(null);
    }
  };

  const handleSubmit = async (event) => {
    setLoader(true);
    event.preventDefault();
    if (!stripe || !elements) {
      return;
    }

    const result = await stripe.createPaymentMethod({
      type: "card",
      card: elements.getElement(CardElement),
    });

    if (result.error) {
      setError(result.error.message);
      setLoader(true);

    } else {
   

      let amountWithDecimal = (creator?.prices?.messages?.chat / 100).toFixed(2);


if (amountWithDecimal.split('.')[1].length === 1) {
    amountWithDecimal += '0';
}

      const datas= {
        paymentMethodId: result.paymentMethod.id,
        amount: amountWithDecimal
      }
      const response = await fetch("/api/payment/stripe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(datas),
      });
      if(response?.ok){

        // setPaymentuccess(response)
        await PaymentDetection()
        setShow(false)
        setLoader(false);
      }else{
        setLoader(false);
        const errorData = await response.json();
        setError(errorData.error);
        setLoader(true);
      }
      // await handlePaymentOnServer(paymentMethodId);
    }
  };

  

  return (
    <>

        <form onSubmit={handleSubmit}>
          <label style={{ display: "block", marginBottom: "8px" }}>
            <CardElement
              options={{
                style: {
                  base: {
                    fontSize: "16px",
                  },
                  invalid: {
                    color: "#9e2146",
                  },
                },
                hidePostalCode: true,
              }}
              onChange={handleCardChange}
            />
          </label>
          <div style={{ marginTop: "1rem" }}>
            {error && <div style={{ color: "red" }}>{error}</div>}
          </div>

          <Stack>
              {loader ? (
                <div className="flex justify-center items-center">
                  <CircularProgress />
                  
                </div>
              ) : (
                <Button
                  variant="text"
                  type="submit"
                  disabled={!stripe}
                  className="btn bg-secondary text-white"
                >
                  Pay
                </Button>
             
              )}
            </Stack>

          
        </form>
     
    </>
  );
}

export default StripePayment;
