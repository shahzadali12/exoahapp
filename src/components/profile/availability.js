import moment from "moment";
import React, { useContext, useEffect, useMemo, useState } from "react";
import DatePicker from "react-datepicker";
import { alpha, styled } from "@mui/material/styles";
import { pink } from "@mui/material/colors";
import Select from "react-select";
import Switch from "@mui/material/Switch";
import axios from "axios";
import { useSession } from "next-auth/react";
import { toast } from "react-toastify";
import { Store } from "../../store/AppStore";
import {
  Checkbox,
  FormControlLabel,
  FormGroup,
  Radio,
  RadioGroup,
} from "@mui/material";

import IndeterminateCheckbox from "../selectTime";

const Availability = ({
  setValue,
  days,
  setDays,
  propss,
  buffer,
  setBufferTime,
  checkeds,
  setChecked,
  Values,
}) => {
  const [selectedDateTime, setSelectedDateTime] = useState([]);
  const session = useSession();

  const { userdata, setApiload, Timeoptions } = useContext(Store);

  const handleDateTimeChange = (value) => {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    const currentDate = {
      date: value.getDate(),
      month: value.getMonth() + 1,
      year: value.getFullYear(),
      hour: value.getHours(),
      minute: value.getMinutes(),
      second: value.getSeconds(),
      monthName: months[value.getMonth() + 1],
      full: `${value}`,
    };
    // if(value.getHours() > 0){
    //     if(value.getMinutes() > 0){
    setSelectedDateTime([...selectedDateTime, currentDate]);

    //     }
    // }
  };

  const IOSSwitch = styled((props) => (
    <Switch
      focusVisibleClassName=".Mui-focusVisible"
      disableRipple
      {...props}
    />
  ))(({ theme }) => ({
    width: 42,
    height: 26,
    padding: 0,
    "& .MuiSwitch-switchBase": {
      padding: 0,
      margin: 2,
      transitionDuration: "300ms",
      "&.Mui-checked": {
        transform: "translateX(16px)",
        color: "#fff",
        "& + .MuiSwitch-track": {
          backgroundColor:
            theme.palette.mode === "dark" ? "#2ECA45" : "#65C466",
          opacity: 1,
          border: 0,
        },
        "&.Mui-disabled + .MuiSwitch-track": {
          opacity: 0.5,
        },
      },
      "&.Mui-focusVisible .MuiSwitch-thumb": {
        color: "#33cf4d",
        border: "6px solid #fff",
      },
      "&.Mui-disabled .MuiSwitch-thumb": {
        color:
          theme.palette.mode === "light"
            ? theme.palette.grey[100]
            : theme.palette.grey[600],
      },
      "&.Mui-disabled + .MuiSwitch-track": {
        opacity: theme.palette.mode === "light" ? 0.7 : 0.3,
      },
    },
    "& .MuiSwitch-thumb": {
      boxSizing: "border-box",
      width: 22,
      height: 22,
    },
    "& .MuiSwitch-track": {
      borderRadius: 26 / 2,
      backgroundColor: theme.palette.mode === "light" ? "#E9E9EA" : "#39393D",
      opacity: 1,
      transition: theme.transitions.create(["background-color"], {
        duration: 500,
      }),
    },
  }));



  const generateTimeSlots = (
    startTime,
    endTime,
    interval,
    breakInterval,
    count
  ) => {
    const timeSlots = [];
    let currentTime = new Date(startTime);

    while (currentTime < endTime) {
      timeSlots.push(new Date(currentTime));
      currentTime = new Date(currentTime.getTime() + interval * 60000);
      currentTime = new Date(currentTime.getTime() + breakInterval * 60000);
    }

    let finalResult = [];
    if (timeSlots.length > 0) {
      for (let index = 0; index < timeSlots.length; index++) {
        const element = timeSlots[index];
        const result = calculateOccurrences(element, count);
        for (let index = 0; index < result.length; index++) {
          const element = result[index];
          finalResult.push({ date: element, booked: false });
        }
      }
    }
    const finalResults = {
      interval: interval.pop(),
      result: finalResult,
    };
    return finalResults;
  };

  const calculateOccurrences = (date, count) => {
    const dayOfWeek = date.getDay();
    const currentDate = new Date();

    const occurrencesArray = [];

    for (let i = 0; i <= count; i++) {
      const year = currentDate.getFullYear();
      const month = currentDate.getMonth() + i;
      const firstDayOfMonth = new Date(year, month, 1);
      const lastDayOfMonth = new Date(year, month + 1, 0);

      for (
        let d = firstDayOfMonth;
        d <= lastDayOfMonth;
        d.setDate(d.getDate() + 1)
      ) {
        if (d.getDay() === dayOfWeek && d >= currentDate) {
          const occurrenceWithSameTime = new Date(d);
          occurrenceWithSameTime.setHours(date.getHours());
          occurrenceWithSameTime.setMinutes(date.getMinutes());
          occurrencesArray.push(occurrenceWithSameTime);
        }
      }
    }

    return occurrencesArray;
  };

  const getNextDateByDayAndTime = (dayName, hours, minutes) => {
    const today = new Date();
    const dayIndex = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ].indexOf(dayName);
    let nextDate = new Date(today);
    while (nextDate.getDay() !== dayIndex) {
      nextDate.setDate(nextDate.getDate() + 1);
    }
    if (nextDate < today) {
      nextDate.setDate(nextDate.getDate() + 7);
    }
    nextDate.setHours(hours, minutes, 0, 0);
    return nextDate;
  };

  const handleStartDateChange = (date, value, index) => {
    const slotIndex = days.findIndex((slot) => slot.name === value);
    if (slotIndex !== -1) {
      const updatedTimeSlots = [...days];
      const hours = date.getHours();
      const minutes = date.getMinutes();

      const today = getNextDateByDayAndTime(value, hours, minutes);

      updatedTimeSlots[slotIndex] = {
        ...updatedTimeSlots[index],
        startTime: today,
      };

      setDays(updatedTimeSlots);

      let arr = [];
      for (let key in checkeds) {
        arr.push(checkeds[key]);
      }
      const result = arr.filter(
        (val) =>
          val != 0 &&
          arr.length > 3 &&
          val != 100 &&
          val != undefined &&
          val != null
      );

      // setValue("available", updatedTimeSlots);

      setValue("brackTime", buffer?.label);
      setValue("durationTime", result);
    }
  };

  const handleEndDateChange = (date, value, index) => {
    const slotIndex = days.findIndex((slot) => slot.name === value);
    if (slotIndex !== -1) {
      const updatedTimeSlots = [...days];
      const hours = date.getHours();
      const minutes = date.getMinutes();

      const today = getNextDateByDayAndTime(value, hours, minutes);

      let arr = [];
      for (let key in checkeds) {
        arr.push(checkeds[key]);
      }
      const result = arr.filter(
        (val) =>
          val != 0 &&
          arr.length > 3 &&
          val != 100 &&
          val != undefined &&
          val != null
      );

      // if (Values?.durationTime) {
      const slots = result?.map((val) => {
        return generateTimeSlots(
          updatedTimeSlots[slotIndex].startTime,
          today,
          [val],
          buffer?.label,
          1
        );
      });

      console.log(slots, "asdasd")

      const finalRecord = slots.map((valuess) => {
        const data =
          valuess.interval === 15
            ? getNewestRecord(valuess.result)
            : valuess.interval === 30
            ? getNewestRecord(valuess.result)
            : valuess.interval === 45
            ? getNewestRecord(valuess.result)
            : null;

        const final = {
          slots: valuess.interval,
          result: data,
        };
        return final;
      });

      updatedTimeSlots[slotIndex] = {
        ...updatedTimeSlots[slotIndex],
        events: finalRecord,
        endTime: today,
      };

      setDays(updatedTimeSlots);

      // setValue("available", updatedTimeSlots);

      setValue("brackTime", buffer?.label);
      setValue("durationTime", result);
    }
  };

  // const handleSaveSetting = async () => {
  //   let arr = [];
  //   for (let key in checkeds) {
  //     arr.push(checkeds[key]);
  //   }
  //   const result = arr.filter(
  //     (val) => val != 0 && arr.length > 3 && val != 100
  //   );

  //   const data = {
  //     bufferTime: buffer.value,
  //     durationTime: result,
  //     email: session?.data?.user?.email,
  //   };

  //   const responce = await axios
  //     .put(`${process.env.NEXTAUTH_URL}/api/authenticated/users/settings`, data)
  //     .then((response) => {
  //       setApiload(response);
  //       toast.success("Settings Updated Successfully");

  //       return response?.data;
  //     })
  //     .catch((error) => {
  //       toast.error("settings error");
  //     });
  // };

  // const handleGenerateSlots = (name) => {
  //   const slotIndex = days.findIndex((slot) => slot.name === name);
  //   if (slotIndex !== -1) {
  //     const updatedTimeSlots = [...days];
  //     const data = calculateOccurrences(updatedTimeSlots[slotIndex].startTime,1);

  //     updatedTimeSlots[slotIndex] = {
  //       ...updatedTimeSlots[slotIndex],
  //       record: data,
  //     };
  //     setDays(updatedTimeSlots);
  //     console.log(updatedTimeSlots, "here is current values");
  //   }
  // };

  const customstyles = {
    control: (base) => ({
      ...base,
      height: 42,
      minheight: 42,
    }),
  };

  const RemoveItem = (vale, vaaa, index) => {
    const result = [...days];

    const slotIndex = days.findIndex((slot) => slot.name === vaaa.name);
    if (slotIndex !== -1) {
      const updatedTimeSlots = [...days];

      updatedTimeSlots[slotIndex].events.filter((filterValue) => {
        return (
          filterValue?.slots === vale.slots &&
          filterValue.result.splice(index, 1)
        );
      });
      setDays(updatedTimeSlots);
      // setValue("available", updatedTimeSlots);
    }
  };


  function removeDuplicates(arr, prop) {
    const uniqueObjects = {};
    const result = [];

    for (const obj of arr) {
      if (!uniqueObjects[obj[prop]]) {
        result.push(obj);
        uniqueObjects[obj[prop]] = true;
      }
    }

    return result;
  }

  const getRecordsByDateTime = (dataArray) => {
    // const filteredRecords = dataArray.filter((record) => {
    // const recordDateTime = new Date(record); // Assuming dateTime is in format YYYY-MM-DDTHH:MM:SS

    // Convert date strings to Date objects
    // const dates = dataArray.map((dateString) => new Date(dateString));

    // Get current date and time
    const currentDate = new Date();

    // console.log(dataArray, "all data")
    // Filter dates that are after the current date and time
    const futureDates = dataArray.filter((date) => date?.date > currentDate);

    // Sort the future dates array
    futureDates.sort((a, b) => a - b);

    // Get the very next date and time

    const filter = futureDates.filter(
      (date) => date?.date?.getDate() === futureDates[0]?.date?.getDate()
    );

    // console.log(removeDuplicates(filter, "date"), "all data dublicate")
    // Check if record's date and time match the provided dateTime
    // return recordDateTime.toDateString() === dateTime; // Compare only up to minutes
    return dataArray 
    // });

    // console.log(filteredRecords, "here is a record")
    // return filteredRecords;
  };

  const renderRecord = (dataArrasy) => {
    const currentDate = new Date();


    const futureDates = dataArrasy.filter((date) =>  new Date(date?.date) > currentDate);

    
    futureDates.sort((a, b) => a - b);

    const current = new Date(futureDates[0].date)


    const filter = futureDates.filter(
      (date) => new Date(date?.date).getDate() === current.getDate()
    );

    return filter
  }

  const getNewestRecord = (dateTimeArray) => {
    // const dates = dateTimeArray.map(
    //   (dateTimeStrings) =>
    //     // new Date(dateTimeStrings).toDateString()
    //     new Date(dateTimeStrings)
    // );

    // const [uniqueDates] = Array.from(new Set(dates));

    const matchedRecords = getRecordsByDateTime(dateTimeArray);

    return matchedRecords;
  };

  const handleChange = (eve, val) => {
    const slotIndex = days.findIndex((slot) => slot.name === val?.name);
    if (slotIndex !== -1) {
      const updatedTimeSlots = [...days];
      updatedTimeSlots[slotIndex] = {
        ...updatedTimeSlots[slotIndex],
        checked: eve.target.checked,
      };
      setDays(updatedTimeSlots);
      let arr = [];
      for (let key in checkeds) {
        arr.push(checkeds[key]);
      }
      const result = arr.filter(
        (val) =>
          val != 0 &&
          arr.length > 3 &&
          val != 100 &&
          val != undefined &&
          val != null
      );

      // setValue("available", updatedTimeSlots);

      setValue("brackTime", buffer.value);
      setValue("durationTime", result);
    }
  };

  const handleChangeRepeat = (eve, val) => {
    const slotIndex = days.findIndex((slot) => slot.name === val?.name);
    if (slotIndex !== -1) {
      const updatedTimeSlots = [...days];

      if (Values?.durationTime) {
        const slots = Values?.durationTime?.map((val) => {
          return generateTimeSlots(
            updatedTimeSlots[slotIndex].startTime,
            updatedTimeSlots[slotIndex].endTime,
            [val],
            buffer?.label,
            3
          );
        });

        if (eve.target.checked) {
          const finalRecord = slots.map((valuess) => {
            const data =
              valuess.interval === 15
                ? getNewestRecord(valuess.result)
                : valuess.interval === 30
                ? getNewestRecord(valuess.result)
                : valuess.interval === 45
                ? getNewestRecord(valuess.result)
                : null;
            const final = {
              slots: valuess.interval,
              result: data,
            };
            return final;
          });

          updatedTimeSlots[slotIndex] = {
            ...updatedTimeSlots[slotIndex],
            repeat: eve.target.checked,
            record: finalRecord,
          };

          setDays(updatedTimeSlots);
          let arr = [];
          for (let key in checkeds) {
            arr.push(checkeds[key]);
          }
          const result = arr.filter(
            (val) =>
              val != 0 &&
              arr.length > 3 &&
              val != 100 &&
              val != undefined &&
              val != null
          );

          // setValue("available", updatedTimeSlots);

          setValue("brackTime", buffer.value);
          setValue("durationTime", result);
        } else {
          updatedTimeSlots[slotIndex] = {
            ...updatedTimeSlots[slotIndex],
            repeat: eve.target.checked,
            record: [],
          };
          let arr = [];
          for (let key in checkeds) {
            arr.push(checkeds[key]);
          }
          setDays(updatedTimeSlots);
          const result = arr.filter(
            (val) =>
              val != 0 &&
              arr.length > 3 &&
              val != 100 &&
              val != undefined &&
              val != null
          );

          // setValue("available", updatedTimeSlots);

          setValue("brackTime", buffer.value);
          setValue("durationTime", result);
        }
      }

      // const slots = generateTimeSlots(
      //   updatedTimeSlots[slotIndex].startTime,
      //   updatedTimeSlots[slotIndex].endTime,
      //   durationTime[0]?.label,
      //   buffer[0]?.label,
      //   3
      // );
    }
  };

  function passDate(date) {
    // if(!Number.isNaN(new Date(date))){
    const dateObj = new Date(date);
    //   return dateObj
    // }else{

    return date;

    // }
  }

  return (
    // <div>
    //   <DatePicker
    //     // multiple
    //     value={selectedDateTime}
    //     // selected={selectedDateTime}
    //     onChange={handleDateTimeChange}

    //     timeFormat="hh:mm aa"
    //     excludeTimes={disabledValue}
    //     showTimeSelect
    //     // filterTime={filterPassedTime}
    //     timeIntervals={bufferMinutes}
    //     // minTime={new Date().setHours(0, 0)} // Set minimum time
    //     // maxTime={new Date().setHours(23, 59)} // Set maximum time
    //     // inline
    //     // shouldCloseOnSelect={false}
    //   />
    //   <div className="flex justify-start items-center flex-wrap">
    //     {selectedDateTime &&
    //       selectedDateTime.map((time, index) => (
    //         <div
    //           className="selection_Blog bg-white p-3 rounded-md  mb-3 mr-3"
    //           key={index}
    //         >
    //           <div className="flex justify-between items-center mb-3">
    //             <span>{moment(time.full).format("MMM dddd YY h:mm:ss a")}</span>
    //             {/* <ul className="flex justify-end items-center btn_area w-auto">
    //               <li>
    //                 <button onClick={() => RemoveItem(time,index)} type="button">Remove</button>
    //               </li>
    //               <li>
    //                 <button type="button">PM</button>
    //               </li>
    //             </ul> */}
    //             <button
    //               onClick={() => RemoveItem(time, index)}
    //               type="button"
    //               className="bg-red-600 text-white px-2 py-1 rounded-md text-sm"
    //             >
    //               Remove
    //             </button>
    //           </div>
    //           {/* <div className="date_content mb-3 w-full">
    //             <ul>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //               <li>
    //                 <button>
    //                   <span>06</span>
    //                   <span>PM</span>
    //                 </button>
    //               </li>
    //             </ul>
    //           </div> */}
    //           <div className="flex justify-between items-center w-full">
    //             <span>
    //               <IOSSwitch className="mr-2" color="secondary" />
    //               Repeat Every Wednesday ?
    //             </span>
    //           </div>
    //         </div>
    //       ))}
    //   </div>
    // </div>
    <>
      <div className="head flex justify-between items-start">
        <div className="heading float-left w-2/6 mb-4">
          <h3 className="text-3xl mb-0 font-semibold">
            {propss}Business hours
          </h3>
          <p className="mb-0">Control your availability</p>
        </div>
      </div>

      <div className=" flex justify-between flex-col w-full">
        <div className="flex justify-between items-start">
          {/* <div className="flex justify-between flex-col mb-4 w-2/6">
            <h6 className="mb-0 text-xl font-semibold">Enable</h6>
            <span className="">Quickly enable or disable business hours</span>
          </div> */}
          <div className="flex justify-around items-center w-full mb-4">
            <div className={` relative z-50 w-2/6 mr-4`}>
              <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-800 text-sm rounded-lg">
                Set Break time
              </label>
              <Select
                options={Timeoptions}
                styles={customstyles}
                onChange={(en) => setBufferTime(en)}
                value={buffer}
              />
            </div>
            <IndeterminateCheckbox
              checkeds={checkeds}
              setChecked={setChecked}
            />
            {/* <div className={` relative z-50 mb-3 w-2/5 mr-4`}>
              <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-800 text-sm rounded-lg">
                Set Duration time
              </label>
              <Select
                options={options}
                styles={customstyles}
                onChange={(en) => setDurationTime(en)}
                value={durationTime}
              />
            </div> */}
            {/* <div className="float-left w-1/4">
              <button
                onClick={handleSaveSetting}
                className="btn bg-secondary text-white m-0 w-full"
                type="button"
              >
                Save availability Settings
              </button>
            </div> */}
          </div>
        </div>
        <div className="w-full float-left">
          {days.map((val, index) => {
            return (
              <div
                className="flex justify-between w-full mb-3 items-start"
                key={index}
              >
                <span className="w-2/4">
                  <IOSSwitch
                    className="mr-2"
                    color="secondary"
                    onChange={(eve) => handleChange(eve, val)}
                    checked={val?.checked}
                  />
                  {val.name}
                </span>

                <div className="flex justify-between items-center w-full">
                  <span>
                    <IOSSwitch
                      className="mr-2"
                      color="secondary"
                      onChange={(eve) => handleChangeRepeat(eve, val)}
                      checked={val?.repeat}
                    />
                    Repeat Every {val.name} ?
                  </span>
                  {/* <p>for six month</p> */}
                </div>

                {val?.checked ? (
                  <>
                    {" "}
                    <div className="flex justify-between flex-col w-1/2">
                      <div className="flex justify-between w-full">
                        <div className="flex justify-items-stretch  w-screen bg-white rounded-md px-2 mr-4 border border-blue-400 relative">
                          <h6 className="capitalize text-lg text-gray-800 mb-0 w-auto absolute left-3">
                            From{" "}
                            {!val?.startTime
                              ? ""
                              : moment(val?.startTime).format("hh:mm a")}
                          </h6>

                          <DatePicker
                            // showIcon={true}
                            // selected={moment(val?.startTime)}
                            className="bg-transparent text-right text-lg w-full"
                            onChange={(event) =>
                              handleStartDateChange(event, val?.name, index)
                            }
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={buffer.label ? buffer.label : 30}
                            dateFormat="h:mm aa"
                          />
                        </div>
                        <div className="flex justify-items-stretch w-screen bg-white rounded-md px-2 mr-4 border border-blue-400 relative">
                          <h6 className="capitalize text-lg text-gray-800 mb-0 w-auto absolute left-3">
                            to{" "}
                            {!val?.endTime
                              ? ""
                              : moment(val?.endTime).format("hh:mm a")}
                          </h6>

                          <DatePicker
                            // selected={val?.endTime}
                            className="bg-transparent text-right text-lg w-full"
                            onChange={(event) =>
                              handleEndDateChange(event, val?.name, index)
                            }
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={buffer.label ? buffer.label : 30}
                            dateFormat="h:mm aa"
                          />
                        </div>
                        {/* <div className="plus p-2 ml-2">
                      <button
                        type="button"
                        onClick={() => handleGenerateSlots(val?.name)}
                      >
                        <i className="fa fa-plus" aria-hidden="true"></i>
                      </button>
                    </div> */}
                      </div>
                      <div className="py-3 flex justify-start items-center flex-wrap w-full">
                        {val?.events &&
                          val?.events.map((vel, indexss) => {
                            return (
                              <div
                                key={indexss}
                                className="flex justify-between items-center flex-col w-full"
                              >
                                <h3 className="w-full bg-slate-700 text-white float-left rounded-md px-2 py-2 text-center">
                                  {vel.slots === 15
                                    ? "15 minutes"
                                    : vel.slots === 30
                                    ? "30 minutes"
                                    : vel.slots === 45
                                    ? "45 minutes"
                                    : null}
                                </h3>
                                <div className="flex justify-between items-center py-1 px-2 rounded-md mb-2 text-white mr-2 flex-wrap w-full">
                                  {vel?.result && renderRecord(vel?.result).map((vele, index12) => {
                                    return (
                                      <div
                                        key={index12}
                                        className="flex justify-between items-center py-1 bg-slate-800 mb-3 pl-2 pr-2 rounded-md text-white mr-1"
                                      >
                                        <div className="flex justify-start items-center w-full">
                                          <span className="">
                                            {moment(vele.date).format("hh:mm")}
                                          </span>
                                          <span className="px-1">-</span>
                                          <span>
                                            {moment(vele.date)
                                              .add(vel.slots, "minutes")
                                              .format("hh:mm")}
                                          </span>
                                        </div>

                                        <div className="flex justify-end ml-2">
                                          <button
                                            type="button"
                                            onClick={() =>
                                              RemoveItem(vel, val, index12)
                                            }
                                            className="w-5 h-5 bg-red-500 rounded-sm"
                                          >
                                            <i
                                              className="fa fa-trash-o"
                                              aria-hidden="true"
                                            ></i>
                                          </button>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </>
                ) : (
                  <>
                    {" "}
                    <div className="flex justify-between flex-col w-1/2 disabled">
                      <div className="flex justify-between w-full ">
                        <div className="flex justify-items-stretch  w-screen bg-white rounded-md px-2 mr-4 border border-blue-400 relative">
                          <h6 className="capitalize text-lg text-gray-500 mb-0 w-auto absolute left-3">
                            From{" "}
                            {!val?.startTime
                              ? ""
                              : moment(val?.startTime).format("hh:mm a")}
                          </h6>
                          <DatePicker
                            // selected={val?.startTime}
                            className="bg-transparent text-right text-lg text-gray-400 w-full"
                            onChange={(event) =>
                              handleStartDateChange(event, val?.name, index)
                            }
                            disabled={"true"}
                            showTimeSelect
                            showTimeSelectOnly
                            // timeIntervals={
                            //   Values?.durationTime ? buffer.label : 30
                            // }
                            dateFormat="h:mm aa"
                          />
                        </div>
                        <div className="flex justify-items-stretch  w-screen bg-white rounded-md px-2 mr-4 border border-blue-400 relative">
                          <h6 className="capitalize text-lg text-gray-500 mb-0 w-auto absolute left-3">
                            to{" "}
                            {!val?.endTime
                              ? ""
                              : moment(val?.endTime).format("hh:mm a")}
                          </h6>

                          <DatePicker
                            // selected={val?.endTime}
                            className="bg-transparent w-full text-right text-lg text-gray-400 "
                            onChange={(event) =>
                              handleEndDateChange(event, val?.name, index)
                            }
                            disabled={true}
                            showTimeSelect
                            showTimeSelectOnly
                            // timeIntervals={
                            //   durationTime.label ? durationTime.label : 30
                            // }
                            dateFormat="h:mm aa"
                          />
                        </div>
                        {/* <div className="plus p-2 ml-2">
                      <button
                        type="button"
                        disabled={true}
                        onClick={() => handleGenerateSlots(val?.name)}
                      >
                        <i className="fa fa-plus" aria-hidden="true"></i>
                      </button>
                    </div> */}
                      </div>
                      <div className="py-3 flex justify-start items-center flex-wrap w-full">
                        {val?.events &&
                          val?.events.map((vel, indexs) => {
                            return (
                              <div
                                key={indexs}
                                className="flex justify-between items-center flex-col w-full"
                              >
                                <h3 className="w-full bg-gray-500 text-white float-left rounded-md px-2 py-2 text-center">
                                  {vel.slots === 15
                                    ? "15 minutes"
                                    : vel.slots === 30
                                    ? "30 minutes"
                                    : vel.slots === 45
                                    ? "45 minutes"
                                    : null}
                                </h3>
                                <div className="flex justify-between items-center py-1 px-2 rounded-md mb-2 text-white mr-2 flex-wrap w-full">
                                  {vel.result.map((vele, index123) => {
                                    return (
                                      <div
                                        key={index123}
                                        className="flex justify-between items-center py-1 bg-gray-500 mb-3 pl-2 pr-2 rounded-md text-white mr-1"
                                      >
                                        <div className="flex justify-start items-center w-full">
                                          <span className="">
                                            {moment(vele.date).format("hh:mm")}
                                          </span>
                                          <span className="px-1">-</span>
                                          <span>
                                            {moment(vele.date)
                                              .add(vel.slots, "minutes")
                                              .format("hh:mm")}
                                          </span>
                                        </div>

                                        <div className="flex justify-end ml-2">
                                          <button
                                            type="button"
                                            disabled={true}
                                            onClick={() =>
                                              RemoveItem(vel, val, index123)
                                            }
                                            className="w-5 h-5 bg-gray-500 rounded-sm"
                                          >
                                            <i
                                              className="fa fa-trash-o"
                                              aria-hidden="true"
                                            ></i>
                                          </button>
                                        </div>
                                      </div>
                                    );
                                  })}
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </>
                )}
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Availability;
