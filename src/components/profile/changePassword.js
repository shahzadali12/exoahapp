import React, { useState } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { useForm, Controller } from "react-hook-form";
import { schemas } from "../schemas/changepass";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-toastify";
import {
  Button,
  CircularProgress,
  Grid,
  Stack,
  TextField,
} from "@mui/material";

export default function ChangePasswor() {
  const { data } = useSession();
  const [loading, setLoading] = useState(false)
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const Onsubmit = async (values) => {
    setLoading(true)
    const alldata = {
      email: data?.user?.email,
      password: values.password,
      currentPassword: values.currentPassword,
    };
    const response = await fetch("/api/authenticated/password/changepass", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": `Bearer `,
      },
      body: JSON.stringify(alldata),
    });
    if (response.status === 201) {
      setLoading(false)
      toast.success("Your Password Successfully Changed!");
      reset()
    } else if (response.status === 401) {
      toast.error("Your Current Password is incorrect! please try again later");
      setLoading(false)
      reset()
    } else if (response.status === 402) {
      toast.error("You Already Changed Password.");
      setLoading(false)
      reset()
    } else {
      toast.error("Something was wrong");
      setLoader(false);
    }
  };
  return (
    <form onSubmit={handleSubmit(Onsubmit)}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            type="password"
            fullWidth
            {...register("currentPassword", { required: true })}
            label={errors.currentPassword ? "Current Password required!" : "Current Password"}
            error={errors.currentPassword ? true : false}
            id="outlined-error-helper-text"
            // helperText={errors.password ? "Incorrect entry." : ""} 
           
          />
        </Grid>
        <Grid item xs={12} sm={6} />
        <Grid item xs={12} sm={6}>
          <TextField
            type="password"
            fullWidth
           
            label={errors.password ? "New Password required!" : "New Password"}
            error={errors.password ? true : false}
            id="outlined-error-helper-text"
            // helperText={errors.password ? "Incorrect entry." : ""} 
            {...register("password", { required: true })}
            // defaultValue="30529399"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            type="password"
            fullWidth
            label={errors.confirm ? "Confirm Password required!" : "Confirm Password"}
            error={errors.confirm ? true : false}
            id="outlined-error-helper-text"
            // helperText={errors.password ? "Incorrect entry." : ""} 
            {...register("confirm", { required: true })}
            // defaultValue="395005"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Stack direction="row">
            {loading ? (
              <div className="flex justify-center items-center">
                <CircularProgress />
              </div>
            ) : (
              <Button
                variant="text"
                type="submit"
                disabled={loading}
                className="btn bg-secondary text-white"
              >
                Change Password
              </Button>
            )}
          </Stack>
        </Grid>
      </Grid>
    </form>
  );
}
