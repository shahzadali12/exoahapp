import React, { useContext, useEffect, useMemo, useState } from "react";
import {
  Grid,
  Typography,
  Stack,
  TextField,
  InputLabel,
  MenuItem,
  Box,
} from "@mui/material";
import ErrorTwoToneIcon from "@mui/icons-material/ErrorTwoTone";
import PhoneInput from "react-phone-input-2";
import { useForm, Controller } from "react-hook-form";
import { schemas } from "../schemas/extraUpdate";
import { yupResolver } from "@hookform/resolvers/yup";
import FormControl from "@mui/material/FormControl";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import { Store } from "../../store/AppStore";
import InputAdornment from "@mui/material/InputAdornment";
import XIcon from "@mui/icons-material/X";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import Select from "react-select";
import axios from "axios";
import { toast } from "react-toastify";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import Availability from "./availability";
import PricePlan from "./setPrice";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";

function EditInformation() {
  const options = [
    { value: "English", label: "English" },
    { value: "Urdu", label: "Urdu" },
    { value: "Punjabi", label: "Punjabi" },
  ];
  const {data:session} = useSession()
  const [loading, setLoading] = useState(false);
  const { userdata, Apiload, setApiload, category, Timeoptions } =
    useContext(Store);

    const route = useRouter()
  const {
    handleSubmit,
    register,

    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    // resolver: yupResolver(schemas),
    mode: "onChange",
  });
  const [Image, setImage] = useState(
    "https://stgapiv2.mentoga.com/assets/images/users/user.jpg"
  );
  const Values = getValues();
  const [state,setState] = useState('')
  const [voicecheckeds, setvoiceChecked] = React.useState({
    parent: 0,
    minutes_15: 0,
    minutes_30: 0,
    minutes_45: 0,
  });

  const [videocheckeds, setvideoChecked] = React.useState({
    parent: 0,
    minutes_15: 0,
    minutes_30: 0,
    minutes_45: 0,
  });

  const [uploadImge, setImageState] = useState(false);
  const uploadPicture = async (e) => {
    const file = e.target.files[0];

    var reader = new FileReader();
    reader.onload = async () => {
      const options = {
        maxSizeMB: 1,
        maxWidthOrHeight: 1920,
        useWebWorker: true,
      };
      // const compressedFile = await imageCompression(file, options);
      setImage(reader.result);
      setImageState(reader.result ? true : false);
    };
    reader.readAsDataURL(file);

    const formData = new FormData();
    formData.append("profileImg", file);

    // console.log(formData,file);
    await axios
      .post(`${process.env.NEXTAUTH_URL}/api/storeFile`, formData)
      .then(({ data }) => {
        setValue("image", data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [languages, setLanguage] = useState([]);
  const [categories, setCategories] = useState([]);
  const handleChange = (event) => {
    // setValue("language", event);
    setLanguage(event);
  };

  const [buffer, setBufferTime] = useState([]);

  const [checkeds, setChecked] = React.useState({
    parent: 0,
    minutes_15: 0,
    minutes_30: 0,
    minutes_45: 0,
  });
  const [days, setDays] = useState([
    {
      id: 1,
      name: "Monday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 2,
      name: "Tuesday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 3,
      name: "Wednesday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 4,
      name: "Thursday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 5,
      name: "Friday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 6,
      name: "Saturday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
    {
      id: 7,
      name: "Sunday",
      startTime: "",
      endTime: "",
      events: [],
      checked: false,
      repeat: false,
    },
  ]);
  useEffect(() => {
    if (userdata?.info?.email) {
      setValue("name",  userdata?.info?.name);
      setValue("email", userdata?.info?.email);
      setValue("aboutus", userdata?.info?.about);
      setValue("phone", userdata?.info?.phone);
      setValue("email", userdata?.info?.email);
      setValue("password", userdata?.info?.password);
      setValue("role", userdata?.info?.role);
      setValue("designation", userdata?.info?.designation);
      if (userdata?.info?.category) {
        setCategories(userdata?.info?.category);
      }
      setValue("shortinfo", userdata?.info?.shortinfo);
      if (userdata?.info?.profilePhoto) {
        setImage(`${process.env.NODE_ENV === "development" ? process.env.NEXTAUTH_URL : process.env.NEXTAUTH_URL + "/public" }/images/users/${userdata?.info?.profilePhoto}`);
        setValue("image.newFilename", userdata?.info?.profilePhoto);
      }
      if ( userdata?.info?.languages) {
        setLanguage(userdata?.info?.languages);
      }
      setValue("fblink", userdata?.links?.fb);
      setValue("instalink", userdata?.links?.insta);
      setValue("ldklink", userdata?.links?.ldklink);
      setValue("twlink", userdata?.links?.twlinks);




      if (userdata?.availability.break || userdata?.availability.slotDuration.length > 0) {
        const [bufferTime] =
          userdata?.availability?.break &&
          Timeoptions.filter(
            (val) =>
              parseInt(val.label) == parseInt(userdata?.availability?.break)
          );
        if (userdata?.availability?.break) {
          setBufferTime(bufferTime);
        }
  
        const min15 = userdata?.availability?.slotDuration.filter(
          (val) => val == 15
        );
        const min30 = userdata?.availability?.slotDuration.filter(
          (val) => val == 30
        );
        const min45 = userdata?.availability?.slotDuration.filter(
          (val) => val == 45
        );
  
        setChecked({
          ...checkeds,
          minutes_15: min15 ? 15 : 0,
          minutes_30: min30 ? 30 : 0,
          minutes_45: min45 ? 45 : 0,
        });
      }
      if(userdata?.availability?.times.length > 0){
        setDays(userdata?.availability?.times)
      }
     

      setValue("prices.messages.chat", userdata?.prices?.messages?.chat);
      setValue("prices.messages.audio", userdata?.prices?.messages?.audio);
      setValue("prices.messages.video", userdata?.prices?.messages?.video);

      setValue("prices.call.voice", userdata?.prices?.call?.voice);
      setValue("prices.call.video", userdata?.prices?.call?.video);


      // setValue("available", userdata?.availability?.times);

    
    }
  }, [userdata?.info?.email]);

  const [step, setStep] = useState(1);


  function FilterArray(mixvalue) {
    let arr = [];
    for (let key in mixvalue) {
      arr.push(mixvalue[key]);
    }
    const result = arr.filter(
      (val) =>
        val != 0 &&
        arr.length > 3 &&
        val != 100 &&
        val != undefined &&
        val != null
    );
    return result;
  }

  const onSubmit = async (data) => {
    const completed = {
      info: {
        name: data.name,
        profilePhoto: data.image?.newFilename,
        designation: data.designation,
        languages: languages,
        category: categories,
        role: data.role,
        slug: data.name.replaceAll(" ", "-").toLowerCase(),
        email: data.email,
        phone: data.phone,
        shortinfo: data.shortinfo,
        about: data.aboutus,
        password: data.password,
      },
      links: {
        fb: data.fblink,
        twlinks: data.twlink,
        insta: data.instalink,
        ldklink: data.ldklink,
      },
      availability: {
        break: buffer?.label,
        slotDuration: FilterArray(checkeds),
        times: days,
      },
      prices: {
        messages: {
          chat: data.prices.messages.chat,
          audio: data.prices.messages.chat,
          video: data.prices.messages.chat,
        },
        call: {
          voice: data.prices.call.voice,
          video: data.prices.call.video,
        },
      },
    };

    
    await axios.put(
        `${process.env.NEXTAUTH_URL}/api/authenticated/login/signup`,
        completed
      )
      .then((val) => {
        setApiload(val);
        toast.success("you profile updated successfully");
        if(session?.user.role !== "user"){
          route.push("/connect")
        }
        
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const customstyles = {
    control: (base) => ({
      ...base,
      height: 54,
      minheight: 54,
    }),
  };

 

  const RenderPage = ({ step }) => {
    if(session?.user.role !== "user"){

    
    switch (step) {
      case 1:
        return (
          <>
            <Grid item xs={12}>
              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <img
                    alt="User 1"
                    src={
                      uploadImge
                        ? Image
                        : `${process.env.NODE_ENV === "development" ? process.env.NEXTAUTH_URL : process.env.NEXTAUTH_URL + "/public"}/images/users/${userdata?.info?.profilePhoto}`
                    }
                    className="rounded-full w-20 h-20 object-center overflow-hidden"
                  />
                </Grid>
                <Grid item sm zeroMinWidth>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <Stack direction="row" spacing={2} alignItems="center">
                        <input
                          accept="image/*"
                          // style={{ display: "none" }}
                          onChange={uploadPicture}
                          id="contained-button-file"
                          // multiple
                          type="file"
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="caption">
                        <ErrorTwoToneIcon
                          sx={{
                            height: 16,
                            width: 16,
                            mr: 1,
                            verticalAlign: "text-bottom",
                          }}
                        />
                        Image size Limit should be 125kb Max.
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Name"
                id="outlined-size-small"
                // defaultValue={userdata?.name ? userdata?.name : ""}
                {...register("name", { required: true })}
                color={errors?.name ? "warning" : ""}
                style={{ backgroundColor: "white" }}
              />
            </Grid>

            {session?.user.role !== "user" && <> <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Designation"
                // defaultValue={userdata?.title ? userdata?.title : ""}
                {...register("designation", { required: true })}
                style={{ backgroundColor: "white" }}
                color={errors?.designation ? "warning" : ""}
                // defaultValue="company.ltd"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-[99999] mb-3 ${
                  errors.language ? "error" : ""
                }`}
              >
                <label className="absolute z-50 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600 text-sm">
                  Select language
                </label>
                <Select
                  isMulti
                  options={options}
                  styles={customstyles}
                  onChange={handleChange}
                  value={languages}
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-[99999] mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-50 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Select Categories
                </label>
                <Select
                  isMulti
                  options={category}
                  styles={customstyles}
                  className=""
                  // onChange={handleChange}
                  onChange={(event) => setCategories(event)}
                  value={categories}
                />
              </div>
            </Grid> </>}

            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                style={{ backgroundColor: "white" }}
                label="Email"
                // defaultValue={userdata?.email ? userdata?.email : ""}
                disabled
                {...register("email", { required: true })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={`form-floating mb-3 ${errors.phone ? "error" : ""}`}
              >
                <Controller
                  control={control}
                  name="phone"
                  rules={{ required: true }}
                  render={({ field: { ref, ...field } }) => (
                    <PhoneInput
                      {...field}
                      inputExtraProps={{
                        ref,
                        required: true,
                        autoFocus: true,
                      }}
                      country={"us"}
                      countryCodeEditable={false}
                      specialLabel={"Mobile Number"}
                    />
                  )}
                />
              </div>
            </Grid>

            {session?.user.role !== "user" && <><Grid item xs={12} sm={6}>
              <div
                className={` relative z-50 mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Short description
                </label>
                <textarea
                  // fullWidth
                  className="w-full h-28 resize-none border-2 border-solid border-gray-350 rounded-md p-2 placeholder-gray-500"
                  // label="About yourself"
                  placeholder="Short description"
                  style={{ backgroundColor: "white" }}
                  // defaultValue={userdata?.aboutus ? userdata?.aboutus : ""}
                  {...register("shortinfo", { required: true })}
                  // defaultValue="loram Analytics"
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-50 mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Type About yourself
                </label>
                <textarea
                  // fullWidth
                  className="w-full h-28 resize-none border-2 border-solid border-gray-350 rounded-md p-2 placeholder-gray-500"
                  // label="About yourself"
                  placeholder="Type anything about yourself.."
                  style={{ backgroundColor: "white" }}
                  // defaultValue={userdata?.aboutus ? userdata?.aboutus : ""}
                  {...register("aboutus", { required: true })}
                  // defaultValue="loram Analytics"
                />
              </div>
            </Grid></>}
          </>
        );
      case 2:
        return (
          <Grid item xs={12}>
            <h4 className="capitalize text-2xl font-semibold mb-4">
              <span
                className="mr-2 cursor-pointer"
                onClick={() => setStep(step == 1 ? 1 : step - 1)}
              >
                <ArrowBackIosNewIcon />
              </span>
              add social media links
            </h4>
            <Grid item xs={12} sm={12} className="mb-4">
              <TextField
                id="input-with-icon-textfield"
                // label="Facebook Link"
                className="w-full"
                {...register("fblink", { required: true })}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <FacebookIcon />
                    </InputAdornment>
                  ),
                }}
                variant="standard"
              />
            </Grid>
            <Grid item xs={12} sm={12} className="mb-4">
              <TextField
                id="input-with-icon-textfield"
                // label="twitter Link"
                className="w-full"
                {...register("twlink", { required: true })}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <XIcon />
                    </InputAdornment>
                  ),
                }}
                variant="standard"
              />
            </Grid>
            <Grid item xs={12} sm={12} className="mb-4">
              <TextField
                id="input-with-icon-textfield"
                {...register("instalink", { required: true })}
                className="w-full"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <InstagramIcon />
                    </InputAdornment>
                  ),
                }}
                variant="standard"
              />
            </Grid>
            <Grid item xs={12} sm={12} className="mb-4">
              <TextField
                id="input-with-icon-textfield"
                // label="linkedin Link"
                {...register("ldklink", { required: true })}
                className="w-full"
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LinkedInIcon />
                    </InputAdornment>
                  ),
                }}
                variant="standard"
              />
            </Grid>
          </Grid>
        );
      case 3:
        return (
          <Grid item xs={12}>
            <div className="float-left w-full">
              {/* <h4 className="capitalize text-2xl font-semibold mb-4"><span className="mr-2 cursor-pointer" onClick={() => setStep(step == 1 ? 1 : step - 1)}><ArrowBackIosNewIcon/></span>Set Time Schedule</h4> */}
              <Availability
                checkeds={checkeds}
                Values={Values}
                setChecked={setChecked}
                buffer={buffer}
                setBufferTime={setBufferTime}

                propss={
                  <span
                    className="mr-2 cursor-pointer"
                    onClick={() => setStep(step == 1 ? 1 : step - 1)}
                  >
                    <ArrowBackIosNewIcon />
                  </span>
                }
                days={days}
                setDays={setDays}
                setValue={setValue}
              />
            </div>
          </Grid>
        );
      case 4:
        return (
          <>
            <PricePlan
              Values={Values}
              setValue={setValue}
              watch={watch}
              props={register}
              propss={
                <span
                  className="mr-2 cursor-pointer"
                  onClick={() => setStep(step == 1 ? 1 : step - 1)}
                >
                  <ArrowBackIosNewIcon />
                </span>
              }
            />
          </>
        );
      default:
        <h2>Thank you for submitted content</h2>;
    }
  }else{
    switch (step) {
      case 1:
        return (
          <>
            <Grid item xs={12}>
              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <img
                    alt="User 1"
                    src={
                      uploadImge
                        ? Image
                        : `${process.env.NEXTAUTH_URL}/images/users/${userdata?.info?.profilePhoto}`
                    }
                    className="rounded-full w-20 h-20 object-center overflow-hidden"
                  />
                </Grid>
                <Grid item sm zeroMinWidth>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <Stack direction="row" spacing={2} alignItems="center">
                        <input
                          accept="image/*"
                          // style={{ display: "none" }}
                          onChange={uploadPicture}
                          id="contained-button-file"
                          // multiple
                          type="file"
                        />
                      </Stack>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="caption">
                        <ErrorTwoToneIcon
                          sx={{
                            height: 16,
                            width: 16,
                            mr: 1,
                            verticalAlign: "text-bottom",
                          }}
                        />
                        Image size Limit should be 125kb Max.
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Name"
                id="outlined-size-small"
                // defaultValue={userdata?.name ? userdata?.name : ""}
                {...register("name", { required: true })}
                color={errors?.name ? "warning" : ""}
                style={{ backgroundColor: "white" }}
              />
            </Grid>

            {session?.user.role !== "user" && <> <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                label="Designation"
                // defaultValue={userdata?.title ? userdata?.title : ""}
                {...register("designation", { required: true })}
                style={{ backgroundColor: "white" }}
                color={errors?.designation ? "warning" : ""}
                // defaultValue="company.ltd"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-[99999] mb-3 ${
                  errors.language ? "error" : ""
                }`}
              >
                <label className="absolute z-50 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600 text-sm">
                  Select language
                </label>
                <Select
                  isMulti
                  options={options}
                  styles={customstyles}
                  onChange={handleChange}
                  value={languages}
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-[99999] mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-50 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Select Categories
                </label>
                <Select
                  isMulti
                  options={category}
                  styles={customstyles}
                  className=""
                  // onChange={handleChange}
                  onChange={(event) => setCategories(event)}
                  value={categories}
                />
              </div>
            </Grid> </>}

            <Grid item xs={12} sm={6}>
              <TextField
                fullWidth
                style={{ backgroundColor: "white" }}
                label="Email"
                // defaultValue={userdata?.email ? userdata?.email : ""}
                disabled
                {...register("email", { required: true })}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={`form-floating mb-3 ${errors.phone ? "error" : ""}`}
              >
                <Controller
                  control={control}
                  name="phone"
                  rules={{ required: true }}
                  render={({ field: { ref, ...field } }) => (
                    <PhoneInput
                      {...field}
                      inputExtraProps={{
                        ref,
                        required: true,
                        autoFocus: true,
                      }}
                      country={"us"}
                      countryCodeEditable={false}
                      specialLabel={"Mobile Number"}
                    />
                  )}
                />
              </div>
            </Grid>

            {session?.user.role !== "user" && <><Grid item xs={12} sm={6}>
              <div
                className={` relative z-50 mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Short description
                </label>
                <textarea
                  // fullWidth
                  className="w-full h-28 resize-none border-2 border-solid border-gray-350 rounded-md p-2 placeholder-gray-500"
                  // label="About yourself"
                  placeholder="Short description"
                  style={{ backgroundColor: "white" }}
                  // defaultValue={userdata?.aboutus ? userdata?.aboutus : ""}
                  {...register("shortinfo", { required: true })}
                  // defaultValue="loram Analytics"
                />
              </div>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div
                className={` relative z-50 mb-3 ${
                  errors.categories ? "error" : ""
                }`}
              >
                <label className="absolute z-10 top-0 left-0 bg-white ml-2 -mt-2 px-2 text-gray-600  text-sm">
                  Type About yourself
                </label>
                <textarea
                  // fullWidth
                  className="w-full h-28 resize-none border-2 border-solid border-gray-350 rounded-md p-2 placeholder-gray-500"
                  // label="About yourself"
                  placeholder="Type anything about yourself.."
                  style={{ backgroundColor: "white" }}
                  // defaultValue={userdata?.aboutus ? userdata?.aboutus : ""}
                  {...register("aboutus", { required: true })}
                  // defaultValue="loram Analytics"
                />
              </div>
            </Grid></>}
          </>
        );
     
      default:
        <h2>Thank you for submitted content</h2>;
    }
  }

  };

 

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={2.5}>
        <RenderPage step={step} />

        {session?.user.role !== "user" ? step === 4 ? ( step == 1 ? null : step != 4 && (
            <Grid item xs={12} sm={6}>
              <div className="float-left">
                <Button
                  onClick={() => setStep(step == 1 ? 1 : step - 1)}
                  variant="text"
                  type="button"
                  className="btn bg-secondary text-white"
                >
                  Prev Step
                </Button>
              </div>
            </Grid>
          )
        ) : (
          <Grid item xs={12} sm={6}>
            <div className="float-left">
              <Button
                onClick={() => setStep(step == 1 ? step + 1 : step + 1)}
                variant="text"
                type="button"
                className="btn bg-secondary text-white"
              >
                next Step
              </Button>
            </div>
          </Grid>
        ) : null}
        {session?.user.role !== "user" ? step == 4 && <Grid item xs={12} sm={6}>
          <div className="float-left">
            <Stack>
              {loading ? (
                <div className="flex justify-center items-center">
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  variant="text"
                  type="submit"
                  disabled={loading}
                  className="btn bg-secondary text-white"
                >
                  Submit
                </Button>
              )}
            </Stack>
          </div>
        </Grid>: <Grid item xs={12} sm={6}>
          <div className="float-left">
            <Stack>
              {loading ? (
                <div className="flex justify-center items-center">
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  variant="text"
                  type="submit"
                  disabled={loading}
                  className="btn bg-secondary py-3 px-4 text-white"
                >
                  Update
                </Button>
              )}
            </Stack>
          </div>
        </Grid>}
        {/* <Grid item xs={12} sm={6}>
          <div className="float-left">
            <Button
              onClick={() => setStep(step == 1 ? 1 : step - 1)}
              variant="text"
              type="button"
              className="btn bg-secondary text-white"
            >
              Prev Step
            </Button>
          </div>
        </Grid> */}

        {/* <Grid item xs={12} sm={6}>
          <div className="float-left">
            <Stack>
              {loading ? (
                <div className="flex justify-center items-center">
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  variant="text"
                  type="submit"
                  disabled={loading}
                  className="btn bg-secondary text-white"
                >
                  Update
                </Button>
              )}
            </Stack>
          </div>
 
        </Grid> */}
      </Grid>
    </form>
  );
}

export default EditInformation;
