import {
  Checkbox,
  FilledInput,
  FormControl,
  FormControlLabel,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
} from "@mui/material";
import React, { useContext, useEffect, useMemo, useState } from "react";
import IndeterminateCheckbox from "../selectTime";
import { Store } from "../../store/AppStore";
function PricePlan({ propss,props,Values,watch}) {
  // const {amount, setAmount} = useContext(Store)


  
 
 

  // const handleAmountChange = (event, parentIndex, childIndex) => {
  //   const { value } = event.target;

  //   if (parentIndex === "messageamount") {
  //     // setAmount((prevState) => {
  //     //   const newState = { ...prevState };
  //     //   newState[parentIndex] = value;

  //     //   return newState;
  //     // });

  //     const newState = { ...amount };
  //     newState[parentIndex] = value;

  //     setAmount(newState);
  //   } else {
  //     // setAmount((prevState) => {
  //     //   const newState = { ...prevState };

  //     //   if (!newState[parentIndex]) {
  //     //     newState[parentIndex] = {};
  //     //   }

  //     //   if (!newState[parentIndex][childIndex]) {
  //     //     newState[parentIndex][childIndex] = {};
  //     //   }

  //     //   newState[parentIndex][childIndex] = {
  //     //     ...newState[parentIndex][childIndex],
  //     //     price: value,
  //     //     name: event.target.name,
  //     //   };

  //     //   return newState;
  //     // });

  //     const newState = { ...amount };

  //     if (!newState[parentIndex]) {
  //       newState[parentIndex] = {};
  //     }

  //     if (!newState[parentIndex][childIndex]) {
  //       newState[parentIndex][childIndex] = {};
  //     }

  //     newState[parentIndex][childIndex] = {
  //       ...newState[parentIndex][childIndex],
  //       price: value,
  //       name: event.target.name,
  //     };

  //     setAmount(newState);
  //   }
  // };

  // const handleCheckboxChange = (event, parentIndex, childIndex) => {
  //   const { checked } = event.target;

  //   setAmount((prevState) => {
  //     const newState = { ...prevState };

  //     if (!newState[parentIndex]) {
  //       newState[parentIndex] = {};
  //     }

  //     if (childIndex !== undefined) {
  //       newState[parentIndex][childIndex].check = checked;
  //     } else {
  //       newState[parentIndex] = checked;
  //     }

  //     return newState;
  //   });
   
  // };




  const wating15 = watch("prices.call.voice.min15.check")
  const wating30 = watch("prices.call.voice.min30.check")
  const wating45 = watch("prices.call.voice.min45.check")

  const watingv15 = watch("prices.call.video.min15.check")
  const watingv30 = watch("prices.call.video.min30.check")
  const watingv45 = watch("prices.call.video.min45.check")

  

  return (
    <>
      <div className="head flex justify-between items-start mt-4">
        <div className="icon"></div>
        <div className="heading float-left w-full mb-4">
          <h3 className="text-3xl mb-0 font-semibold">
            {propss}Set Pricing Plans
          </h3>
          <p className="mb-0">Control your price</p>
        </div>
      </div>
    
      <div className="flex justify-between items-center w-full border-b-[1px] border-gray-300 pb-3 mb-3">
        <div className="w-2/6">
          <h3 className="text-2xl mb-0 font-semibold capitalize">
            Set Message Rate
          </h3>
          <p className="mb-0">Test Message,Audio Message,video Message</p>
        </div>

        <div className="flex justify-end items-center w-1/4">
          <FormControl fullWidth sx={{ m: 1 }} variant="filled">
            <FilledInput
              id="filled-adornment-amount"
              // value={amount?.messageamount} // Set the value to the state variable
              // name="messageamount"
              {...props('prices.messages.chat')}
              // onChange={(event) => handleAmountChange(event, "messageamount")}
              // autoFocus={true}
            
              startAdornment={
                <InputAdornment position="start">$</InputAdornment>
              }
            />
          </FormControl>

          <span className="font-semibold text-lg ml-2 text-gray-700"></span>
        </div>
      </div>
      <div className="flex justify-between items-center w-full border-b-[1px] border-gray-300 pb-3 mb-3">
        <div className="w-2/6">
          <h3 className="text-2xl mb-0 font-semibold capitalize">
            Set Voice Call Rate
          </h3>
          <p className="mb-0">Voice call</p>
        </div>

        <div className=" flex justify-between flex-wrap">
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="15 minutes"
                control={
                  <Checkbox
                    // name="min15"
                    // checked={amount?.voiceamount?.min15?.check}
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "voiceamount", "min15")
                    // }
                    // checked={Values.voiceamount.min15.check}
                    defaultChecked={Values?.prices?.call?.voice?.min15?.check ? true : false}
                    {...props('prices.call.voice.min15.check')}
                   
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.voiceamount?.min15?.price} // Set the value to the state variable
                // name="15"
                // autoFocus={true}
                // onChange={(event) =>
                //   handleAmountChange(event, "voiceamount", "min15")
                // }
                disabled={wating15 ? false : true}
                {...props('prices.call.voice.min15.price')}
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="30 minutes"
                control={
                  <Checkbox
                    // name="min30"
                    // checked={amount?.voiceamount?.min30?.check}
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "voiceamount", "min30")
                    // }

                    defaultChecked={Values?.prices?.call?.voice?.min30?.check ? true : false}
                    {...props('prices.call.voice.min30.check')}
                   
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.voiceamount?.min30?.price} // Set the value to the state variable
                // name="30"
                // autoFocus={true}

                // disabled={amount?.voiceamount?.min30?.check ? false : true}
                // onChange={(event) =>
                //   handleAmountChange(event, "voiceamount", "min30")
                // }
                disabled={wating30 ? false : true}
                {...props('prices.call.voice.min30.price')}
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="45 minutes"
                control={
                  <Checkbox
                    // name="min45"
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "voiceamount", "min45")
                    // }
                    // checked={amount?.voiceamount?.min45?.check}
                    defaultChecked={Values?.prices?.call?.voice?.min45?.check ? true : false}
                    {...props('prices.call.voice.min45.check')}
                   
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.voiceamount?.min45?.price} // Set the value to the state variable
                // disabled={amount?.voiceamount?.min45?.check ? false : true}
                // name="45"
                // autoFocus={true}
                // onChange={(event) =>
                //   handleAmountChange(event, "voiceamount", "min45")
                // }
                disabled={wating45 ? false : true}
                {...props('prices.call.voice.min45.price')}
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
        </div>
      </div>
      <div className="flex justify-between items-center w-full border-b-[1px] border-gray-300 pb-3 mb-3">
        <div className="w-2/6">
          <h3 className="text-2xl mb-0 font-semibold capitalize">
            Set Video Call Rate
          </h3>
          <p className="mb-0">Video Call</p>
        </div>
        <div className=" flex justify-between flex-wrap">
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="15 minutes"
                control={
                  <Checkbox
                    // name="min15"
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "videoamount", "min15")
                    // }
                    // checked={amount?.videoamount?.min15?.check}
                    defaultChecked={Values?.prices?.call?.video?.min15?.check ? true : false}
                    {...props('prices.call.video.min15.check')}
                    
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.videoamount?.min15?.price} // Set the value to the state variable
                // disabled={amount?.videoamount?.min15?.check ? false : true}
                // name="15"
                // autoFocus={true}
                // onChange={(event) =>
                //   handleAmountChange(event, "videoamount", "min15")
                // }
                disabled={watingv15 ? false : true}

                {...props('prices.call.video.min15.price')}
                
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="30 minutes"
                control={
                  <Checkbox
                    // name="min30"
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "videoamount", "min30")
                    // }
                    // checked={amount?.videoamount?.min30?.check}
                    defaultChecked={Values?.prices?.call?.video?.min30?.check ? true : false}
                    {...props('prices.call.video.min30.check')}
                    
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.videoamount?.min30?.price} // Set the value to the state variable
                // disabled={amount?.videoamount?.min30?.check ? false : true}
                // name="30"
                // autoFocus={true}
                // onChange={(event) =>
                //   handleAmountChange(event, "videoamount", "min30")
                // }
                disabled={watingv30 ? false : true}
                {...props('prices.call.video.min30.price')}
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
          <div className="flex justify-end items-center w-full">
            <div className="w-1/3">
              <FormControlLabel
                label="45 minutes"
                control={
                  <Checkbox
                    // name="min45"
                    // onChange={(event) =>
                    //   handleCheckboxChange(event, "videoamount", "min45")
                    // }
                    defaultChecked={Values?.prices?.call?.video?.min45?.check ? true : false}
                    // checked={amount?.videoamount?.min45?.check}
                    {...props('prices.call.video.min45.check')}
                    
                  />
                }
              />
            </div>
            <FormControl fullWidth sx={{ m: 1 }} variant="filled">
              <FilledInput
                id="filled-adornment-amount"
                // value={amount?.videoamount?.min45?.price} // Set the value to the state variable
                // disabled={amount?.videoamount?.min45?.check ? false : true}
                // name="45"
                // autoFocus={true}
                // onChange={(event) =>
                //   handleAmountChange(event, "videoamount", "min45")
                // }
                disabled={watingv45 ? false : true}
                {...props('prices.call.video.min45.price')}
                startAdornment={
                  <InputAdornment position="start">$</InputAdornment>
                }
              />
            </FormControl>
          </div>
        </div>
      </div>
    </>
  );
}

export default PricePlan;
