import * as yup from "yup";
export const schemas = yup.object().shape({
  ////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  topic: yup.string().required("topic is Required!"),
  start_time: yup.string().required("Start time is Required!"),
  password: yup.string().required("password is Required!")
  
});
