import * as yup from "yup";
export const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
export const schemas = yup.object().shape({
  ////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  name: yup.string().required("Please Enter The Name is Required!"),
  username: yup.string().required("Please Enter The User Name is Required!"),  
  phone: yup.number().required("Phone number is Required!"),
  
});
