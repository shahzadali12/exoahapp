import * as yup from "yup";
export const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
export const schemas = yup.object().shape({
  ////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  name: yup.string().required("Please Enter The Name is Required!"),
  email: yup.string().email("Email Not Valid").required("Email is Required!"),

    // phone: yup.number().required("Phone number is Required!"),
    // .matches(/[0-9]/, 'Password requires a number')
    // .matches(/[a-z]/, 'Password requires a lowercase letter')
    // .matches(/[A-Z]/, 'Password requires an uppercase letter')
    // .matches(/[^\w]/, 'Password requires a symbol'),
  // confirm: yup.string().oneOf([yup.ref('password'), null], 'Password Not Match'),
  // phone: yup.number().required("Phone number is Required!"),
});
