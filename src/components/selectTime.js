import * as React from "react";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

export default function IndeterminateCheckbox({ checkeds, setChecked }) {

  const handleChange1 = (event) => {
    const { checked, name, value } = event.target;
    if (name === "parent") {
      setChecked({
        ...checkeds,
        parent: checked ? 100 : 0,
        minutes_15: checked ? 15 : 0,
        minutes_30: checked ? 30 : 0,
        minutes_45: checked ? 45 : 0,
      });
      if (!checked) {
        setChecked({
          ...checkeds,
          parent: checked ? 100 : 0,
          minutes_15: checked ? 15 : 0,
          minutes_30: checked ? 30 : 0,
          minutes_45: checked ? 45 : 0,
          
        });
      }
    } else {
      setChecked({
        ...checkeds,
        [name]: name == "minutes_15" ? checked ? 15 : 0 : name == "minutes_30" ? checked ? 30 : 0 : name == "minutes_45" ? checked ? 45 : 0 : 0,
      });
    }
  };

 
  
  return (
    <div className="w-3/4">
      <h6 className="mb-0">Availability for meetings</h6>
      <FormControlLabel
        label="All"
        control={
          <Checkbox
            checked={checkeds.parent === 100 && checkeds.minutes_15 === 15 && checkeds.minutes_30 === 30 && checkeds.minutes_45 === 45}
            indeterminate={(checkeds.parent !== checkeds.minutes_15) || (checkeds.parent !== checkeds.minutes_30) || (checkeds.parent !== checkeds.minutes_45)}
            onChange={handleChange1}
            name="parent"
          />
        }
      />
      <FormControlLabel
        label="15 minutes"
        control={
          <Checkbox
          checked={checkeds.minutes_15 === 15} name='minutes_15' onChange={handleChange1} 
          />
        } // Updated checkbox 2
      />
      <FormControlLabel
        label="30 minutes"
        control={
          <Checkbox
          checked={checkeds.minutes_30 === 30} name='minutes_30' onChange={handleChange1} 
          />
        } // Updated checkbox 3
      />
      <FormControlLabel
        label="45 minutes"
        control={
          <Checkbox
          checked={checkeds.minutes_45 === 45} name='minutes_45' onChange={handleChange1} 
          />
        } // Added checkbox 4
      />
    </div>
  );
}
