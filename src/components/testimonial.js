import React,{useState,useEffect} from 'react'

function TestimonialCom({img,title,description,role,headstyle}) {
    const [SmallDevices, setScreen] = useState(false)

    useEffect(() => {
        setScreen(window.innerWidth > 768 ? false : true);
      },[]);
    // "-mt-8 relative mb-5"
  return (
    <div className={headstyle}>
        <div className="container">
          <div className={SmallDevices && SmallDevices ? "flex justify-center items-center flex-col" : "flex justify-center items-center m-auto"}>
            <img
              src={img}
              alt="img here"
              className={SmallDevices && SmallDevices ? "w-2/3 border-[8px] border-solid border-[#3F267C] rounded-3xl overflow-hidden" : "min-w-52 border-[8px] border-solid border-[#3F267C] rounded-3xl overflow-hidden"}
            />
            <div className={SmallDevices && SmallDevices ? "p-3 testimonial_b custom_full" : "pl-10 pt-4 testimonial_b"}>
              <p className={SmallDevices && SmallDevices ? "text-gray-600 font-light text-justify text-xl leading-normal mb-2 w-full overflow-hidden" :"text-white font-light text-justify text-2xl leading-normal mb-2 overflow-hidden"}>{description}</p>
              <h6 className={SmallDevices && SmallDevices ? "text-white font-bold text-4xl mb-0" : "text-white font-medium text-4xl mb-0"}>-{title},</h6>
              <span className={SmallDevices && SmallDevices ? "text-gray-600 font-light text-justify text-2xl leading-normal pl-3" : "text-white font-light text-justify text-2xl leading-normal pl-3"}>
                {role}
              </span>
            </div>
          </div>
        </div>
      </div>
  )
}

export default TestimonialCom