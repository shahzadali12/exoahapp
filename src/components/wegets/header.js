import Link from "next/link";
import React, { useState, useEffect,useContext } from "react";
import { Button, Menu, MenuItem } from "@mui/material";
import { Store } from "../../store/AppStore";
import { useRouter } from "next/router";
import user from "../../../public/images/user/avatar-1.jpg";
import { signOut } from "next-auth/react";

function HeaderSimple() {
  const [SmallDevices, setScreen] = useState(false);

  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);
  const {userdata,setShow} = useContext(Store);
  const [anchorEl, setAnchorEl] = useState(null);



  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const route = useRouter()
  function handleSignOut() {
    signOut({
      redirect: false,
      // callbackUrl: `${process.env.NEXTAUTH_URL}/login`
    });
    localStorage.removeItem("meeting");
    localStorage.removeItem("token");
    setShow(false);

    route.push(`/login`);
   
  }
  return (
    <header className="py-4">
      <div className="container">
        <div className="flex justify-between items-center">
          <div className="logo">
            <h1 className="mb-0">
              <Link href="/">
                <img
                  className={SmallDevices ? "w-28" : `w-40`}
                  src="../logo2.png"
                  alt="logo here"
                />
              </Link>
            </h1>
          </div>
         
          <div className="lg:flex lg:flex-1 lg:justify-end items-center">
            <Link
              href="/login"
              className="text-lg capitalize font-light leading-6 text-black mr-2 px-2 py-1 rounded-xl"
            >
              Log in
              
            </Link>
            <Link
              href="/signup"
              className="text-lg capitalize font-medium leading-6 text-white bg-black px-2 py-1 rounded-xl"
            >
              Sign up
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
}

export default HeaderSimple;
