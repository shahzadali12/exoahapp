import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import logo from "../../../public/logo.svg";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";

import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import { schemas } from "../schemas/profile";
import { yupResolver } from "@hookform/resolvers/yup";

import CircularProgress from "@mui/material/CircularProgress";

import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { Grid } from "@mui/material";
import Image from "next/image";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

import CloudUploadIcon from "@mui/icons-material/CloudUpload";

import { styled } from "@mui/material/styles";

function UpdateProfile() {
  const [loading, setLoading] = useState(false);
  const [selectionRole, setselectionRole] = useState(false);
  
  const {
    handleSubmit,
    register,
    reset,
    setValue,
    control,
    watch,
    getValues,
    setError,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemas),
    mode: "onChange",
  });

  const route = useRouter();
  const onSubmit = async (data) => {
    setLoading(true);

    const regis = {
      profilePhoto: "",
      password: data.password,
      role: "user",
      email: data.email,
      name: data.name,
    };
    const response = await fetch(`/api/authenticated/login/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(regis),
    });

    if (response.status === 201) {
      setLoading(false);
      // SetTimerButton(true);
      route.push("/login");
      toast.success("Congratulations your account has been created!");
    } else if (response.status === 409) {
      toast.error("This Email account already exist!");
      // SetTimerButton(true);
      // route.push("/login");
      setLoading(false);
    } else {
      toast.error("Something was wrong");
      // SetTimerButton(true);
      route.push("/login");
    }
  };

  const VisuallyHiddenInput = styled("input")({
    clip: "rect(0 0 0 0)",
    clipPath: "inset(50%)",
    height: 1,
    overflow: "hidden",
    position: "absolute",
    bottom: 0,
    left: 0,
    whiteSpace: "nowrap",
    width: 1,
  });
  return (
    //   <form onSubmit={handleSubmit(onSubmit)}>
    //     <div className="row">
    //       <div className="col-md-12">
    //         <div className={`form-floating mb-3 ${errors.name ? "error" : ""}`}>
    //           <input
    //             type="text"
    //             className="form-control"
    //             id="floatingInput"
    //             {...register("name", { required: true })}
    //             placeholder="First Name"
    //           />
    //           <label htmlFor="floatingInput">Name</label>
    //         </div>
    //       </div>
    //       {/* <div className="col-md-6">
    //       <div className="form-floating mb-3">
    //         <input
    //           type="email"
    //           className="form-control"
    //           id="floatingInput"
    //           placeholder="Last Name"
    //         />
    //         <label htmlFor="floatingInput">Last Name</label>
    //       </div>
    //     </div> */}
    //     </div>
    //     <div className={`form-floating mb-3 ${errors.email ? "error" : ""}`}>
    //       <input
    //         type="email"
    //         className="form-control"
    //         id="floatingInput"
    //         {...register("email", { required: true })}
    //         placeholder="Email Address / Username"
    //       />
    //       <label htmlFor="floatingInput">Email Address </label>
    //     </div>
    //     <div className={`form-floating mb-3 ${errors.password ? "error" : ""}`}>
    //       <input
    //         type="password"
    //         className="form-control"
    //         id="floatingInput"
    //         {...register("password", { required: true })}
    //         placeholder="Password"
    //       />
    //       <label htmlFor="floatingInput">Password</label>
    //     </div>
    //     {/* <div className="form-check mt-3s"> */}
    //     <FormControlLabel
    //       control={
    //         <Checkbox
    //           onChange={(e) => setValue("agree", e.target.checked)}
    //         />
    //       }
    //       label="Agree with Terms & Condition."
    //     />

    //     {/* <input className="form-check-input input-primary" type="checkbox" id="customCheckc1" checked="" />
    //   <label className="form-check-label" htmlFor="customCheckc1">
    //     <h5>Agree with <span>Terms & Condition.</span></h5>
    //   </label> */}
    //     {/* </div> */}
    //     <div className="d-grid mt-4">
    //       <Stack>
    //       {loading ? <div className="flex justify-center items-center"><CircularProgress /></div> : <Button
    //           variant="text"
    //           type="submit"
    //           disabled={loading}
    //           className="btn bg-secondary text-white"
    //         >
    //            Sign Up
    //         </Button> }
    //       </Stack>
    //     </div>
    //   </form>
    <>
      <div className="flex justify-center py-3 items-center flex-col">
        {/* <i className="text-6xl"><AccountCircleIcon fontSize="large"/></i> */}
        <img
          src="https://stgapiv2.mentoga.com/assets/images/users/user.jpg"
          alt="logo"
          width={100}
          height={100}
          className="rounded-full mb-2"
        />
        <Button
          component="label"
          variant="contained"
          startIcon={<CloudUploadIcon />}
        >
          Upload Profile Photo
          <VisuallyHiddenInput type="file" />
        </Button>
      </div>
      <div className="">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="row">
            <div className="col-md-6">
              <div
                className={`form-floating mb-3 ${errors.name ? "error" : ""}`}
              >
                <input
                  type="text"
                  className="form-control"
                  id="floatingInput"
                  {...register("name", { required: true })}
                  placeholder="First Name"
                />
                <label htmlFor="floatingInput">Name</label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-floating mb-3">
                <input
                  type="text"
                  className="form-control"
                  id="floatingInput"
                  placeholder="User Name"
                />
                <label htmlFor="floatingInput">User Name</label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-floating mb-3">
                <input
                  type="email"
                  className="form-control"
                  id="floatingInput"
                  placeholder="Last Name"
                />
                <label htmlFor="floatingInput">Email Address</label>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-floating mb-3">
                <input
                  type="email"
                  className="form-control"
                  id="floatingInput"
                  placeholder="Last Name"
                />
                <label htmlFor="floatingInput">Date of Birth</label>
              </div>
            </div>

            <div className="col-md-6">
              <div className="form-floating mb-3">
                <input
                  type="date"
                  className="form-control"
                  id="floatingInput"
                  placeholder="Last Name"
                />
                <label htmlFor="floatingInput">Date of Birth</label>
              </div>
            </div>
          </div>

          <div className="d-grid mt-4">
            <Stack>
              {loading ? (
                <div className="flex justify-center items-center">
                  <CircularProgress />
                </div>
              ) : (
                <Button
                  variant="text"
                  type="submit"
                  disabled={loading}
                  className="btn bg-secondary text-white"
                >
                  Profile
                </Button>
              )}
            </Stack>
          </div>
        </form>
      </div>
    </>
  );
}

export default UpdateProfile;
