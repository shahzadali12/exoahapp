import mongoose from 'mongoose';



// Connect to MongoDB
async function connect() {
  try {
    const {connection} = await mongoose.connect(process.env.MONGODB_URI,{
      dbName: "exoah",
      useNewUrlParser: true,
      useUnifiedTopology: true,
      // bufferCommands: false,
      // bufferMaxEntries: 0,
      // useFindAndModify: true,
      // useCreateIndex: false,
    })
    console.log('Connected to MongoDB');
    return connection
    
    
    
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    // process.exit(1);
  }
}

export { connect };

