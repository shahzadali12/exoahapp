import { useRouter } from "next/router";
import SideBAR from "../components/Sidebar";
import Footer from "../components/footer";
import Header from "../components/header";
import { useSession } from "next-auth/react";
import Models from "../components/model";
import { useContext, useEffect, useMemo, useState } from "react";
import UpdateMissing from "../components/auth/update";
import Link from "next/link";
import { Store } from "../store/AppStore";
import { useMediaQuery } from "react-responsive";
import axios from "axios";
import HeaderLayout from "../components/home/header";
import MainLayout from "./main";
import FooterLayout from "../components/home/footer";

import LoginComponent from "../components/auth/loginCom";
import moment from "moment";
import CallPayment from "../components/payment/call";

function DefaultLayout({ children }) {
  const route = useRouter();
  const { data } = useSession();


  // const SmallDevices = useMediaQuery({ query: '(max-width: 1224px)' })
  const {
    userdata,
    show,
    setShow,
    logins,
    setlogins,
    indexing,
    setindexing,
    slotbooked,
    setslotbooked,
    Selected_creator,
    paymentSlot,setpaymentSlot,
    prices
    
  } = useContext(Store);

  

  const SmallDevices = useMediaQuery(
    { maxDeviceWidth: 991 },
    { deviceWidth: 480 } // `device` prop
  );

  const postRequried = () => {
    route.push("/profile");
  };

  // relative={"relative"}

  useEffect(() => {
    if (!data?.user?.name) {
      if (
        route.pathname == "/dashboard" ||
        !route.pathname == "/" ||
        !route?.pathname == "/creators" ||
        !route?.pathname == "/signup" ||
        !route?.pathname == "/404" ||
        !route?.pathname == "/login"
      ) {
        route.push("/login");
      }
    }
  }, [data, route?.pathname]);

  return route?.pathname === "/creators" ? (
    <div className="flex justify-between flex-col bg-black">
      <HeaderLayout relative={""} con="active" />

      <div className="custom-min-height w-full ">{children}</div>

      <FooterLayout p={"pt-4"} />
    </div>
  ) : route?.pathname === "/[...slug]" ? (
    <div className="flex justify-between flex-col ">
      <HeaderLayout relative={"relative"} con="active" />
      {logins && (
        <Models
          show={logins}
          setShowlog={setlogins}
          data={<LoginComponent full="w-full" />}
        />
      )}

      <Models
        show={slotbooked}
        setShowlog={setslotbooked}
        data={
          <div className="flex justify-center items-center flex-col">
            <img src="../images/icon.png" className="w-28" alt="img here" />
            <div className="content_booked text-center">
              <h5>Yay Call Booked!</h5>
              <p>
                Congratulations! Your 1-on-1 Call with{" "}
                {Selected_creator?.infos?.name && Selected_creator?.infos?.name}{" "}
                has been successfully scheduled!
              </p>

              <span>Here are the call details:</span>
              <ul>
                <li>Date: {moment(indexing?.starttime).format("DD MMM YY")}</li>
                <li>
                  Time: {moment(indexing?.starttime).format("hh:mm")} -{" "}
                  {moment(indexing?.starttime)
                    .add(indexing?.sslots, "minutes")
                    .format("hh:mm")}
                </li>
                <li>Call Duration: {indexing?.sslots} mins</li>
              </ul>
              <p>
                A confirmation email has been sent to you with call details of
                your booked meeting. <b>Mark your Calendar!</b>
              </p>
            </div>
          </div>
        }
      />




      <Models
        show={paymentSlot}
        heading={`Pay $${prices == 15
          ? Selected_creator?.prices?.call?.video?.min15?.price
          : prices == 30
          ? Selected_creator?.prices?.call?.video?.min30?.price
          : prices == 45
          ? Selected_creator?.prices?.call?.video?.min45?.price
          : 0} for Book a Timeslot!`}
        setShowlog={setpaymentSlot}
        data={<CallPayment setpaymentSlot={setpaymentSlot} price={prices == 15
          ? Selected_creator?.prices?.call?.video?.min15?.price
          : prices == 30
          ? Selected_creator?.prices?.call?.video?.min30?.price
          : prices == 45
          ? Selected_creator?.prices?.call?.video?.min45?.price
          : 0}/>}
      />

      <div className="custom-min-height w-full">{children}</div>
      <FooterLayout p={"pt-4"} />
    </div>
  ) : route?.pathname === "/" ? (
    data?.user.role === "user" ? (
      <>
        <div
          className={
            SmallDevices
              ? "alert alert-success p-0 mb-0"
              : "alert alert-success mb-0"
          }
        >
          <div className="container text-center py-2">
            <div>
              Welcome to Join {data?.user?.name} Today 20% off spacial offer for
              you
            </div>
          </div>
        </div>{" "}
        <div className="flex justify-between flex-col bg-black">
          <HeaderLayout con="active" />
          <div className="custom-min-height w-full bg-black overflow-hidden">
            {children}
          </div>
          <FooterLayout />
        </div>
      </>
    ) : (
      <>
        {" "}
        {show.about ||
        show.language ||
        show?.name ||
        show.phone ||
        show.designation ||
        show.shortinfo ||
        show.role ? (
          <>
            <div
              className={
                SmallDevices
                  ? "alert alert-warning p-0 mb-0"
                  : "alert alert-warning mb-0"
              }
            >
              <div className="container text-center py-2">
                <div>
                  Verify your account now to unlock exclusive benefits and
                  secure your personalized experience.
                  <button
                    type="button"
                    onClick={postRequried}
                    className="mx-1 font-semibold"
                  >
                    Click
                  </button>
                  the link to complete the verification process.
                </div>
              </div>
            </div>
            {show?.role && (
              <Models
                show={show.role}
                setShow={setShow}
                heading={"Please Select Your Role"}
                data={<UpdateMissing show={show} setShow={setShow} />}
              />
            )}
            <>
              <div className="flex justify-between flex-col bg-black">
                <HeaderLayout con="active" />
                <div className="custom-min-height w-full bg-black overflow-hidden">
                  {children}
                </div>
                <FooterLayout />
              </div>
            </>
          </>
        ) : (
          <>
            <div className="flex justify-between flex-col">
              <HeaderLayout />
              <div className="custom-min-height w-full bg-black overflow-hidden">
                {children}
              </div>
              <FooterLayout />
            </div>
          </>
        )}
      </>
    )
  ) : route?.pathname === "/login" ||
    route?.pathname === "/signup" ||
    route?.pathname === "/404" ? (
    <div className="custom-min-height w-full overflow-hidden">{children}</div>
  ) : (
    <>
      {show?.role && (
        <Models
          show={show.role}
          setShow={setShow}
          heading={"Please Select Your Role"}
          data={<UpdateMissing show={show} setShow={setShow} />}
        />
      )}
      {data?.user.role === "user" ? (
        <>
          <div
            className={
              SmallDevices
                ? "alert alert-success p-0 mb-0"
                : "alert alert-success mb-0"
            }
          >
            <div className="container text-center py-2">
              <div>
                Welcome to Join {data?.user?.name} Today 20% off spacial offer
                for you
              </div>
            </div>
          </div>{" "}
        </>
      ) : show.about ||
        show.language ||
        show?.name ||
        show.phone ||
        show.designation ||
        show.shortinfo ||
        show.role ? (
        <div
          className={
            SmallDevices
              ? "alert alert-warning p-0 mb-0"
              : "alert alert-warning mb-0"
          }
        >
          <div className="container text-center py-2">
            <div>
              Verify your account now to unlock exclusive benefits and secure
              your personalized experience.
              <button
                type="button"
                onClick={postRequried}
                className="mx-1 font-semibold"
              >
                Click
              </button>
              the link to complete the verification process.
            </div>
          </div>
        </div>
      ) : null}

      {data?.user?.name && (
        <>
          <Header />
          <SideBAR />
          <div className="pc-container">
            <div className="pc-content">
              <div className="row">{children}</div>
            </div>
          </div>
          <Footer />{" "}
        </>
      )}
    </>
  );
}
export default DefaultLayout;
