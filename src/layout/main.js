import React from 'react'
import HeaderLayout from "../components/home/header";
import FooterLayout from "../components/home/footer";
function MainLayout({values}) {
  return (
    <div className="flex justify-between flex-col relative"><HeaderLayout/><div className='min-h-screen w-full bg-black overflow-hidden'>{values}</div><FooterLayout/></div> 
  )
}

export default MainLayout

