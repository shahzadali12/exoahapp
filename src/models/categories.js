import mongoose, { Schema } from 'mongoose';

const CategorySchema = new mongoose.Schema({
  label: {
    type: String,
    trim: true,
    require: true
  },
  id: {
    type: String,
  },
}, { timestamps: true });

export const Categories = mongoose.models.categories || mongoose.model('categories', CategorySchema);



