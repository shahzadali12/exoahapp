import mongoose, { Schema } from 'mongoose';

const message = new Schema({
    id: {
        type: String,
      },
    text: {
      type: String,
      required: true,
    },
    sender: {
      type: String,
      required: true,
    },
    
 }, { timestamps: true });
  

const chatsSchema = new Schema({
    sendId: {
    type: String,
    require: true,
  },
  botId: {
    type: String,
    require: true,
  },
  title: {
    type: String,
    require: true,
  },
  messages:{
    type: [message],
    default: {},
  },
  id: {
    type: String,
  },
}, { timestamps: true });

export const ChatMessage = mongoose.models.chats || mongoose.model('chats', chatsSchema);



