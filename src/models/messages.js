import mongoose, { Schema } from 'mongoose';

const messagesSchema = new Schema({

  
  messages:  { type: Array, default: [] },
  id: {
    type: String,
  },
}, { timestamps: true });

export const messages = mongoose.models.messages || mongoose.model('messages', messagesSchema);



