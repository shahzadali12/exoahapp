import mongoose, { Schema } from "mongoose";

const UserSchema = new mongoose.Schema(
  {
    zoom:  {
      type: String,
    },
    info: {
      name: {
        type: String,
        trim: true,
        require: true,
        max: 32,
      },
      slug: String,
      email: {
        type: String,
        require: true,
        unique: true,
      },
      profilePhoto: {
        type: String,
      },
      designation: {
        type: String,
      },
      role: {
        type: String,
      },
      languages: { type: Array, default: [] },
      category: { type: Array, default: [] },
      password: {
        type: String,
        require: true,
      },
      phone:  {
        type: String,
      },
      shortinfo:  {
        type: String,
      },
      about:  {
        type: String,
      },
    },
    messages: { type: Array, default: [] },
    booked_slots:  { type: Array, default: [] },
    meetings:  { type: Array, default: [] },
    links: {
      fb: String,
      twlinks: String,
      insta: String,
      ldklink: String,
    },
    availability: {
      break: String,
      slotDuration: { type: Array, default: [] },
      times: { type: Array, default: [] },
    },
    prices: {
      messages: {
        chat: String,
        audio: String,
        video: String,
      },
      call: Object,
    },
    id: {
      type: String,
    },
  },
  { timestamps: true }
);

export const User = mongoose.models.users || mongoose.model("users", UserSchema);

// {
//   voice: {
//       min15: {
//           check:Boolean,
//           price:String,
//       },
//       min30: {
//           check:Boolean,
//           price:String,
//       },
//       min45: {
//           check:Boolean,
//           price:String
//       },
//   },
//   video: {
//       min15: {
//           check:Boolean,
//           price:String,
//       },
//       min30: {
//           check:Boolean,
//           price:String,
//       },
//       min45: {
//           check:Boolean,
//           price:String,
//       },
//   },
