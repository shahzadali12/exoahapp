import Link from "next/link";
import React, { useState, useEffect, useRef, useMemo, useContext } from "react";
import XIcon from "@mui/icons-material/X";
import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import CommentOutlinedIcon from "@mui/icons-material/CommentOutlined";
import VoiceChatIcon from "@mui/icons-material/VoiceChat";
import VideoChatOutlinedIcon from "@mui/icons-material/VideoChatOutlined";
import GraphicEqOutlinedIcon from "@mui/icons-material/GraphicEqOutlined";
import VideoCallOutlinedIcon from "@mui/icons-material/VideoCallOutlined";
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import Overlay from "../components/overlay";
import { Store } from "../store/AppStore";
import BookingSlot from "../components/bookslot";
import { toast } from "react-toastify";
import { getSession } from "next-auth/react";
import axios from "axios";
import { useRouter } from "next/router";
function DyanamicPages({ apiData }) {
  
  const { Selected_creator, userdata, setSelected_creator,states, setStates } = useContext(Store);
const Route = useRouter()
  const [SmallDevices, setScreen] = useState(false);
  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);

  const target = useRef(null);

  

  useEffect(() => {
    if (apiData) {
      setSelected_creator(apiData);
    }
  }, [apiData]);

  const BookedSlot = async () => {
    // if (userdata?.info?.name) {
      await axios
        .get(`/api/slots/${apiData?.infos?.slug}`)
        .then((val) => {
          setStates({ menuStatus: "open", avaliable: true, data: val?.data })
     
        }
         
        )
        .catch((err) => console.log(err));
    // } else {
    //   toast.error("login your account first");
    // }
  };


 

  return (
    <>
      <Overlay
        creator={apiData}
        target={target}
        states={states}
        setStates={setStates}
      />

      <section
        className={`${
          SmallDevices ? "float-left w-full" : "py-12 float-left w-full"
        }`}
      >
        <div className="container ">
          <div
            className={
              SmallDevices
                ? "flex justify-between flex-col"
                : "flex justify-between mb-7"
            }
          >
            <div
              className={`${
                SmallDevices
                  ? "column flex justify-between  w-full mb-2"
                  : "column flex justify-between  w-[80%]"
              }`}
            >
              <div
                className={SmallDevices ? "c-image w-1/2 relative " : "c-image w-[36%] relative"}
              >
                <img
                  src={
                    apiData?.infos?.profilePhoto
                      ? process.env.NODE_ENV === "development"
                        ? process.env.NEXTAUTH_URL +
                          "/images/users/" +
                          apiData?.infos?.profilePhoto
                        : process.env.NEXTAUTH_URL +
                          "/public/images/users/" +
                          apiData?.infos?.profilePhoto
                      : "../current.png"
                  }
                  className={`${
                    SmallDevices
                      ? " w-full object-cover border-4 border-gray-300 rounded-3xl"
                      : "border-4 border-gray-300 rounded-3xl w-full object-cover "
                  } `}
                  alt="img here"
                />
                <span className={SmallDevices ? "absolute -top-0 -right-1" : "absolute -top-2 -right-2"}><img className={SmallDevices ? "w-5" : "w-9"} src="../images/verified.png" alt="here img"/></span>
              </div>

              <div
                className={
                  SmallDevices
                    ? "c_content w-2/3 pr-0 pl-2 pt-3"
                    : "c_content pt-6 pl-5 pr-[10%] w-4/5"
                }
              >
                <h3
                  className={
                    SmallDevices
                      ? "text-2xl mb-0 font-bold uppercase leading-none"
                      : "text-5xl mb-0 font-semibold uppercase"
                  }
                >
                  {apiData?.infos?.name && apiData?.infos?.name}
                </h3>
                <h5
                  className={
                    SmallDevices
                      ? "text-lg font-medium mb-1"
                      : "pl-1 text-3xl font-medium mb-7"
                  }
                >
                  {apiData?.infos?.designation && apiData?.infos?.designation}
                </h5>

                {SmallDevices && (
                  <>
                    <span className="text-yellow-600 mb-2 float-left w-full">
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>
                      <i className="fa fa-star" aria-hidden="true"></i>{" "}
                      <b className="text-sm text-black">5.0 (56)</b>
                    </span>
                    <h6 className="text-base leading-none mb-2">
                      <b>Speaks</b>:{" "}
                      {apiData?.infos?.languages?.length > 0 &&
                        apiData?.infos?.languages?.map((val) => val.label)}
                    </h6>
                    <h6 className="text-base leading-none">
                      <b>Send a High Five</b>:{" "}
                      <i className="fa fa-hand-spock-o" aria-hidden="true"></i>
                    </h6>
                  </>
                )}

                {!SmallDevices && (
                  <>
                    <div className="flex justify-between flex-col">
                      <span
                        className={
                          SmallDevices
                            ? "font-bold text-lg leading-none p-0"
                            : "font-semibold text-2xl leading-none p-0 mb-2"
                        }
                      >
                        Expertise:
                      </span>
                      <ul className="p-0 flex justify-start items-center flex-wrap lists-s1 ">
                        {apiData?.infos?.category.length > 0 &&
                          apiData?.infos?.category
                            ?.slice(0, 3)
                            .map((val, inde) => (
                              <li key={inde}>{val?.label}</li>
                            ))}
                      </ul>
                    </div>

                    <div className="flex justify-between flex-col">
                      <span
                        className={
                          SmallDevices
                            ? "font-bold text-lg leading-none p-0"
                            : "font-semibold text-2xl leading-none p-0 mb-2"
                        }
                      >
                        Languages:
                      </span>
                      <ul className="p-0 flex justify-start items-center flex-wrap lists-s1 ">
                        {apiData?.infos?.languages.length > 0 &&
                          apiData?.infos?.languages?.map((val, inde) => (
                            <li key={inde}>{val?.label}</li>
                          ))}
                      </ul>
                    </div>
                  </>
                )}
                
                {!SmallDevices && (
                  <>
                  <span
                  className={
                    SmallDevices
                      ? "font-bold text-lg leading-none p-0"
                      : "font-semibold text-2xl leading-none p-0 "
                  }
                >
                  Social Profiles:
                </span>
                    <ul className="social_icons pl-0 flex justify-between items-center">
                      {apiData?.links?.ldklink && (
                        <li>
                          <Link
                            href={apiData?.links?.ldklink}
                            className="text-4xl font-semibold text-gray-950"
                          >
                            <LinkedInIcon fontSize="large" />{" "}
                            <span className="text-2xl font-normal text-gray-600">LinkedIn</span>
                          </Link>
                        </li>
                      )}
                      {apiData?.links?.twlinks && (
                        <li>
                          <Link
                            href={apiData?.links?.twlinks}
                            className="text-4xl font-semibold text-gray-950"
                          >
                            <XIcon fontSize="large" />{" "}
                            <span className="text-2xl font-normal text-gray-600">Twitter</span>
                          </Link>
                        </li>
                      )}
                      {apiData?.links?.fb && (
                        <li>
                          <Link
                            href={apiData?.links?.fb}
                            className="text-4xl font-semibold text-gray-950"
                          >
                            <FacebookIcon fontSize="large" />{" "}
                            <span className="text-2xl font-normal text-gray-600">Facebook</span>
                          </Link>
                        </li>
                      )}

                      {apiData?.links?.insta && (
                        <li>
                          <Link
                            href={apiData?.links?.insta}
                            className="text-4xl font-semibold text-gray-950"
                          >
                            <InstagramIcon fontSize="large" />{" "}
                            <span className="text-2xl font-normal text-gray-600">Instagram</span>
                          </Link>
                        </li>
                      )}
                    </ul>
                  </>
                )}
              </div>
            </div>
            {SmallDevices && (
              <ul className="social_icons flex justify-around pl-0 items-center w-full mb-2">
                {apiData?.links?.fb && (
                  <li >
                    <Link
                      href={apiData?.links?.fb}
                      className="text-4xl font-semibold text-gray-950"
                    >
                      <FacebookIcon fontSize="inherit" />
                    </Link>
                  </li>
                )}
                {apiData?.links?.twlinks && (
                  <li >
                    <Link
                      href={apiData?.links?.twlinks}
                      className="text-4xl font-semibold text-gray-950"
                    >
                      <XIcon fontSize="inherit" />
                    </Link>
                  </li>
                )}
                {apiData?.links?.insta && (
                  <li >
                    <Link
                      href={apiData?.links?.insta}
                      className="text-4xl font-semibold text-gray-950"
                    >
                      <InstagramIcon fontSize="inherit" />
                    </Link>
                  </li>
                )}
                {apiData?.links?.ldklink && (
                  <li>
                    <Link
                      href={apiData?.links?.ldklink}
                      className="text-4xl font-semibold text-gray-950"
                    >
                      <LinkedInIcon fontSize="inherit" />
                    </Link>
                  </li>
                )}
              </ul>
            )}

            {SmallDevices && (
              <div className="w-full ">
                <h3 className="text-3xl tracking-tight text-black font-semibold">
                  About <strong className="text-[#3F267C]">Me:</strong>
                </h3>
                {apiData?.infos?.about && (
                  <p className="text-lg text-justify mb-2">
                    {apiData?.infos?.about}
                  </p>
                )}
              </div>
            )}

            {SmallDevices && (
              <>
                <div className="w-full ">
                  <h3 className="text-3xl tracking-tight text-black font-semibold">
                    Ask me <strong className="text-[#3F267C]">About:</strong>
                  </h3>
                </div>
                <div className="pl-1 flex justify-between ">
                  <ul className="p-0 flex justify-start items-center flex-wrap mb-1">
                    {apiData?.infos?.category.length > 0 &&
                      apiData?.infos?.category?.map((val, inde) => (
                        <li className="bg-gray-300 rounded-full px-3 py-1 mr-3 mb-3 text-base" key={inde}>{val?.label}</li>
                      ))}
                  </ul>
                </div>
              </>
            )}
            {/* <BookingSlot slots={apiData?.availability?.times} /> */}

            {!SmallDevices && apiData?._id != userdata?._id ? (
              <div className={`${SmallDevices ? "w-full" : "w-[20%]"} `}>
                <h6 className="text-2xl font-semibold text-center mb-2">
                  Interact with Steven Right Away
                </h6>
                <div className="flex justify-between flex-col ">
                  <button
                    type="button"
                    // onClick={() => {
                    //   userdata?.info?.name
                    //     ? setStates({ menuStatus: "open", avaliable: false })
                    //     : toast.error("login your account first");
                    // }}
                    onClick={() => setStates({ menuStatus: "open", avaliable: false })}
                    className="border rounded-md border-black text-2xl capitalize text-gray-950 px-3 py-3 bg-gray-100 font-semibold mb-4"
                  >
                    Start Chatting
                  </button>
                  <button
                    type="button"
                    onClick={BookedSlot}
                    className="border rounded-md border-black text-2xl bg-[#3F267C] capitalize text-white px-3 py-3 font-semibold mb-4"
                  >
                    Book a Call
                  </button>
                  <button
                    type="button"
                    className="border rounded-md border-black text-2xl bg-[#FF981F] capitalize text-gray-950 px-3 py-3 font-semibold mb-4"
                  >
                    Send a High Five!
                  </button>
                </div>
              </div>
            ) : null}
          </div>

          {/* <div className="container text-center mb-8">
            <div className="line_center"></div>
          </div> */}
          <div
            className={
              SmallDevices
                ? "w-full flex justify-between flex-col"
                : "w-full flex justify-between "
            }
          >
            {!SmallDevices && (
              <>
              <div className="pr-7 w-[80%]">
                <h3 className="text-4xl tracking-tight text-black font-semibold">
                  About Me:
                </h3>
                {apiData?.infos?.about && (
                  <p className="text-2xl mb-3">{apiData?.infos?.about}</p>
                )}

                <div className="w-full pr-7">
                  <h3 className="text-4xl tracking-tight text-black font-semibold">
                    Ask me About:
                  </h3>
                </div>
                <div className="flex justify-between ">
                  <ul className="p-0 flex justify-start items-center flex-wrap  mb-1">
                    {apiData?.infos?.category.length > 0 &&
                      apiData?.infos?.category?.map((val, inde) => (
                        <li
                          key={inde}
                          className="bg-gray-300 rounded-full px-3 py-1 mr-3 mb-3 text-xl"
                        >
                          {val?.label}
                        </li>
                      ))}
                  </ul>
                </div>
                <div className="w-full pr-7">
              <h3 className="text-4xl tracking-tight text-black font-semibold">
                Find More Experts:
              </h3>
              {apiData?.infos?.about && (
                <div className="flex justify-between items-center">
                  <div className="item">
                    <figure className="relative float-left w-full mb-0">
                      <img src="../images/authors/c1.png"  alt="img here"/>
                      <div className="absolute bottom-3 left-0 right-0 text-center">
                        <h6 className="font-semibold text-white capitalize text-2xl leading-none float-left w-full mb-2">rachel pantano</h6>
                        <span className="uppercase text-gray-400 text-sm leading-4 float-left">Massage Therapist & Mobility Coach</span>
                      </div>
                    </figure>
                  </div>
                  <div className="item">
                    <figure className="relative float-left w-full mb-0">
                      <img src="../images/authors/c1.png"  alt="img here"/>
                      <div className="absolute bottom-3 left-0 right-0 text-center">
                        <h6 className="font-semibold text-white capitalize text-2xl leading-none float-left w-full mb-2">rachel pantano</h6>
                        <span className="uppercase text-gray-400 text-sm leading-4 float-left">Massage Therapist & Mobility Coach</span>
                      </div>
                    </figure>
                  </div>
                  <div className="item">
                    <figure className="relative float-left w-full mb-0">
                      <img src="../images/authors/c1.png"  alt="img here"/>
                      <div className="absolute bottom-3 left-0 right-0 text-center">
                        <h6 className="font-semibold text-white capitalize text-2xl leading-none float-left w-full mb-2">rachel pantano</h6>
                        <span className="uppercase text-gray-400 text-sm leading-4 float-left">Massage Therapist & Mobility Coach</span>
                      </div>
                    </figure>
                  </div>
                  <div className="item">
                    <figure className="relative float-left w-full mb-0">
                      <img src="../images/authors/c1.png"  alt="img here"/>
                      <div className="absolute bottom-3 left-0 right-0 text-center">
                        <h6 className="font-semibold text-white capitalize text-2xl leading-none float-left w-full mb-2">rachel pantano</h6>
                        <span className="uppercase text-gray-400 text-sm leading-4 float-left">Massage Therapist & Mobility Coach</span>
                      </div>
                    </figure>
                  </div>
                </div>
              )}

              
            </div>
              </div>
              
            </>
            )}

            <div className={`flex justify-between flex-col ${SmallDevices ? "w-full" : "w-[20%]"}`}>
              <div className="widget">
                <h6 className="text-lg text-black capitalize font-semibold w-full text-left">
                  Chat Rates
                </h6>
                <ul className="list flex justify-between flex-col bg-gray-400 p-3 rounded-md">
                  <li className="text-lg mb-1">
                    <i className="mr-2 text-[#3F267C]">
                      <CommentOutlinedIcon />
                    </i>
                    Text Message: $
                    {apiData?.prices?.messages?.chat &&
                      apiData?.prices?.messages?.chat}
                  </li>
                  <li className="text-lg mb-1">
                    <i className="mr-2 text-[#3F267C]">
                      <VoiceChatIcon />
                    </i>
                    Audio Message: $
                    {apiData?.prices?.messages?.audio &&
                      apiData?.prices?.messages?.audio}
                  </li>
                  <li className="text-lg">
                    <i className=" mr-2 text-[#3F267C]">
                      <VideoChatOutlinedIcon />
                    </i>
                    Video Message: $
                    {apiData?.prices?.messages?.video &&
                      apiData?.prices?.messages?.video}
                  </li>
                </ul>
                <h6 className="text-lg text-black capitalize font-semibold w-full text-left">
                  Call Rates
                </h6>
                <ul className="list flex justify-between flex-col bg-gray-400 p-3 rounded-md">
                  <li className="text-lg">
                    <i className="mr-2 text-[#3F267C]">
                      <GraphicEqOutlinedIcon />
                    </i>
                    $
                    {apiData?.prices?.call?.voice?.min15?.price &&
                      apiData?.prices?.call?.voice?.min15?.price}
                    /15 minutes
                  </li>
                  {/* <li className="text-lg mb-1">
                    <i className="mr-2 text-[#3F267C]">
                      <VideoCallOutlinedIcon />
                    </i>
                    <b className="mr-2">Video Call</b>: ${apiData?.prices?.call?.video?.min15?.price && apiData?.prices?.call?.video?.min15?.price}/15 Mins
                  </li>
                  <li className="text-lg">
                    Call Duration can be extended while booking.
                  </li> */}
                </ul>
              </div>
              <div className="widget">
                
              </div>
            </div>
          </div>
        </div>
      </section>

  

      {apiData?._id && SmallDevices || apiData?._id != apiData?._id ? (
        <div className="flex justify-between fixed left-0 right-0 bottom-0 bg-white z-[9999]">
          <button
            type="button"
            onClick={() => {
              userdata?.info?.name
                ? setStates({ menuStatus: "open", avaliable: false })
                : toast.error("login your account first");
            }}
            className="border rounded-md  text-base capitalize text-gray-950 py-3 bg-gray-100 font-semibold w-full"
          >
            Start Chatting
          </button>
          <button
            type="button"
            // onClick={()=> setStates({menuStatus: "open",avaliable: true})}
            onClick={BookedSlot}
            className="border rounded-md  text-base bg-[#3F267C] capitalize text-white py-3 font-medium w-full"
          >
            Book a Call
          </button>
          <button
            type="button"
            className="border rounded-md  text-base bg-[#FF981F] capitalize text-gray-950 py-3 font-semibold w-full"
          >
            Send a High Five!
          </button>
        </div>
      ) : null}
    </>
  );
}

export async function getServerSideProps({ params }) {
  // const session = await getSession({ req });
  const [slug] = params?.slug;

  const listingAPi = await fetch(
    `${process.env.NEXTAUTH_URL}/api/authenticated/slug/${slug}`
  );
  const apiData = await listingAPi.json();

  if (!apiData?.infos) {
    return {
      redirect: {
        destination: `${process.env.NEXTAUTH_URL}`,
        permanent: false,
      },
    };
  }

  return { props: { apiData } };
}

export default DyanamicPages;
