import "../styles/globals.css";
// import "../styles/animation.css";
import { Roboto } from "next/font/google";
import { SessionProvider } from "next-auth/react";
import DefaultLayout from "../layout/default";
import { ToastContainer, toast } from "react-toastify";
import "react-phone-input-2/lib/style.css";
import "ag-grid-community/styles/ag-grid.css";
import "ag-grid-community/styles/ag-theme-alpine.css";
import "ag-grid-community/styles/ag-theme-material.css";
import "react-toastify/dist/ReactToastify.css";
import "react-datepicker/dist/react-datepicker.css";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import AppStore from "../store/AppStore";



const stripePromise = loadStripe("pk_test_51OZqMFB044ieUnHA9VXHSYfmUBuNDN0UsXBHXAOqJM8Yte9ZJh8dYqGelZ81QUA4I13QinxsD0kIRHwgD6K2slBC00EDT0i5RT");
const roboto = Roboto({
  weight: [
    "100",
    "300",
    "400",
    "500",
    "700",
    "900",
    "100",
    "300",
    "400",
    "500",
    "700",
    "900",
  ],
  style: ["normal", "italic", "normal", "italic"],
  subsets: [
    "cyrillic",
    "cyrillic-ext",
    "greek",
    "greek-ext",
    "latin",
    "latin-ext",
    "vietnamese",
  ],
  display: "swap",
});

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}) {
  const route = useRouter();

  // useEffect(() => {
  //   if (route?.pathname != "/meeting") {
  //     localStorage.removeItem("meeting");
  //   }
  // }, [route?.pathname]);
  return (
   
      <Elements stripe={stripePromise} >
         <SessionProvider session={session}>
        <AppStore>
          <main className={roboto?.className}>
            <ToastContainer />
            <DefaultLayout>
              <Component {...pageProps} />
            </DefaultLayout>
          </main>
        </AppStore>
        </SessionProvider>
      </Elements>
  
  );
}

