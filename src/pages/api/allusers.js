import { connect } from "../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../models/users";

const corsOptions = {
  origin: process.env.Cross_URL,
};
async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return GETAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}
const GETAPI = async (req, res) => {
  await connect();
  try {

    const users = await User.find();

    return res.status(200).json(users);
    
  } catch (error) {
    return res.status(500).json(error);
    // return res.status(500).json(error)
  }
};

export default handler;
