import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";
import axios from "axios";
import { User } from "../../../models/users";
import { connect } from "../../../database/db";
import { compare } from "bcryptjs";
export default async function Auth(req, res) {
  return await NextAuth(req, res, {
    providers: [
      GoogleProvider({
        clientId: process.env.GoogleclientId,
        clientSecret: process.env.GoogleclientSecret,
        authorization: {
          params: {
            prompt: "consent",
            access_type: "offline",
            response_type: "code",
          },
        },
      }),
      CredentialsProvider({
        name: "Credentials",
        async authorize(credentials) {
          return (
            (await axios
              .post(
                `${process.env.NEXTAUTH_URL}/api/authenticated/login/login`,
                credentials
              )
              .then((response) => {
                return response.data;
              })
              .catch((error) => {
                throw new Error(error.response.data);
              })) || null
          );
        },

        // async authorize(credentials) {
        //   const { email, password } = credentials;
  
        //   try {
        //     await connect();
        //     const user = await User.findOne({ "info.email":email });
  
        //     if (!user) {
        //       return null;
        //     }
  
        //     const passwordsMatch = await compare(password, user?.info?.password);
  
        //     if (!passwordsMatch) {
        //       return null;
        //     }
  
        //     return user?.info;
        //   } catch (error) {
        //     console.log("Error: ", error);
        //   }
        // },
      }),
    ],

    callbacks: {
      async signIn(data) {
        (await axios
          .post(`${process.env.NEXTAUTH_URL}/api/auth/google`, data)
          .then((response) => {
            return response.data;
          })
          .catch((error) => {
            throw new Error(error.response.data);
          })) || null;

        return true;
      },

      session: async ({ session, token }) => {
        if (session?.user) {
          session.user._id = token._id;
          session.user.email = token.email;
          session.user.role = token.role;
        }
        
        return session;
      },
      jwt: async ({ user, token }) => {
        if (user) {
          token._id = user._id;
          token.role = user.role;
          token.email = user.email;
        }
       
        return token;
      },

      // async jwt({ token, account }) {
      //   // Persist the OAuth access_token to the token right after signin
      //   if (account) {
      //     token.accessToken = account.access_token;
      //     token._id = account._id;
      //   }
      //   // console.log("in jwt",token)
      //   return token;
      // },
      // async session({ session, token, user }) {
      //   // Send properties to the client, like an access_token from a provider.
      //   session.accessToken = token.accessToken;
      //   session._id = token._id;
      //   return session;
      // },

      // If we want to access our extra user info from sessions we have to pass it the token here to get them in sync:
      // session: async ({ session, token }) => {
      //   if (token) {
      //     session.user = token.user;
      //   }
      //   return session;
      // },
    },
    // redirect: async ({ url, baseUrl }) => {
    //   // Allows relative callback URLs
    //   if (url.startsWith("/")) return `${baseUrl + url}`;
    //   // Allows callback URLs on the same origin
    //   else if (new URL(url).origin === baseUrl) return url;
    //   return baseUrl;
    // },

    secret: "XH6bp/TkLvnUkQiPDEZNyHc0CV+VV5RL/n+HdVHoHN0=sh",
    session: {
      strategy: "jwt",
    },
    pages: {
      signIn: "/",
    },
  });
}
