import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../../models/users";
import { connect } from "../../../database/db";
const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return POSTAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}
const POSTAPI = async (req, res) => {
  await connect();
  try {
    const { email, name, image } = req.body.user;

    const userRes = await User.findOne({ 'info.email': email});

    if (!userRes) {
      //   const error = {
      //     message: "No user Found with Email Please Sign Up...!",
      //     status: 404,
      //   };
      //   return res.status(404).json(JSON.stringify(error));

      const hashpassword = await bcrypt.hash(name, 12);

     
      const data = {
        info: {
          name: name,
          email: email,
          slug: name.replaceAll(" ", "-").toLowerCase(),
          password: hashpassword,
        },
      };

      const newUser = await new User(data);
      const create = await newUser.save();

      const datas = {
        name:create.info.name,
        email:create.info.email
      }
      return res.status(200).json(datas);

    } else {
      const datas = {
        name:userRes.info.name,
        email:userRes.info.email
      }
      return res.status(200).json(datas);
    }
  } catch (error) {
    return res.status(500).json(JSON.stringify(error));
    // return res.status(500).json(error)
  }
};

export default handler;
