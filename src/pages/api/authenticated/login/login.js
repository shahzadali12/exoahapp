import { connect } from "../../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../../../models/users";

const corsOptions = {
  origin: process.env.Cross_URL,
};
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return POSTAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}
const POSTAPI = async (req, res) => {
  await connect();
  try {
    const { email, password } = req.body;
    


    const userRes = await User.findOne({ 'info.email': email});
    
  

    if (!userRes) {
      const error = {
        message: "No user Found with Email Please Sign Up...!",
        status: 404,
      };
      return res.status(404).json(JSON.stringify(error));
    }else{
      const checkPassword = await compare(password, userRes?.info?.password);
      
      
      if (!checkPassword) {
        const error = {
          message: "Email or Password is incorrect! please try again later",
          status: 401,
        };
        return res.status(401).json(JSON.stringify(error));
      }else{
      

        const data = {
          name: userRes.info.name,
          email: userRes.info.email,
          role: userRes.info.role,
        }
       return res.status(200).json(data);
      }
    }

  } catch (error) {
    const systemerror = {
      message: "No user Found with this Account Please Sign Up...!",
      status: 404,
    };
    return res.status(500).json(JSON.stringify(systemerror));
    // return res.status(500).json(error)
  }
};

export default handler;
