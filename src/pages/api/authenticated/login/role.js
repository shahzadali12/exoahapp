// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../../../models/users";

const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "PUT":
        return PUTAPI(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}
const PUTAPI = async (req, res) => {
  try {
    await connect();

    const { email,  role, zoom,name, } = req.body;

   
    if (!email) return res.status(422).send({ message: "felids are requried" });
    // const hashpassword = await bcrypt.hash(password, 12);
    // return res.status(201).json("here 2");
      if(zoom){
        const updatedObject = await User.findOneAndUpdate(
          { "info.email": email },
          {
            $set: {	
              info: {
                zoom: zoom,
              }
            }
          },
          { new: true }
        );
        return res.status(201).json(updatedObject);
      }else{
        const updatedObject = await User.findOneAndUpdate(
          { "info.email": email },
          {
            $set: {
              info: {
                email: email,
                name: name,
                slug: name.replaceAll(" ", "-").toLowerCase(),
                role: role
              },
             
            }
          },
          { new: true }
        );
        return res.status(201).json(updatedObject);
      }
      
      
    
  } catch (error) {
    const systemerror = {
      message: "No user Found with this Account Please Sign Up...!",
      status: 404,
    };
    return res.status(500).json(JSON.stringify(systemerror));
  }
};
