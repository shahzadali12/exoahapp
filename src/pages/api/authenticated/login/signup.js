// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../../../models/users";
import moment from "moment";
import axios from "axios";

const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return POSTApi(req, res);
      case "PUT":
        return PUTAPI(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}
const PUTAPI = async (req, res) => {
  try {
    await connect();

    const { info, links, availability, prices } = req.body;


    if (!info?.email) {return res.status(422).send({ message: "felids are requried" });}
    // const hashpassword = await bcrypt.hash(password, 12);

    // info: {
    //   name: info?.name,
    //   profilePhoto: info?.profilePhoto,
    //   designation: info?.designation,
    //   languages: info?.languages,
    //   category: info?.category,
    //   phone: info?.phone,
    //   role: info?.role,
    //   shortinfo: info?.shortinfo,
    //   about: info?.about,
    // },
    // links: {
    //   fb: links.fb,
    //   twlinks: links.twlinks,
    //   insta: links.insta,
    //   ldklink: links.ldklink,
    // },
    // availability: {
    //   break: availability.break,
    //   slotDuration: availability.slotDuration,
    //   times: availability.times,
    // },
    // prices: {
    //   messages: {
    //     chat: prices.messages.chat,
    //     audio: prices.messages.audio,
    //     video: prices.messages.video,
    //   },
    //   call: {
    //     voice: {
    //       min15: {
    //         check: prices.call.voice.min15.check,
    //         price: prices.call.voice.min15.price,
    //       },
    //       min30: {
    //         check: prices.call.voice.min30.check,
    //         price: prices.call.voice.min30.price,
    //       },
    //       min45: {
    //         check: prices.call.voice.min45.check,
    //         price: prices.call.voice.min45.price,
    //       },
    //     },
    //     video: {
    //       min15: {
    //         check: prices.call.video.min15.check,
    //         price: prices.call.video.min15.price,
    //       },
    //       min30: {
    //         check: prices.call.video.min30.check,
    //         price: prices.call.video.min30.price,
    //       },
    //       min45: {
    //         check: prices.call.video.min45.check,
    //         price: prices.call.video.min45.price,
    //       },
    //     },
    //   },
    // },

    // if(!name || !profilePhoto || !shortinfo || !title || !phone || !language || !about){

    
  

   
      const updatedObject = await User.findOneAndUpdate(
        { 'info.email': info.email },
        {
          $set: {
            info, links, availability, prices 
          },
        },
        
        { new: true }
      );

      
      return res.status(201).json(updatedObject);
    
    
  
    
    // }else{
    //   // return res.status(201).json("here 2");

    //   const updatedObject = await User.findOneAndUpdate(
    //     { email: email },
    //     {
    //       $set: {
    //         role: role,
    //       }
    //     },
    //     { new: true }
    //   );
    //   return res.status(201).json(updatedObject);
    // }
  } catch (error) {
    const systemerror = {
      message: "No user Found with this Account Please Sign Up...!",
      status: 404,
    };
    return res.status(500).json(JSON.stringify(systemerror));
  }
};

const POSTApi = async (req, res) => {
  try {
    await connect();

    const { name, email, password, role } = req.body.info;

    if (!email) return res.status(422).send({ message: "felids are requried" });


      const hashpassword = await bcrypt.hash(password, 12);

      const data = {
        info: {
          name: name,
          email: email,
          slug: name.replaceAll(" ", "-").toLowerCase(),
          role: role,
          password: hashpassword,
        }
      }
    
    const newRecord = await new User(data);
    
    await newRecord.save();

    return res.status(201).json(newRecord);
   
  } catch (error) {
    // const systemerror = {
    //   message: "No user Found with this Account Please Sign Up...!",
    //   status: 404,
    // };
    if(error?.code === 11000) {
      return res.status(409).json("That Email is taken. Try another.");
    }else{
      return res.status(500).json(error);
    }
    
  }
};
