// import { sql_query } from "../../../../lib/db";
import cors from 'cors';
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return getTask(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
 
}
const getTask = async (req, res) => {
  try {
    // const result = await sql_query("SELECT * FROM users WHERE email = ?", [
    //   req.query.email,
    // ]);
    return res.status(200).json("hello");
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
};

