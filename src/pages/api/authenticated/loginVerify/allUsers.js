// import { sql_query } from "../../../../lib/db";
import cors from "cors";
const corsOptions = {
  origin: process.env.Cross_URL,
};
// import { TEST_FILE } from "../../../../components/Tst/file";
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};

  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GET(req, res);
      case "POST":
        return POST(req, res);
      case "PUT":
        return PUT(req, res);
      case "PATCH":
        return DELETE(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  try {
    const userData = await sql_query(`SELECT * FROM users`);
    return res.status(200).json(userData);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};

const PUT = async (req, res) => {
  const { first_name, country, phone, id, profile_img, description, email } = req.body;
  try {
    // const userData = await sql_query(
    //   `UPDATE users SET first_name=?,description=?,country=?,phone=?,profile_img=? WHERE id = ?`,
    //   [first_name, description, country, phone, profile_img, id]
    // );
    // const EmailData = {
    //   id: 17,
    //   payload: req.body
    // }
    // await TEST_FILE(EmailData)

    return res.status(200).json("Successfully Updated!");
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};

const POST = async (req, res) => {
  const { id, verify_id, status_id } = req.body;

  try {
    // const userData = await sql_query(
    //   `UPDATE users SET email_verify = ?, status = ? WHERE id = ?`,
    //   [verify_id, status_id, id]
    // );

    return res.status(200).json("Successfully Updated!");
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};

const DELETE = async (req, res) => {
  const { id } = req.body;
  try {
    // const userData = await sql_query(`DELETE FROM users WHERE id = ?`, [id]);
    return res.status(200).json("user Delete Successfully.");
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};
