// import { sql_query } from "../../../lib/db";
import cors from "cors";
const corsOptions = {
  origin: process.env.Cross_URL
};
async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "PUT": return PaymentMethods(req, res);
     
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}


const PaymentMethods = async (req, res) => {
  try {
    const {
      user_id,
      payout_method,
      payout_account_id,
      routing_number,
      payout_email,
    } = req.body;

    // const id = await sql_query(`SELECT id FROM users WHERE id = ?`, [user_id]);

    if (!id) {
      return res.status(404).json("User Not Found!");
    }

    // const updated = await sql_query(
    //   "UPDATE users SET payout_method = ? ,payout_account_id = ? ,routing_number = ?, payout_email = ? WHERE id = ?",
    //   [payout_method, payout_account_id, routing_number, payout_email, user_id]
    // );

    return res.status(200).json("Payment method added sucessfully");
  } catch (error) {
    return res.status(500).json("here is error message");
  }
};
export default handler;
