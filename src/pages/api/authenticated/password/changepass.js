import axios from "axios";
import bcrypt from "bcryptjs";
const jwt = require("jsonwebtoken");
import { compare } from "bcryptjs";
import cors from "cors";
import { User } from "../../../../models/users";
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function Handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return Authenticate(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}
async function Authenticate(req, res) {
  try {
    if (!req.body.email || !req.body.currentPassword || !req.body.password) {
      return res.status(400).json("Method not allowed");
    }

    const user = await User.findOne({ "info.email": req.body.email });

    const alreadyChanged = await compare(
      req.body.password,
      user?.info?.password
    );

    if (alreadyChanged) {
      return res.status(402).json("You Already Changed Password.");
    }

    const checkPassword = await compare(
      req.body.currentPassword,
      user?.info?.password
    );

    if (!checkPassword) {
      return res
        .status(401)
        .json("Your Current Password is incorrect! please try again later");
    }

    const JWT_SECRET = "XH6bp/TkLvnUkQiPDEZNyHc0CV+VV5RL/n+HdVHoHN0=shahzad";

    const secret = JWT_SECRET + user?.info?.password;

    // if (!req?.headers.authorization) {
    //     return res.status(401).send('Unauthorized header')
    // }

    // const token = req.headers.authorization.replace('Bearer ', '');

    // const decoded = jwt.verify(token, secret);
    // if(!decoded){
    //   res.status(400).json("token not valid");
    // }

    const hashpassword = await bcrypt.hash(req.body.password, 12);

    const updatedObject = await User.updateOne(
      { "info.email": req.body.email },
      {
        $set: {
          "info.password": hashpassword,
        }
      }
    );


    //  await User.findOneAndUpdate(
    //   { 'info.email': req.body.email },
    //   {
    //     $set: {
    //       info : {
    //         password : hashpassword
    //       }
    //     },
    //   },

    // );

    return res.status(201).json(updatedObject);

    // const payload = {
    //   id:id,
    //   first_name:first_name,
    //   description:description,
    //   profile_img:profile_img,
    //   email:email,
    //   country:country,
    //   phone:phone,
    //   ip:ip,
    //   date_created:date_created,
    //   user_type:user_type,
    //   money:money,
    //   payout_method:payout_method,
    //   payout_account_id:payout_account_id,
    //   routing_number:routing_number,
    //   payout_email:payout_email,
    // }
    // const EmailData = {
    //   id: 6,
    //   payload
    // }
    //  await TEST_FILE(EmailData)
    return res.status(201).json("your password changed successfully");
  } catch (error) {
    return res.status(401).json(error);
  }
}
