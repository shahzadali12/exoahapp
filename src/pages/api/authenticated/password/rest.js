import axios from "axios";

// const jwt = require("jsonwebtoken");
import { connect } from "../../../../database/db";
import cors from 'cors';
import { User } from "../../../../models/users";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function Handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST": return Authenticate(req, res);  
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

async function Authenticate(req, res) {
  try {
    await connect();


    const user = await User.findOne({"info.email": req?.body?.email});


   
    if(req.body.email !== user?.info?.email){
       return res.status(404).send("User not registered in database")
    }
    
    // const JWT_SECRET="asdasdaskdjasdoaiusdojqowiqoeiwmqoiwmeoqwme"
    
    const secret = JWT_SECRET + user?.info?.password 

    const payload = {
      id: user._id,
      name: user?.info?.name,
      email: user?.info?.email 
    }
    const tokens = jwt.sign(payload, secret, {expiresIn: "15m"})
    const urllink = `${process.env.NEXTAUTH_URL}/Forgot?token=${tokens}`

    // const sending = {
    //   email: user.email,
    //   name: user.first_name,
    //   link:urllink
    // }
    //   const EmailData = {
    //     id: 5,
    //     payload: sending
    //   }
  
      // await TEST_FILE(EmailData)
      
    
    return res.status(200).json('Your password reset!');
} catch (error) {
    return res.status(422).json(error);
  }
}

