// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { User } from "../../../models/users";
import moment from "moment";
import axios from "axios";

const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "PUT":
        return PUTAPI(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}
const PUTAPI = async (req, res) => {
  try {
    await connect();
    const { info, availability,currentuser } = req.body;

    if (!info?.id) {return res.status(422).send({ message: "felids are requried" });}
    

  
      const updatedObject = await User.findOneAndUpdate(
        { '_id': info.id },
        {
          $set: {
            availability
          },
        },
        
        { new: true }
      );

      const query = { 'info.email': currentuser.email }
      const famousquery = { '_id': info.id }
      const bookedupdate = {
        $push: { booked_slots: { $each: [currentuser?.meeting] } },
        $inc: { countField: 1 } // Increment a field in the document
      };

      const update = {
        $push: { meetings: { $each: [currentuser?.meeting] } },
        $inc: { countField: 1 } // Increment a field in the document
      };

      const result = await User.findOneAndUpdate(query, update, { returnOriginal: false });
      await User.findOneAndUpdate(famousquery, bookedupdate, { returnOriginal: false });


      return res.status(201).json("successfully booked");

  } catch (error) {
    const systemerror = {
      message: "No user Found with this Account Please Sign Up...!",
      status: 404,
    };
    return res.status(500).json(JSON.stringify(systemerror));
  }
};


