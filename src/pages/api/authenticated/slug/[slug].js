
import cors from "cors";
import { User } from "../../../../models/users";
import { connect } from "../../../../database/db";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GET(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  await connect();
  const { slug } = req.query;

  try {
    
    const {info,links,prices,_id,messages} = await User.findOne({"info.slug":slug})

    
    const infos = {
      name: info.name,
      designation: info.designation,
      category: info.category,
      profilePhoto: info.profilePhoto,
      role: info.role,
      slug: info.slug,
      shortinfo: info.shortinfo,
      about: info.about,
      languages: info.languages,
    }
    

      const Data = {
        infos,links,prices,_id,messages
      }
      return res.status(200).json(Data);
    
    
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};









