
import cors from "cors";
import { User } from "../../../../models/users";
import { connect } from "../../../../database/db";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GET(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  await connect();
  const { id } = req.query;

  try {
    
    const Getuser = await User.findOne({"info.email":id})

    return res.status(200).json(Getuser);
    if(Getuser._id){
      // const Data = {
      //   zoomToken: Getuser.zoom,
      //   id:Getuser._id,
      //   name: Getuser.name,
      //   email: Getuser.email,
      //   picture: Getuser.profilePhoto,
      //   role: Getuser.role,
      //   phone: Getuser.phone
      // }
      return res.status(200).json(Getuser);
    }else{
      return res.status(404).json("User not found!");
    }

    
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};









