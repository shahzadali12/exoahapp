import cors from "cors";
import { connect } from "../../../../database/db";
import { User } from "../../../../models/users";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return GET(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  const { email } = req.body;
  await connect();
  try {
    const userRes = await User.findOne({ 'info.email': email});
    return res.status(200).json(userRes?.zoom);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
};
