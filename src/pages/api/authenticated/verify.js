const jwt = require("jsonwebtoken");
// import jwt_decode from "jwt-decode";
import { render } from "@react-email/render";
import UpdateMissing from '../../../components/auth/update'


import { useEffect } from "react";
import moment from "moment/moment";
import Link from "next/link";
import {
  Html,
  Head,
  Body,
  Container,
  Img,
  Hr,
  Preview,
  Text,
} from "@react-email/components";
import { Tailwind } from "@react-email/tailwind";

import cors from "cors";
import axios from "axios";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function Handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return Authenticate(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

async function Authenticate(req, res) {
//   const { verify } = req.body;
//   const decodedHeader = jwt_decode(verify);
//   const { id, name, email } = decodedHeader;
 
  
  const html = render(Thanks("shahzad","hi"), {
    pretty: true,
  });

   
   
    res.statusCode = 200;
    res.write(html);
    return res.end();
  
  

 
}


export function Thanks(props) {


  return (
    <Html lang="en">
      <Head />
      <Body style={main}>
        <Tailwind>
          <Container className="h-screen w-2/4">
            <div className="text-center mb-16 w-3/4 mx-auto	">
              <img
                src="https://backlinkerr.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Flogo.50031f8b.png&w=256&q=75"
                className="w-44"
                alt="Backlinkerr Logo"
              />
            </div>
            <div className="flex relative justify-center flex-col shadow-md rounded-md w-3/4 mx-auto	">
              
              <div className="w-16 h-16 p-2 absolute top-0 -mt-10 mx-auto left-0 right-0 rounded-full bg-slate-50 shadow-sm">
                <img
                  src="https://cdn-icons-png.flaticon.com/512/4315/4315445.png"
                  className="w-full h-full"
                  alt="icon"
                />
              </div>
              <div className="content_area text-center pt-8">
                <h4 className="font-sans text-2xl leading-none my-4 text-slate-700">
                  Sucessfully Verified!
                </h4>
                <p className="text-slate-700 text-sm mb-5 pl-6 pr-6 font-sans">
                  {`Welcome to the biggest marketplace of link building. Please click the below button to login and get started. `}
                </p>
               
              </div>
            </div>

           
          </Container>
        </Tailwind>
      </Body>
    </Html>
  );
}

const main = {
  backgroundColor: "#fff",
  margin:"0",
  // backgroundImage: 'url("https://i.pinimg.com/originals/9b/96/79/9b96799d061a0528da6b0da7bac5374a.gif")',
};

const footer = {
  color: "#333",
  fontSize: "12px",
  fontWeight: 800,
  letterSpacing: "0",
  lineHeight: "23px",
  margin: "0 0px 15px",
  marginTop: "20px",
  textAlign: "center",
  textTransform: "uppercase",
};
