// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { messages } from "../../../models/messages";
import { User } from "../../../models/users";
const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GETApi(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}
const GETApi = async (req, res) => {
  try {
    const {id} = req.query
    await connect();
    // { sernder: id[0],receiver: id[1] }
   
    const open = await messages.find({_id: id[0]});
    // const open2 = await messages.find({receiver: id[0]});

    // // const open = await messages.find({sender: id[0], receiver: id[1]});
    // // const open2 = await messages.find({sender: id[1], receiver: id[0]});

    // const result = [...open, ...open2]

    // result.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));

   
    // const checking = await User.find({ _id : id[0] })
    
    // const checking = await User.aggregate([
    //   {
    //     $match: {
    //       $expr: {
    //         $eq: ['$_id', { $toObjectId: id[0] }] // Convert string to ObjectId
    //       }
    //     }
    //   },
    //   {
    //     $lookup: {
    //       from: 'messages',
    //       localField: 'sender',
    //       foreignField: '_id',
    //       as: 'messages'
    //     }
    //   },
    //   {
    //     $addFields: {
    //       messages: { $ifNull: [ { $arrayElemAt: ["$messages", 0] }, null ] }
    //     }
    //   },
    //   {
    //     $project: {
    //       _id: 1, // Include _id field
    //       info: 1, // Include specific fields from collectionA
    //       'messages.sender': 1, // Include specific fields from collectionB
    //       'messages.receiver': 1, // Include specific fields from collectionB
    //       'messages.mess': 1 // Include specific fields from collectionB
    //     }
    //   }
    // ])

    return res.status(200).json(open);
 
    

    // const checking = await messages.aggregate([
    //   {
    //     $lookup: {
    //       from: "users",
    //       let: { sender: "$sender", receiver: "$receiver" },
    //       pipeline: [
    //         {
    //           $match: {
    //             $expr: {
    //               $or: [
    //                 { $eq: ["$_id", "$$sender"] },
    //                 { $eq: ["$_id", "$$receiver"] }
    //               ]
    //             }
    //           }
    //         },
    //         {
    //           $project: { "info.profilePhoto": 1, "_id": 0 }
    //         }
    //       ],
    //       as: "user_pictures"
    //     }
    //   },
    //   { $unwind: "$user_pictures" },
    //   {
    //     $addFields: {
    //       sender_picture: { $first: "$user_pictures.info.profilePhoto" }
    //     }
    //   },
    //   {
    //     $project: { user_pictures: 0 }
    //   }
    // ])

    

  } catch (error) {
    return res.status(500).json(error);
  }
};
