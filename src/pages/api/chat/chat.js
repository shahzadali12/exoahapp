// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../database/db";
import cors from "cors";
import { compare } from "bcryptjs";
import { messages } from "../../../models/messages";
import {User} from "../../../models/users"
const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return POSTApi(req, res);

      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}

const POSTApi = async (req, res) => {
  try {
    await connect();

    const { chatid, id,receiver, message,date } = req.body;

    // const result = await User.deleteMany({messages: {}});



    //  const result = await messages.updateMany({});

    // return res.status(200).json(result);

    // const result = await User.updateMany(
    //   {},
    //   { $unset: { messages: "" } }
    // );

    // return res.status(200).json(result);

    // const newRecord = await messages.updateOne(
    //   { _id: ObjectId("your_document_id_here") }, // Specify the document by its _id
    //   { $push: { your_array_field: "value_to_add" } } // Use $push to add value to the array
    // );

    // const all = await User.find({_id : id})
    // return res.status(201).json(all);
    const data = {
      messages: { id:id, text:message, date: date }
    };
    

    if(chatid){
      const updatedObject = await messages.updateOne(
        { _id: chatid },
        {
          $push: {messages: { id:id, text:message, date: date } },
        },
      );
      return res.status(201).json(updatedObject);
    }else{

      const newRecord = await new messages(data);

      await newRecord.save();

      if(newRecord._id){
        const current_user = await User.updateOne(
          { _id: id , "messages": {$not: { $eq: newRecord._id }}},
          {
            $addToSet: {messages: newRecord._id },
          },
        );
        const user2 = await User.updateOne(
          { _id: receiver , "messages": {$not: { $eq: newRecord._id }}},
          {
            $addToSet: {messages: newRecord._id },
          },
        );

        return res.status(200).json({current_user,user2});
      }

   
    
    }


   

    
    // const updatedObject = await User.findOneAndUpdate(
    //   { 'info.email': info.email },
    //   {
    //     $set: {
    //       info, links, availability, prices 
    //     },
    //   },
      
    //   { new: true }
    // );

    

   
  } catch (error) {
    return res.status(500).json(error);
  }
};
