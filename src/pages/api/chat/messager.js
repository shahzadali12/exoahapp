// import { sql_query } from "../../../../lib/db";
import { connect } from "../../../database/db";
import { ObjectId } from 'mongodb'; 
import cors from "cors";
import { compare } from "bcryptjs";
import { messages } from "../../../models/messages";
import {User} from "../../../models/users"
const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return POSTApi(req, res);

      default:
        return res.status(400).json({ message: "bad request" });
    }
  });
}



const POSTApi = async (req, res) => {
  try {
    await connect();

    const {message,id} = req.body;

    // const result = await User.deleteMany({messages: {}});

    // const messa = await User.find({_id: "65f3f5eb4f47fbe9448bc6ec"})



   

    // const users = await User.aggregate([
    //     {
    //         $match: {
    //             "_id": "65f3f5eb4f47fbe9448bc6ec" // Replace 'arrayField' with the actual field name in your documents
    //         }
    //     }
    // ])



    
    // const matchedUsers = await User.aggregate([
    //     {
    //         $lookup: {
    //             from: messages,
    //             localField: "_id",
    //             foreignField: "messages",
    //             as: "matchedMessages"
    //         }
    //     },
    //     {
    //         $match: {
    //             "matchedMessages": { $in: req.body }
    //         }
    //     },
    //     {
    //         $project: {
    //             _id: 1,
                
    //         }
    //     }
    // ]).toArray();


    // const users = await User.aggregate([
    //     {
    //         $match: {
    //             messages: { $in: req.body }
    //         }
    //     }
    // ]).toArray();





        // const matchedUsers = await User.aggregate([
        //     {
        //         $lookup: {
        //             from: messages,
        //             localField: "messages",
        //             foreignField: "_id",
        //             as: "matchedMessages"
        //         }
        //     },
        //     {
        //         $match: {
        //             matchedMessages: { $ne: req.body } // Filter users who have matched messages
        //         }
        //     }
        // ]).toArray();


        // const matchedUsers = await User.aggregate([
        //     {
        //         $lookup: {
        //             from: messages,
        //             localField: "messages",
        //             foreignField: "_id",
        //             as: "matchedMessages"
        //         }
        //     },
        //     {
        //         $match: {
        //             "matchedMessages._id": { $in: req.body.map(id => ObjectId(id)) }
        //         }
        //     },
        //     {
        //         $project: {
        //             _id: 1,
        //             // Add other fields you want to retrieve
        //         }
        //     }
        // ]).toArray();

      
        
        const objectIdsToFind = message.map(id => new ObjectId(id));



        const matchedUsers = await User.aggregate([
            {
                $match: {
                    messages: { $in: objectIdsToFind }
                }
            },
            {
                $project: {
                    _id: 1,
                    "info.name": 1,
                    "info.profilePhoto": 1,
                    "info.slug" : 1,
                    "info.designation" : 1,
                    "info.updatedAt": 1,
                    messId: { $arrayElemAt: [objectIdsToFind, 0] }
                }
            },
            
           
           
        ]);


        const respon = matchedUsers.filter((val)=> val._id != id)





   
    
    return res.status(200).json(respon);
   





    


    


    //  const result = await messages.updateMany({});

    // return res.status(200).json(result);

    // const result = await User.updateMany(
    //   {},
    //   { $unset: { messages: "" } }
    // );

    // return res.status(200).json(result);

    // const newRecord = await messages.updateOne(
    //   { _id: ObjectId("your_document_id_here") }, // Specify the document by its _id
    //   { $push: { your_array_field: "value_to_add" } } // Use $push to add value to the array
    // );

    // const all = await User.find({_id : id})
    // return res.status(201).json(all);


    // const data = {
    //   messages: { id:id, text:message, date: date }
    // };
    

    // if(chatid){
    //   const updatedObject = await messages.updateOne(
    //     { _id: chatid },
    //     {
    //       $push: {messages: { id:id, text:message, date: date } },
    //     },
    //   );
    //   return res.status(201).json(updatedObject);
    // }else{

    //   const newRecord = await new messages(data);

    //   await newRecord.save();

    //   if(newRecord._id){
    //     const current_user = await User.updateOne(
    //       { _id: id , "messages": {$not: { $eq: newRecord._id }}},
    //       {
    //         $addToSet: {messages: newRecord._id },
    //       },
    //     );
    //     const user2 = await User.updateOne(
    //       { _id: receiver , "messages": {$not: { $eq: newRecord._id }}},
    //       {
    //         $addToSet: {messages: newRecord._id },
    //       },
    //     );

    //     return res.status(200).json({current_user,user2});
    //   }

   
    
    // }


   

    
    // const updatedObject = await User.findOneAndUpdate(
    //   { 'info.email': info.email },
    //   {
    //     $set: {
    //       info, links, availability, prices 
    //     },
    //   },
      
    //   { new: true }
    // );

    

   
  } catch (error) {
    return res.status(500).json(error);
  }
};
