
import cors from "cors";
import { User } from "../../models/users";
import { connect } from "../../database/db";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GET(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  await connect();

  try {
    
    const Creators = await User.find({"info.role" : "creator"})
    return res.status(200).json(Creators);

    if(Getuser._id){
      // const Data = {
      //   zoomToken: Getuser.zoom,
      //   id:Getuser._id,
      //   name: Getuser.name,
      //   email: Getuser.email,
      //   picture: Getuser.profilePhoto,
      //   role: Getuser.role,
      //   phone: Getuser.phone
      // }
      
    }else{
      return res.status(404).json("User not found!");
    }

    
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};









