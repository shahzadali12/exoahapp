import axios from "axios";
import moment from "moment";
import React from "react";
var jwt = require("jsonwebtoken");
import getConfig from "next/config";
import { generateToken } from "../../../auth/token";
// import middleware from '../../auth/middleware';

async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return GETAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}

async function GETAPI(req, res) {
  // await connect();

  const getip = await fetch("https://geolocation-db.com/json/");
  const ip = await getip.json();

  const token = await generateToken(ip);

  return res.status(200).json(token);
}

export default handler;
