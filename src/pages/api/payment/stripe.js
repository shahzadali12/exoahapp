
import { Stripe } from 'stripe';
// const stripe = require('stripe')('sk_test_51OZqMFB044ieUnHAsB1Ut1JQMFdbu8hq4GDy5Co0ycL5GhtZ2HmAHhRGNbEMPvXSfA5QSnfiShaplUXL6hDyGKU200wen9tJnz');

const stripe = new Stripe('sk_test_51OZqMFB044ieUnHAsB1Ut1JQMFdbu8hq4GDy5Co0ycL5GhtZ2HmAHhRGNbEMPvXSfA5QSnfiShaplUXL6hDyGKU200wen9tJnz', {
  apiVersion: '2020-08-27',
});

// import { Stripe } from '@stripe/stripe-js';

import cors from "cors";


const corsOptions = {
  origin: process.env.Cross_URL,
};
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return POSTAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}

async function POSTAPI(req, res) {

  const { paymentMethodId,amount } = req.body;
  // const { item } = req.body;

  // const redirectURL =
  //   process.env.NODE_ENV === 'development'
  //     ? 'http://localhost:3000'
  //     : 'https://stripe-checkout-next-js-demo.vercel.app';

  // const transformedItem = {
  //   price_data: {
  //     currency: 'usd',
  //     product_data: {
  //       images: [item.image],
  //       name: item.name,
  //     },
  //     unit_amount: item.price * 100,
  //   },
  //   description: item.description,
  //   quantity: item.quantity,
  // };

  // const session = await stripe.checkout.sessions.create({
  //   payment_method_types: ['card'],
  //   line_items: [transformedItem],
  //   mode: 'payment',
  //   success_url: redirectURL + '?status=success',
  //   cancel_url: redirectURL + '?status=cancel',
  //   metadata: {
  //     images: item.image,
  //   },
  // });

  // res.json({ id: session.id });

  try {
    // Create a PaymentIntent on the server

    // let formattedNumber = (parseInt(amount) * 100).toFixed(2);

    const intent = await stripe.paymentIntents.create({
      amount: 100, // Amount in cents
      currency: 'usd',
      payment_method: paymentMethodId,
      confirm: true
    });
    // { success: true }
    // If successful, return a success response
    return res.status(200).json(intent);
  } catch (error) {
    console.error(error);
    return res.status(500).json(error);
  }
}

export default handler;