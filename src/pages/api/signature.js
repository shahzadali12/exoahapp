
import cors from "cors";
const bcrypt = require("bcryptjs");
const corsOptions = {
  origin: process.env.Cross_URL,
};
const KJUR = require('jsrsasign')
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return Signature(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}



function Signature(req,res) {


    const iat = Math.round(new Date().getTime() / 1000) - 30;
    const exp = iat + 60 * 60 * 2
  
    const oHeader = { alg: 'HS256', typ: 'JWT' }
   
    const oPayload = {
      sdkKey: 'l9WEJktBRg2m0TNfSwlpA',
      mn: req.body.meetingNumber,
      role: req.body.role,
      iat: iat,
      exp: exp,
      // appKey: 'u8t6JzlhToWqPjlqMWROw',
      // tokenExp: iat + 60 * 60 * 2
    }
  
    const sHeader = JSON.stringify(oHeader)
    const sPayload = JSON.stringify(oPayload)
    const signature = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, 'ITMbmmHE3uCqWAyJd7wCHF6W1FBxg65M')
  
    res.json({
      signature: signature
    })
}


export default handler
