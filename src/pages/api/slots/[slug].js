
import cors from "cors";
import { User } from "../../../models/users";
import { connect } from "../../../database/db";
const corsOptions = {
  origin: process.env.Cross_URL
};
export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};
  cors(options)(req, res, () => {
    switch (req.method) {
      case "GET":
        return GET(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

const GET = async (req, res) => {
  await connect();
  const { slug } = req.query;

  try {
    
    const {availability,zoom} = await User.findOne({"info.slug":slug})

    const data = {
      availability,zoom
    }
      return res.status(200).json(data);
    
    
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};









