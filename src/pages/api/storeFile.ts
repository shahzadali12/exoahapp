import { NextApiHandler, NextApiRequest } from "next";
import formidable from "formidable";
import path from "path";
import fs from "fs/promises";
import cors from "cors";
const corsOptions = {
  origin: process.env.Cross_URL,
};

export default async function handler(req, res) {
  const requestedDomain = req.headers.origin;
  const options = corsOptions[requestedDomain] || {};

  cors(options)(req, res, () => {
    switch (req.method) {
      case "POST":
        return POST(req, res);
      default:
        return res.status(400).send("Method not allowed");
    }
  });
}

export const config = {
  api: {
    bodyParser: false,
  },
};

const readFile = (
  req: NextApiRequest,
  saveLocally?: boolean
): Promise<{ fields: formidable.Fields; files: formidable.Files }> => {
  const options: formidable.Options = {};
  if (saveLocally) {
    options.uploadDir = path.join(process.cwd(), "/public/images/users");
    options.filename = (name, ext, path, form) => {
      return Date.now().toString() + path.originalFilename;
    };
  }
  options.maxFileSize = 4000 * 1024 * 1024;
  const form = formidable(options);
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) reject(err);
      resolve({ fields, files });
    });
  });
};

const POST: NextApiHandler = async (req, res) => {
  try {
    try {
      await fs.readdir(
        path.join(process.cwd() + "/public/images/users")
      );
    } catch (error) {
      await fs.mkdir(path.join(process.cwd() + "/public/images/users"));
    }
    const filename = await readFile(req, true);
    
    if (filename?.files?.profileImg) {
      const [data] = filename?.files?.profileImg && filename?.files?.profileImg;
      return res.status(200).json(data);
    }
    else if (filename?.files?.userImage) {
      const [data] = filename?.files?.userImage && filename?.files?.userImage;
      return res.status(200).json(data);
    }else if(filename.files?.content){
      // const [data] = filename?.files?.content && filename?.files?.content;
      return res.status(200).json(filename);
    }
  } catch (error) {
    return res.status(500).json(error);
  }
};
