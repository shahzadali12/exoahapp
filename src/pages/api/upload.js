import cloudinary from '../../database/cloudinary';
import multer from 'multer';
// const upload = multer({ dest: 'uploads/' });

// export const config = {
//   api: {
//     bodyParser: false,
//   },
// };

async function handler(req, res) {
    switch (req.method) {
      case "POST":
        return POSTAPi(req, res);
      default:
        return res.status(400).json({ message: "bad request" });
    }
  }
  
  async function POSTAPi(req, res) {

    return res.json(JSON.stringify(req.files));
    
    try {

        
        const { path } = req.files[0]; // Adjust the file handling based on your needs
  
        // Upload to Cloudinary
        const result = await cloudinary.uploader.upload(path, {
          resource_type: 'auto', // 'auto' detects the file type
        });
  
        // Respond with the Cloudinary URL or other data
        res.status(200).json({ url: result.secure_url });
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
      }
    
}






export default handler