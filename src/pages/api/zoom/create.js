import axios from "axios";
import moment from "moment";
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return POSTAPI(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}

async function POSTAPI(req, res) {
  const { token, topic, start_time, duration, password } = req.body;

  const response = await axios.post(
    "https://api.zoom.us/v2/users/me/meetings",
    {
      topic: topic,
      type: 2,
      start_time: start_time,
      duration: duration,
    },
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }
  );

  const data = {
    start: response?.data?.start_url,
    join: response?.data?.join_url,
    password: response?.data?.password,
    timezone: response?.data?.timezone,
    status: response?.data?.status,
    meetingid: response?.data?.id,
  };

  // const data = {
  //   start: "start_url",
  //   join: "join_url",
  //   password: "password",
  //   timezone: "timezone",
  //   status: "status",
  //   meetingid: "id"
  // }

  return res.status(200).json(data);
}
export default handler;
