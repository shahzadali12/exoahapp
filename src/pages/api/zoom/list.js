import axios from "axios";
async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return GETPOST(req, res);
    default:
      return res.status(400).json({ message: "bad request" });
  }
}

async function GETPOST(req, res) {
  const {token} = req.body;

  
  const axiosOptions = {
    method: 'get',
    url: "https://api.zoom.us/v2/users/me/meetings",
    headers: {
      // 'Authorization': `Bearer eyJzdiI6IjAwMDAwMSIsImFsZyI6IkhTNTEyIiwidiI6IjIuMCIsImtpZCI6IjIzYTkzZGM1LWE5OTEtNGRhMi1hMjEwLWQ1NmE2MTA3MTFkNiJ9.eyJ2ZXIiOjksImF1aWQiOiIzNDY3OGNmMWY2YmFiMGMwMDkzZGZiZmM2MTNlMTcyMCIsImNvZGUiOiI0bGhhaDZzczBtNzlLRUdSTHczU2QtS3dDRk9HTnZjX0EiLCJpc3MiOiJ6bTpjaWQ6dTh0Nkp6bGhUb1dxUGpscU1XUk93IiwiZ25vIjowLCJ0eXBlIjowLCJ0aWQiOjAsImF1ZCI6Imh0dHBzOi8vb2F1dGguem9vbS51cyIsInVpZCI6Ijg4TDRyNzc3UjFLNkpqZzZkUUpyZXciLCJuYmYiOjE3MDU0MjE2OTAsImV4cCI6MTcwNTQyNTI5MCwiaWF0IjoxNzA1NDIxNjkwLCJhaWQiOiJYdWc5WkxMbFJxLUMyeFBvQTF6VXNRIn0.QkNvxFojLd-yKYQHUH0KXEc516EkNdcHBIbnH-P2Ld9k5UUbuNSP99z-DSpMQFSKFt6VxtCm9KL62FVRvlWmTg`,
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
      // 'Authorization': `Basic ${base64Credentials}`
    }
  };
  
 const respoc =  await axios(axiosOptions)
    .then(response => {  
      return response.data
    })
    .catch(error => {
      console.error('Error fetching Zoom meetings:', error);
    });

    return res.status(200).json(respoc)
}
export default handler;