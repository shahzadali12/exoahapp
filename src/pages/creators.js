import Link from "next/link";
import React, { useState, useEffect, useContext } from "react";

import FilterMenu from "../components/filterMenu";
import { Store } from "../store/AppStore";
import axios from "axios";
function Creators() {
  const { dataa, filtered, setSelection,uniqueObjects } = useContext(Store);

  const [SmallDevices, setScreen] = useState(false);
 
  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);

  const OnSelection = (data) => {
    
    const [options] =
      data &&
      [data].map((vel) => {
        const name = {
          label: vel.label,
          value: vel.value,
        };
        return name;
      });
     
    setSelection(options);
  };

  
  const all = {label: "All", value: "All"}



  return (
    <div className="bg-black float-left w-full py-10">
      <div className="container">
        <div
          className={`flex justify-between w-full ${
            SmallDevices ? "flex-col" : ""
          }`}
        >
          {SmallDevices ? (
            ""
          ) : (
            <div className="column w-1/5">
              <h3 className="text-white font-medium text-2xl">
                Search by Category:
              </h3>

              <ul className="flex justify-normal flex-wrap p-0">
                <li className="mb-2 mr-2">
                  <button
                    onClick={() => OnSelection(all)}
                    className="px-2 py-2 bg-[#3F267C] text-white inline-block rounded-md"
                  >
                    All
                  </button>
                </li>
                {uniqueObjects.map((val, index) => {
                  return (
                    <li key={index} className="mb-2 mr-2">
                      <button
                        onClick={() => OnSelection(val)}
                        className="px-2 py-2 bg-[#3F267C] text-white inline-block rounded-md"
                      >
                        {val.label}
                      </button>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}

          <div
            className={
              SmallDevices ? "column w-full text-center px-6" : "column w-4/5"
            }
          >
            <h3 className="text-white font-medium text-4xl mb-3">
              Find your Favorite Creators from any industry
            </h3>

            {SmallDevices ? (
              <>
                {" "}
                <div className="mb-3 float-left w-full">
                  <span className="inline-block text-black text-xl bg-white font-bold rounded-md px-2 py-1 border border-gray-400">
                    Trending Creators
                  </span>
                </div>{" "}
                <div className="w-full text-left flex justify-end items-center mb-3">
                  <FilterMenu name={"Texting"} />{" "}
                </div>{" "}
              </>
            ) : (
              <div className="mb-3 float-left w-full">
                <span className="inline-block text-black text-xl bg-white font-bold rounded-md px-2 py-1 border border-gray-400">
                  Trending Creators
                </span>
              </div>
            )}
            <div className="flex justify-between items-center flex-wrap w-full">
              {filtered.map((val, i) => {
                return (
                  <div
                    key={i}
                    className={
                      SmallDevices ? "grid_item mb-3 w-full" : "grid_item mb-3 w-1/4 overflow-hidden pr-4"
                    }
                  >
                    <Link href={val?.info?.slug ? val?.info?.slug : val?.info?.name}>
                      <img
                        src={process.env.NODE_ENV === "development" ? process.env.NEXTAUTH_URL + "/images/users/" + val?.info?.profilePhoto : process.env.NEXTAUTH_URL + "/public/images/users/" + val?.info?.profilePhoto}
                        className="mb-2 border-3 border-white rounded-3xl w-full h-80 object-cover"
                        alt="image"
                      />
                      <div className="description">
                        <h6 className="text-white capitalize text-2xl mb-0 font-semibold">
                          {val?.info?.name}
                        </h6>
                        <p className="text-gray-600 capitalize mb-0">
                          {val?.info?.designation}
                        </p>
                      </div>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}



// export async function getServerSideProps({ params }) {
//   // const session = await getSession({ req });
//   const [slug] = params.slug;

//    const listingAPi = await fetch(
//     `${process.env.NEXTAUTH_URL}/api/authenticated/slug/${slug}`
//   );
//   const apiData = await listingAPi.json();

  
//   if (!apiData) {
//     return {
//       redirect: {
//         destination: `${process.env.NEXTAUTH_URL}/`,
//         permanent: false,
//       },
//     };
//   }


//   return { props: { apiData } };
// }




export default Creators;
