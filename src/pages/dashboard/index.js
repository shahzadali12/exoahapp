"use client";
import Login from "../../components/auth/login";
import Register from "../../components/auth/register";
import Header from "../../components/header";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import PaymentsIcon from "@mui/icons-material/Payments";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import OutboundIcon from "@mui/icons-material/Outbound";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import CreditScoreIcon from "@mui/icons-material/CreditScore";
import { getSession, useSession, signOut } from "next-auth/react";
import { useEffect } from "react";
import { useRouter } from "next/router";
function DashboardHome() {
  const datas = useSession();
  const route = useRouter();
  useEffect(() => {
    if (!datas?.data) {
      route.push("/login");
    }
  }, [route.pathname]);

  return (
    <>
      <div className="col-xl-4 col-md-6">
        <div className="card bg-secondary-dark dashnum-card text-white overflow-hidden">
          <span className="round small"></span>
          <span className="round big"></span>
          <div className="card-body">
            <div className="row">
              <div className="col">
                <div className="avtar avtar-lg">
                  <i className="ti text-white">
                    <PaymentsIcon />
                  </i>
                </div>
              </div>
              <div className="col-auto">
                <div className="btn-group">
                  <a
                    type="button"
                    className="avtar bg-secondary dropdown-toggle arrow-none"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <i className="ti">
                      <MoreVertIcon />
                    </i>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-end">
                    <li>
                      <button className="dropdown-item" type="button">
                        Import Card
                      </button>
                    </li>
                    <li>
                      <button className="dropdown-item" type="button">
                        Export
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <span className="text-white d-block f-34 f-w-500 my-2">
              1350 <i className="ti ti-arrow-up-right-circle opacity-50"></i>
            </span>
            <p className="mb-0 opacity-50">Total Pending Orders</p>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-md-6">
        <div className="card bg-primary-dark dashnum-card text-white overflow-hidden">
          <span className="round small"></span>
          <span className="round big"></span>
          <div className="card-body">
            <div className="row">
              <div className="col">
                <div className="avtar avtar-lg">
                  <i className="text-white">
                    <CreditCardIcon />
                  </i>
                </div>
              </div>
              <div className="col-auto">
                <ul
                  className="nav nav-pills justify-content-end mb-0"
                  id="chart-tab-tab"
                  role="tablist"
                >
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link text-white active"
                      id="chart-tab-home-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#chart-tab-home"
                      type="button"
                      role="tab"
                      aria-controls="chart-tab-home"
                      aria-selected="true"
                    >
                      Month
                    </button>
                  </li>
                  <li className="nav-item" role="presentation">
                    <button
                      className="nav-link text-white"
                      id="chart-tab-profile-tab"
                      data-bs-toggle="pill"
                      data-bs-target="#chart-tab-profile"
                      type="button"
                      role="tab"
                      aria-controls="chart-tab-profile"
                      aria-selected="false"
                    >
                      Year
                    </button>
                  </li>
                </ul>
              </div>
            </div>
            <div className="tab-content" id="chart-tab-tabContent">
              <div
                className="tab-pane show active"
                id="chart-tab-home"
                role="tabpanel"
                aria-labelledby="chart-tab-home-tab"
                tabIndex="0"
              >
                <div className="row">
                  <div className="col-6">
                    <span className="text-white d-block f-34 f-w-500 my-2">
                      $130
                      <i className="ti opacity-50">
                        <OutboundIcon />
                      </i>
                    </span>
                    <p className="mb-0 opacity-50">Total Earning</p>
                  </div>
                  <div className="col-6">
                    <div id="tab-chart-1"></div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane"
                id="chart-tab-profile"
                role="tabpanel"
                aria-labelledby="chart-tab-profile-tab"
                tabIndex="0"
              >
                <div className="row">
                  <div className="col-6">
                    <span className="text-white d-block f-34 f-w-500 my-2">
                      $29961{" "}
                      <i className="ti opacity-50">
                        <OutboundIcon />
                      </i>
                    </span>
                    <p className="mb-0 opacity-50">C/W Last Year</p>
                  </div>
                  <div className="col-6">
                    <div id="tab-chart-2"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-md-12">
        <div className="card bg-primary-dark dashnum-card dashnum-card-small text-white overflow-hidden">
          <span className="round bg-primary small"></span>
          <span className="round bg-primary big"></span>
          <div className="card-body p-3">
            <div className="d-flex align-items-center">
              <div className="avtar avtar-lg">
                <i className="text-white ti">
                  <CreditScoreIcon />
                </i>
              </div>
              <div className="ms-2">
                <h4 className="text-white mb-1">
                  $203k{" "}
                  <i className="ti opacity-50">
                    <OutboundIcon />
                  </i>
                </h4>
                <p className="mb-0 opacity-50 text-sm">Net Profit</p>
              </div>
            </div>
          </div>
        </div>
        <div className="card dashnum-card dashnum-card-small overflow-hidden">
          <span className="round bg-warning small"></span>
          <span className="round bg-warning big"></span>
          <div className="card-body p-3">
            <div className="d-flex align-items-center">
              <div className="avtar avtar-lg bg-light-warning">
                <i className="text-warning ti">
                  <CreditScoreIcon />
                </i>
              </div>
              <div className="ms-2">
                <h4 className="mb-1">
                  $550K{" "}
                  <i className="ti opacity-50">
                    <OutboundIcon />
                  </i>
                </h4>
                <p className="mb-0 opacity-50 text-sm">Total Revenue</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="col-xl-8 col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="row mb-3 align-items-center">
              <div className="col">
                <small>Total Growth</small>
                <h3>$2,324.00</h3>
              </div>
              <div className="col-auto">
                {/* <select className="form-select p-r-35">
                  <option>Today</option>
                  <option selected>This Month</option>
                  <option>This Year</option>
                </select> */}
              </div>
            </div>
            <div id="growthchart"></div>
          </div>
        </div>
      </div>
      <div className="col-xl-4 col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="row mb-3 align-items-center">
              <div className="col">
                <h4>Popular Stocks</h4>
              </div>
              <div className="col-auto"> </div>
            </div>
            <div className="rounded bg-light-secondary overflow-hidden mb-3">
              <div className="px-3 pt-3">
                <div className="row mb-1 align-items-start">
                  <div className="col">
                    <h5 className="text-secondary mb-0">Bajaj Finery</h5>
                    <small className="text-muted">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">$1839.00</h4>
                  </div>
                </div>
              </div>
              <div id="bajajchart"></div>
            </div>
            <ul className="list-group list-group-flush">
              <li className="list-group-item px-0">
                <div className="row align-items-start">
                  <div className="col">
                    <h5 className="mb-0">Bajaj Finery</h5>
                    <small className="text-success">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">
                      $1839.00
                      <span className="ms-2 align-top avtar avtar-xxs bg-light-success">
                        <i className="ti text-success">
                          <ExpandLessIcon />
                        </i>
                      </span>
                    </h4>
                  </div>
                </div>
              </li>
              <li className="list-group-item px-0">
                <div className="row align-items-start">
                  <div className="col">
                    <h5 className="mb-0">TTML</h5>
                    <small className="text-danger">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">
                      $100.00
                      <span className="ms-2 align-top avtar avtar-xxs bg-light-danger">
                        <i className="ti text-danger">
                          <ExpandMoreIcon />
                        </i>
                      </span>
                    </h4>
                  </div>
                </div>
              </li>
              <li className="list-group-item px-0">
                <div className="row align-items-start">
                  <div className="col">
                    <h5 className="mb-0">Reliance</h5>
                    <small className="text-success">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">
                      $200.00
                      <span className="ms-2 align-top avtar avtar-xxs bg-light-success">
                        <i className="ti text-success">
                          <ExpandLessIcon />
                        </i>
                      </span>
                    </h4>
                  </div>
                </div>
              </li>
              <li className="list-group-item px-0">
                <div className="row align-items-start">
                  <div className="col">
                    <h5 className="mb-0">TTML</h5>
                    <small className="text-danger">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">
                      $189.00
                      <span className="ms-2 align-top avtar avtar-xxs bg-light-danger">
                        <i className="ti text-danger">
                          <ExpandMoreIcon />
                        </i>
                      </span>
                    </h4>
                  </div>
                </div>
              </li>
              <li className="list-group-item px-0">
                <div className="row align-items-start">
                  <div className="col">
                    <h5 className="mb-0">Stolon</h5>
                    <small className="text-danger">10% Profit</small>
                  </div>
                  <div className="col-auto">
                    <h4 className="mb-0">
                      $189.00
                      <span className="ms-2 align-top avtar avtar-xxs bg-light-danger">
                        <i className="ti text-danger">
                          <ExpandMoreIcon />
                        </i>
                      </span>
                    </h4>
                  </div>
                </div>
              </li>
            </ul>

            <div className="text-center">
              <a href="#!" className="b-b-primary text-primary">
                View all{" "}
                <i className="ti">
                  <ChevronRightIcon />
                </i>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}








// export async function getServerSideProps({req,res}) {
//   const session = await getSession({req});
  

//   if (!session) {
//     return {
//       redirect: {
//         destination: process.env.NEXTAUTH_URL + '/login',
//         permanent: false,
//       },
//     };
//   }

//   return { props: {session} };
// }
export default DashboardHome;