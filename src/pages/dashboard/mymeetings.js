import axios from "axios";
import Link from "next/link";
import moment from "moment";
import { useSession, getSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useMemo, useState } from "react";
import { Store } from "../../store/AppStore";

function Rooms() {
  const { userdata } = useContext(Store);
  const [responce, setMeeting] = useState([]);
  const [updateValue, setUpdate] = useState([]);

  const {data:session} = useSession();
    const router = useRouter()
  useEffect(()=>{
    if(!session?.user?.email){
      router.replace("/login")
    }
  },[session])


  const PostApi = async () => {
   
    // ${userdata?._id}

    await axios.get(`/api/mymeetings/${userdata?._id}`).then((result) => setMeeting(result?.data)).catch((error) => {
        console.log(error);
      });

    // const respocs = await postApi.json();
    // console.log(postApi)

    // if(postApi.status){
    //   const respocs = await postApi?.json();
    //   console.log(respocs)

    // }
  };

  useEffect(() => {
    if (userdata?._id) {
      PostApi();
    }
  }, [userdata?._id]);

  const [toggle, setToggle] = useState(false);

  const route = useRouter();
  const JoinHandler = async (data, index) => {
    if (data.id) {
      const datasa = {
        id: data?.id && data?.id,
        token: userdata?.zoom,
      };
      const response = data?.id && (await axios.post("/api/conduct", datasa));
      setToggle(true);
      if (typeof window !== "undefined" && window.localStorage) {
        // Set the new value in localStorage
        localStorage.setItem("meeting", JSON.stringify(response));
        localStorage.setItem("token", JSON.stringify(userdata?.zoom));
      }
      route.push("/meeting");
    } else {
      setToggle(false);
    }
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  return (
    <>
      <div className="flex justify-between  flex-wrap">
      {responce && responce?.map((data, index) => {
          return (
            <div className={`w-1/3 px-2`} key={index}>
              <div
                
                className={`flex flex-col rounded-md  mb-3 px-3 py-3 relative ${
                  moment(new Date()) > moment(data?.date)
                    ? "bg-yellow-300"
                    : "bg-slate-300"
                }`}
              >
                <div className="flex justify-start items-center mb-2">
                  <img
                    src={
                      data?.profilePhoto
                        ? process.env.NODE_ENV === "development"
                          ? process.env.NEXTAUTH_URL +
                            "/images/users/" +
                            data?.profilePhoto
                          : process.env.NEXTAUTH_URL +
                            "/public/images/users/" +
                            data?.profilePhoto
                        : "../current.png"
                    }
                    className="w-16 h-16 object-cover rounded-full mr-4"
                    alt="img here"
                  />
                  <h3>{data.name}</h3>
                  {/* <h3>{data.status}</h3> */}
                </div>

                <span>
                  {data?.date && moment(data?.date).format("DD-MM-YY hh:mm A")}
                </span>
                {/* <span>Meeting: </span> */}
                <span>Duration: {data?.duration}</span>

                {moment(new Date()) > moment(data?.date) ? (
                  <div className="absolute right-4 top-full -mt-20">
                    <button
                      type="button"
                      className="bg-red-600 text-white py-2 px-3 rounded-md"
                    >
                      Expired Meeting
                    </button>
                  </div>
                ) : (
                  <div className="absolute right-4 top-full -mt-20">
                    <Link
                      // type="button"
                      href={`${data?.start}`}
                      // onClick={() => JoinHandler(d, index)}
                      className="bg-sky-600 text-white py-2 px-3 rounded-md"
                    >
                      Join Meeting
                    </Link>
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
}

export default Rooms;

// export async function getServerSideProps({ req, res }) {
//   const session = await getSession({ req });

//   if (!session) {
//     return {
//       redirect: {
//         destination: process.env.NEXTAUTH_URL + "/login",
//         permanent: false,
//       },
//     };
//   }
//   return { props: { session } };
// }
