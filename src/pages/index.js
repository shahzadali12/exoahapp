import React, { useState, useEffect } from "react";
import Banner from "../components/home/banner";
import TeamBack from "../components/home/teams";
import POPUP from "../components/home/dailogbox";
import ChatSystem from "../components/chat";
import Slider from "react-slick";
import VerifiedUserIcon from "@mui/icons-material/VerifiedUser";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import HourglassTopIcon from "@mui/icons-material/HourglassTop";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAlt";
import { useMediaQuery } from "react-responsive";
import Link from "next/link";
import TestimonialCom from "../components/testimonial";
function Home() {
  const isDesktopOrLaptop = useMediaQuery({ minWidth: 1224 });
  const isBigScreen = useMediaQuery({ minWidth: 1824 });
  const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 });
  // const SmallDevices = useMediaQuery({ query: '(max-width: 991px)' })
  const isPortrait = useMediaQuery({ orientation: "portrait" });
  const isRetina = useMediaQuery({ minResolution: "2dppx" });

  const [SmallDevices, setScreen] = useState(false);

  useEffect(() => {
    setScreen(window.innerWidth > 768 ? false : true);
  }, []);

  // console.log(isDesktopOrLaptops,SmallDevices, "here is a desktop")
  var settings = {
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1,
    speed: 20000,
    dots: false,
    infinite: true,
    cssEase: "linear",
    waitForAnimate: true,
    pauseOnFocus: true,
    pauseOnHover: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3.4,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 1,
          speed: 20000,
          dots: false,
          infinite: true,
          cssEase: "linear",
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2.5,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 1,
          speed: 20000,
          dots: false,
          infinite: true,
          cssEase: "linear",
        },
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 1,
          speed: 20000,
          dots: false,
          infinite: true,
          cssEase: "linear",
        },
      },
    ],
  };
  // var Testimonial = {
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   // autoplay: true,
  //   // autoplaySpeed: 1,
  //   // speed: 20000,
  //   dots: false,
  //   infinite: true,
  //   cssEase: "linear",
  //   waitForAnimate: true,
  //   pauseOnFocus: true,
  //   pauseOnHover: true,
  // };

  const Testimonial = {
    centerMode: true,
    centerPadding: "270px",
    slidesToShow: 2,
    focusOnSelect: true,
    dots: false,
    infinite: true,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          centerMode: true,
          centerPadding: "220px",
          slidesToShow: 2,
          focusOnSelect: true,
          dots: false,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          centerMode: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 2,
          centerPadding: "15px",
          infinite: false,
          arrows: false,
          dots: false,
          infinite: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 2,
          centerPadding: "15px",
          infinite: false,
          arrows: false,
          dots: false,
          infinite: true,
        },
      },
      {
        breakpoint: 320,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          arrows: false,
          dots: false,
          infinite: true,
        },
      },
    ],
  };

  const [open, setOpen] = useState(false);

  return (
    <div className="bg-black float-left w-full">
      <Banner />

      {/* <div className="-mt-8 relative mb-5">
        <div className="container">
          <div className={SmallDevices && SmallDevices ? "flex justify-center items-center flex-col" : "flex justify-center items-center m-auto w-3/4"}>
            <img
              src="../img1.png"
              alt="img here"
              className={SmallDevices && SmallDevices ? "w-2/3 border-[8px] border-solid border-[#3F267C] rounded-3xl overflow-hidden" : "min-w-52 border-[8px] border-solid border-[#3F267C] rounded-3xl overflow-hidden"}
            />
            <div className={SmallDevices && SmallDevices ? "p-3 testimonial_b custom_full" : "pl-10 pt-4 testimonial_b"}>
              <p className={SmallDevices && SmallDevices ? "text-gray-600 font-light text-justify text-xl leading-normal mb-2 w-full overflow-hidden" :"text-white font-light text-justify text-2xl leading-normal mb-2 w-2/3 overflow-hidden"}>
                I've been using Insider Interact for a while now and it has been
                an amazing experience for me. It's given me the opportunity to
                connect with my followers on a deeper level and earn in a
                meaningful way
              </p>
              <h6 className={SmallDevices && SmallDevices ? "text-white font-bold text-4xl mb-0" : "text-white font-medium text-4xl mb-0"}>-Sarah,</h6>
              <span className={SmallDevices && SmallDevices ? "text-gray-600 font-light text-justify text-2xl leading-normal pl-3" : "text-white font-light text-justify text-2xl leading-normal pl-3"}>
                lifestyle blogger
              </span>
            </div>
          </div>
        </div>
      </div> */}

      <TestimonialCom
        headstyle={`relative mb-5 ${
          SmallDevices ? "w-full" : "w-[60%]"
        } -mt-8 mx-auto`}
        img="../img1.png"
        title={"Sarah"}
        role={"lifestyle blogger"}
        description="I've been using Insider Interact for a while now and it has been an amazing experience for me. It's given me the opportunity to connect with my followers on a deeper level and earn in a meaningful way"
      />

      <div className="overflow-hidden float-left w-full">
        <Slider {...settings}>
          <div className="p-2">
            <img src="../images/brands/logo1.png" alt="img here" />
          </div>
          <div className="p-2">
            <img src="../images/brands/logo2.png" alt="img here" />
          </div>
          <div className="p-2">
            <img src="../images/brands/logo3.png" alt="img here" />
          </div>
          <div className="p-2">
            <img src="../images/brands/logo4.png" alt="img here" />
          </div>
          <div className="p-2">
            <img src="../images/brands/logo5.png" alt="img here" />
          </div>
        </Slider>
      </div>

      <div className="section float-left w-full bg-triangle">
        <div className="container">
          <div className="heading mb-6 float-left w-full">
            <h3
              className={
                SmallDevices && SmallDevices
                  ? "text-4xl font-light tracking-tight text-white text-center"
                  : "text-8xl font-light tracking-tight text-white sm:text-6xl w-2/4"
              }
            >
              Insider Interact{" "}
              <b className="text-[#3F267C] w-full inline-block">Brings You</b>
            </h3>
          </div>

          <div
            className={`flex justify-between items-center w-full ${
              SmallDevices ? "flex-col-reverse" : ""
            }`}
          >
            <div
              className={`column ${
                SmallDevices && SmallDevices ? "w-full mb-6" : "w-6/12"
              }`}
            >
              <ul
                className={`list_items flex justify-between items-start flex-col p-0 mb-0 ${
                  SmallDevices ? "responsive_list w-[90%]" : "w-4/5"
                }`}
              >
                <li>Easy & Direct Access to World-Renowned Experts</li>
                <li>Experts from Wide Range of Fields to Choose From</li>
                <li>Best Way to Learn from Leading Experts</li>
                <li>Choose to Exclusively Chat, Call, or Video Call</li>
                <li>Easy-to-Use Interface</li>
                <li>Secure Payment Methods</li>
              </ul>
            </div>

            <div
              className={`column interact_content text-center float-right relative ${
                SmallDevices && SmallDevices ? "w-full mb-3" : "w-6/12"
              }`}
            >
              <h4
                className={
                  SmallDevices && SmallDevices
                    ? "text-2xl text-white capitalize font-medium"
                    : "text-6xl text-white capitalize font-medium"
                }
              >
                Exclusive Experience For All
              </h4>
              <p
                className={
                  SmallDevices && SmallDevices
                    ? "text-white font-light text-base leading-normal w-full"
                    : "text-white text-justify text-2xl font-light"
                }
              >
                Insider Interact bridges the gap between fans and their favorite
                known personalities, offering a unique and personalized
                experience for everyone involved.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <div className="float-left w-full">
        <TestimonialCom
          headstyle={`relative mb-5 ${
            SmallDevices ? "w-full" : "w-[60%]"
          } z-0 pt-20 mx-auto`}
          img="../img2.png"
          title={"Alex"}
          role={"Graphic Designer"}
          description="I signed up for Insider Interact on a whim and it turned out to be one of the best decisions I've made. Interacting with people on this platform is always fun and interesting. Not only am I able to earn from my creative work, but I also get to collaborate with other like-minded creators and learn from them.
              "
        />
      </div>

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <div className="cards_system py-9 float-left w-full">
        <div className="container">
          <div className="text-center mb-10 float-left w-full">
            <h3
              className={
                SmallDevices && SmallDevices
                  ? "text-4xl font-light tracking-tight text-white sm:text-6xl"
                  : "text-8xl font-light tracking-tight text-white sm:text-6xl"
              }
            >
              Benefits for <b className="text-[#3F267C]">Creators</b>{" "}
            </h3>
          </div>

          <div
            className={
              SmallDevices && SmallDevices
                ? "flex justify-between items-center w-full flex-col items-clo"
                : "flex justify-between items-center w-full"
            }
          >
            <div
              className={
                SmallDevices && SmallDevices
                  ? "flex justify-between items-center flex-wrap flex-col"
                  : "flex justify-between items-center flex-wrap"
              }
            >
              <div className="card_item">
                <h6 className="text-white text-xl font-medium mb-0">
                  Safe Interactions with Fans
                </h6>
                <p className="text-gray-500 mb-0 font-light text-base">
                  A safe and secure space for meaningful interactions with your
                  fans, followers, or anyone seeking your advice.
                </p>
                <i>
                  <VerifiedUserIcon fontSize="large" />
                </i>
              </div>
              <div className="card_item">
                <h6 className="text-white text-xl font-medium mb-0">
                  Share Your Skills
                </h6>
                <p className="text-gray-500 mb-0 font-light text-base">
                  By offering one-on-one interactions, you can share your
                  lifelong learnings and valuable skills with your fans who are
                  truly looking for an advice.
                </p>
                <i className="-rotate-90">
                  <DoubleArrowIcon fontSize="large" />
                </i>
              </div>

              <div className="card_item">
                <h6 className="text-white text-xl font-medium mb-0">
                  Monetize Your Time
                </h6>
                <p className="text-gray-500 mb-0 font-light text-base">
                  Earn from personalized interactions, through chats, calls, or
                  video calls. Share your knowledge and make a positive impact
                  while earning.
                </p>
                <i>
                  <AttachMoneyIcon fontSize="large" />
                </i>
              </div>
              <div className="card_item">
                <h6 className="text-white text-xl font-medium mb-0">
                  Flexible Schedule and Rates
                </h6>
                <p className="text-gray-500 mb-0 font-light text-base">
                  Have the freedom to set your own preferred time slots and
                  rates. Prioritize your availability and provide the best
                  experience to your fans.
                </p>
                <i>
                  <HourglassTopIcon fontSize="large" />
                </i>
              </div>
            </div>
            <div
              className={
                SmallDevices && SmallDevices
                  ? "flex justify-between items-cente flex-wrap w-full"
                  : "flex justify-between items-cente flex-wrap mr-5 ml-20 w-full"
              }
            >
              <div className="card_item w-full">
                <h6 className="text-white text-xl font-medium mb-0">
                  Complete Convenience
                </h6>
                <p className="text-gray-500 mb-0 font-light text-base">
                  No more sorting through thousands of DMs. We handle everything
                  from bookings to payments, so you can focus on doing what you
                  love!
                </p>
                <i>
                  <SentimentSatisfiedAltIcon fontSize="large" />
                </i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <TestimonialCom
        headstyle={`relative mb-5 ${
          SmallDevices ? "w-full" : "w-[60%]"
        } z-0 pt-20 mx-auto`}
        img="../img3.png"
        title={"Lisa M"}
        role={"Marketing Director"}
        description="As a marketing director, I've always known the power of connection, but Insider Interact takes it to a new level. I've expanded my professional network significantly, and the opportunity to mentor others has been professionally fulfilling beyond words"
      />

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <div className="relative mb-5 float-left w-full z-0 pt-20">
        <div className="container relative">
          <div
            className={
              SmallDevices && SmallDevices
                ? "flex justify-end dosts "
                : "flex justify-center dosts"
            }
          >
            <div
              className={`absolute top-0 bottom-0 left-0 right-0 ${
                SmallDevices && SmallDevices ? "w-1/4" : ""
              }`}
              style={{
                backgroundImage: `url("../sans1.png")`,
                backgroundRepeat: "no-repeat",
              }}
            ></div>

            <div
              className={
                SmallDevices && SmallDevices
                  ? "testimonial_b w-2/3"
                  : ` testimonial_b translate-x-[500px]`
              }
            >
              <div className="heading mb-6 float-left w-full">
                <h4
                  className={
                    SmallDevices && SmallDevices
                      ? `text-4xl font-light tracking-tight text-white sm:text-6xl`
                      : `text-8xl font-light tracking-tight text-white sm:text-6xl`
                  }
                >
                  Benefits for <b className="text-[#3F267C]">Fans</b>
                </h4>
              </div>
              <ul className="list_dot">
                <li>Reach Out Your Favorite Experts</li>
                <li>Real-Time Interactions</li>
                <li>Directly Ask Your Role Models</li>
                <li>Get Insider Knowledge</li>
                <li>Receive Personalized Advice</li>
                <li>No DMs Ignored</li>
                <li>Easy Scheduling & Payments</li>
                <li>Secure Platform</li>
                <li>Wide Range of Experts from Diverse Fields</li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <div className="py-20">
        <div className="container">
          <div className="heading mb-6 float-left w-full text-center">
            <h4
              className={
                SmallDevices && SmallDevices
                  ? "text-4xl font-light tracking-tight text-white"
                  : "text-8xl font-light tracking-tight text-white sm:text-6xl"
              }
            >
              Proud to Flaunt Our{" "}
              <b
                className={`text-[#3F267C] ${
                  SmallDevices ? "text-4xl" : "text-9xl"
                }`}
              >
                1500+
              </b>{" "}
              Happy Users
            </h4>
          </div>

          <div className="center-slider">
            <Slider {...Testimonial}>
              <div className="slide_items flex justify-between items-center flex-col w-full">
                <i className="fa fa-quote-left text-9xl" aria-hidden="true"></i>
                <p>
                  I never thought I could get personalized career coaching from
                  a top executive so easily. Insider Interact has opened doors I
                  didn't even know existed.
                </p>
                <div className="auther_name">
                  <h6
                    className={`text-white font-bold ${
                      SmallDevices ? "text-xl" : "text-4xl"
                    } mb-0`}
                  >
                    -Anita S,
                  </h6>
                  <span
                    className={`text-white font-light text-justify ${
                      SmallDevices ? "text-xl" : "text-2xl"
                    } leading-normal`}
                  >
                    Aspiring Professional
                  </span>
                </div>
              </div>

              <div className="slide_items flex justify-between items-center flex-col w-full">
                <i className="fa fa-quote-left text-9xl" aria-hidden="true"></i>
                <p>
                  I was hesitant to join at first, but after seeing the
                  satisfaction of other creators on Insider Interact, I decided
                  to give it a try. And let me tell you, I have not regretted it
                  one bit. It's been a great opportunity for my career.
                </p>

                <div className="auther_name">
                  <h6
                    className={`text-white font-bold ${
                      SmallDevices ? "text-xl" : "text-4xl"
                    } mb-0`}
                  >
                    -Emily,
                  </h6>
                  <span
                    className={`text-white font-light text-justify ${
                      SmallDevices ? "text-xl" : "text-2xl"
                    } leading-normal`}
                  >
                    Fashion Influencer
                  </span>
                </div>
              </div>
              <div className="slide_items flex justify-between items-center flex-col w-full">
                <i className="fa fa-quote-left text-9xl" aria-hidden="true"></i>
                <p>
                  I've always wanted to get career advice from an entrepreneur I
                  admire. Insider Interact made it possible. I had an engaging
                  conversation with them, and it was so helpful. I’m really
                  thankful that this platform exists! I highly recommend it to
                  anyone looking for meaningful conversations with inspirational
                  people.
                </p>
                <div className="auther_name">
                  <h6
                    className={`text-white font-bold ${
                      SmallDevices ? "text-xl" : "text-4xl"
                    } mb-0`}
                  >
                    -David,
                  </h6>
                  <span
                    className={`text-white font-light text-justify ${
                      SmallDevices ? "text-xl" : "text-2xl"
                    } leading-normal`}
                  >
                    Small Business Owner
                  </span>
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </div>

      <div className="container text-center">
        <div className="line_center"></div>
      </div>

      <div className="newlatter py-20 overlay_bg">
        <div className="container">
          <div className="heading mb-6 float-left w-full text-center">
            <h4
              className={
                SmallDevices && SmallDevices
                  ? "text-4xl font-light tracking-tight text-white"
                  : "text-7xl font-light tracking-tight text-white sm:text-6xl"
              }
            >
              Get <b className="text-[#3F267C]">10% OFF</b> on Your First
              Interaction!
            </h4>
          </div>
          <div className="box_felids">
            <input type="text" name="abc" placeholder="example@info.com" />
            <button
              type="button"
              className="bg-[#3F267C] border rounded-md text-2xl font-medium capitalize text-white px-8"
            >
              Submit
            </button>
          </div>
          <h6 className="text-3xl text-center mt-6 font-light capitalize text-white">
            See You on the Inside!
          </h6>
        </div>
      </div>

      {/* <Banner/>
        <TeamBack open={open} setOpen={setOpen}/>
        <POPUP open={open} setOpen={setOpen}/> */}
      {/* <ChatSystem/> */}
    </div>
  );
}

export default Home;
