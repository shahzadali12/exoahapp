import { getSession, useSession } from "next-auth/react";
import Login from "../components/auth/login";
import React, { useEffect } from "react";
import { useRouter } from "next/router";

function LoginPage() {

  const {data:session} = useSession();

  
  const router = useRouter()
  useEffect(()=>{
    if(session?.user?.email){
      router.replace("/dashboard")
    }
  },[session])

  return <Login />;

}

export default LoginPage;



// export async function getServerSideProps({req,res}) {
//   const session = await getSession({req});

//   if (session) {
//     return {
//       redirect: {
//         destination: process.env.NEXTAUTH_URL + '/dashboard',
//         permanent: false,
//       },
//     };
//   }
//   return { props: {session} };
// }