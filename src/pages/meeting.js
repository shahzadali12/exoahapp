import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useSession,getSession } from "next-auth/react";
import { Store } from "../store/AppStore";
function Meeting() {
  const [token, setTokens] = useState([]);

  const {userdata} = useContext(Store)

  const {data} = useSession()
  var authEndpoint = "http://localhost:3000/api/signature";
  var sdkKey = process.env.ClientId;
  var role = 0;
  var userName = data?.user?.name;
  var userEmail = data?.user?.email;
  var registrantToken = "";
  var zakToken = "";
  var leaveUrl = "http://localhost:3000";


useEffect(() => {
    const storedValue = localStorage.getItem("meeting");
    const token = localStorage.getItem("token");
    //   setTokens({meeting:JSON.parse(storedValue), token:JSON.parse(token)})
    const TokData = JSON.parse(token);
    const mResponce = JSON.parse(storedValue);
    const payload = {
      meetingNumber: mResponce?.data?.id,
      role: userdata?.role,
      passWord: mResponce?.data?.password,
    };
    
    return async () => {
      new Promise(async (resolve, reject) => {
        const ZoomEmbed = await (
          await import("@zoomus/websdk/embedded")
        ).default;

        resolve(ZoomEmbed.createClient());
      }).then(async (client) => {
          let meetingSDKElement = document.getElementById("meetingSDKElement");
          
          client.init({
            language: "en-US",
            leaveUrl: 'google.com',
            isSupportAV: true,
            zoomAppRoot: meetingSDKElement,
            customize: {
              video: {
                viewSizes: {
                  default: {
                    height: 400,
                    width: 400,
                  },
                  ribbon: {
                    width: 500,
                  },
                },
              },
              // chat: {
              //   popper: {
              //     placement: "right",
              //   }
              // }
            },
          });
          const { data } = await axios({
            url: "/api/signature",
            method: "post",
            data: {
              meetingNumber: mResponce?.data?.id,
              role: userdata?.role,
              passWord: mResponce?.data?.password,
            },
          })
            .then((response) => {
              return response;
            })
            .catch((error) => {
              console.log(error);
            });

         await client.join({
            signature: data.signature,
            sdkKey: sdkKey,
            meetingNumber: mResponce?.data?.id,
            password: mResponce?.data?.password,
            userName: userName,
            userEmail: userEmail,
            tk: registrantToken,
            zak: zakToken
          });
          
         

        })
        .catch((err) => {
          console.log(err);
        });
    };
  }, []);
 
  return (
    <>
      <div className="flex justify-center items-center ">
        <div id="meetingSDKElement" className="w-full h-screen">
        </div>
      </div>
     
    </>
  );
}

export async function getServerSideProps({ req }) {
    const session = await getSession({ req });
    if (session?.user) {
      var { id, role } = session?.user;
    }
    if (!session) {
      return {
        redirect: {
          destination: `/login`,
          permanent: false,
        },
      };
    }
  
  
    return { props: {session} };
  }

export default Meeting;
