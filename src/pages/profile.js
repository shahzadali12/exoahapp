import React, { useContext, useState, useEffect } from "react";

// material-ui
import { styled, useTheme } from "@mui/material/styles";
import { Box, Grid, Tab, Tabs, Typography } from "@mui/material";

import PersonOutlineTwoToneIcon from "@mui/icons-material/PersonOutlineTwoTone";
import CreditCardTwoToneIcon from "@mui/icons-material/CreditCardTwoTone";
import VpnKeyTwoToneIcon from "@mui/icons-material/VpnKeyTwoTone";
import EditInformation from "../components/profile/edit";
import PaymentWay from "../components/profile/payment";
import ChangePasswor from "../components/profile/changePassword";
import axios from "axios";
import UpdateProfile from "../components/wegets/profile";
import { getSession, useSession } from "next-auth/react";
import { Store } from "../store/AppStore";
import { useRouter } from "next/router";


// tabs
function TabPanel({ children, value, index, ...other }) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
}


const tabsOption = [
  {
    label: "User Profile",
    icon: <PersonOutlineTwoToneIcon />,
    caption: "Profile Settings",
  },

  {
    label: "Payment",
    icon: <CreditCardTwoToneIcon />,
    caption: "Add & Update Card",
  },
  {
    label: "Change Password",
    icon: <VpnKeyTwoToneIcon />,
    caption: "Profile Security",
  },
];

export const StyledTab = styled((props) => <Tab {...props} />)(
  ({ theme, border, value, cart }) => ({
    color: "#000",
    minHeight: "auto",
    minWidth: 250,
    padding: 16,
    borderRadius: `${border}px`,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    textAlign: "left",
    justifyContent: "flex-start",
    "&:after": {
      backgroundColor: "transparent !important",
    },
    "&.Mui-selected": {
      color: theme.palette.primary.main,
      background:
        theme.palette.mode === "dark"
          ? theme.palette.dark.main
          : theme.palette.grey[50],
    },
    "& > svg": {
      marginBottom: "0px !important",
      marginRight: 10,
      marginTop: 2,
      height: 20,
      width: 20,
    },
    [theme.breakpoints.down("md")]: {
      minWidth: "100%",
    },
  })
);

const ProfileP = () => {
  const {data:session} = useSession()
  const {userdata} = useContext(Store)

  
  const router = useRouter()
  useEffect(()=>{
    if(!session?.user?.email){
      router.replace("/login")
    }
  },[session])

  const theme = useTheme();
  // const { borderRadius } = useConfig();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const otherOptions = userdata?.info?.role && tabsOption.map((vak)=> vak).filter((vl)=> userdata?.info?.role == "user" ? vl.label != "Payment" : vl)



  return (
    <>
      <div className="row">
        <div className="mb-4 col-md-12">
          <div className="page-header">
            <div className="page-block">
              <div className="row align-items-center">
                <div className="col-md-12">
                  <div className="page-header-title">
                    <h5 className="m-b-10">Edit Profile</h5>
                  </div>
                  <ul className="breadcrumb">
                    <li className="breadcrumb-item">
                      <a href="/">Home</a>
                    </li>
                    <li className="breadcrumb-item">
                      <a href="/dashboard">Dashboard</a>
                    </li>
                    <li className="breadcrumb-item" aria-current="page">
                      Profile
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Grid bgcolor={"#fff"} rowSpacing={4} container spacing={3}> */}
      <div className="row px-4 ">
        <div className="col-md-12 bg-white rounded-md py-4">
          <div className="row">
            <div xs={12} lg={4} className="col-md-3 ">
              <Box
                sx={{
                  flexGrow: 1,
                  bgcolor: "background.paper",
                  display: "flex",
                  justifyContent: "center",
                  flexDirection: "column",
                  // height: 350,
                  width: "100%",
                }}
              >
                <Tabs
                  orientation="vertical"
                  variant="scrollable"
                  value={value}
                  onChange={handleChange}
                  aria-label="Vertical tabs example"
                  sx={{
                    "& .MuiTabs-flexContainer": {
                      borderBottom: "none",
                    },
                    "& .MuiTabs-indicator": {
                      display: "none",
                    },
                    "& .MuiButtonBase-root + .MuiButtonBase-root": {
                      position: "relative",
                      overflow: "visible",
                      ml: 0,
                      "&:after": {
                        content: '""',
                        bgcolor: "#ccc",
                        width: 1,
                        height: "calc(100% - 16px)",
                        position: "absolute",
                        top: 8,
                        left: -8,
                      },
                    },
                  }}
                >
               
                  {otherOptions && otherOptions.map((tab, index) => (
                    
                    
                     <StyledTab
                    theme={theme}
                    // border={"1px solid " + (theme.palette.mode === 'dark'? "grey.5" : "")}
                    value={index}
                    // cart={cart}
                    // disabled={index > cart.checkout.step}
                    key={index}
                    icon={tab.icon}
                    label={
                      <Grid container direction="column">
                        <Typography
                          variant="subtitle1"
                          color="inherit"
                          className="capitalize text-md font-bold py-0 leading-none rounded-md mb-1"
                        >
                          {tab.label}
                        </Typography>
                        <Typography
                          component="div"
                          variant="caption"
                          className="leading-none text-sm mb-0"
                          sx={{ textTransform: "capitalize" }}
                        >
                          {tab.caption}
                        </Typography>
                      </Grid>
                    }
                  />

                    
                    
                  ))}
                </Tabs>
              </Box>
            </div>

            <div item xs={12} lg={8} className="col-md-9 ">
              
              <TabPanel value={value} index={0}>
                <EditInformation />
              </TabPanel>

             {session?.user.role != "user" && <TabPanel value={value} index={1}>
                <PaymentWay />
              </TabPanel>}

              <TabPanel value={value} index={session?.user.role != "user" ? 2 : 1}>
                <ChangePasswor />
              </TabPanel>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

export default ProfileP;



// export async function getServerSideProps({req,res}) {
//   const session = await getSession({req});
  

//   if (!session) {
//     return {
//       redirect: {
//         destination: process.env.NEXTAUTH_URL + '/login',
//         permanent: false,
//       },
//     };
//   }
//   return { props: {session} };
// }
