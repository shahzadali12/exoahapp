import axios from "axios";
import moment from "moment";
import { useSession, getSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useContext, useEffect, useMemo, useState } from "react";

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import CreateAmeeting from "../components/createAmeeting";
import { Store } from "../store/AppStore";

function Rooms() {
  const {userdata} = useContext(Store)
  const [responce, setMeeting] = useState([]);
  const [updateValue,setUpdate] = useState([]);
  const {data} = useSession()
  const PostApi = async () => {
    const token = { token: userdata?.zoom };

    
   
      

    const postApi = await fetch("/api/zoom/list", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
      body: JSON.stringify(token),
    }).then((result)=> result.json()).then((respocs)=>{
      setMeeting(respocs);
    }).catch((error)=>{
      console.log(error)
    });

    // const respocs = await postApi.json();
    // console.log(postApi)
    
    // if(postApi.status){
    //   const respocs = await postApi?.json();
    //   console.log(respocs)
        
    // } 
  };

  useEffect(() => {
    if(userdata?.zoom){
        PostApi();
    }
  }, [userdata,updateValue]);

  const [toggle, setToggle] = useState(false);

  const route = useRouter();
  const JoinHandler = async (data, index) => {
    if (data.id) {
      const datasa = {
        id: data?.id && data?.id,
        token: userdata?.zoom,
      };
      const response = data?.id && (await axios.post("/api/conduct", datasa));
      setToggle(true);
      if (typeof window !== "undefined" && window.localStorage) {
        // Set the new value in localStorage
        localStorage.setItem("meeting", JSON.stringify(response));
        localStorage.setItem("token", JSON.stringify(userdata?.zoom));
      }
      route.push("/meeting");
    } else {
      setToggle(false);
    }
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
    {/* <div className=""><button className="bg-sky-600 text-white py-2 px-3 rounded-md mb-4" onClick={handleShow}>Create a meeting</button></div>

   
    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create A Meeting</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <CreateAmeeting setShow={setShow} setUpdate={setUpdate} access_token={userdata?.zoom}/>
        </Modal.Body>
        
      </Modal> */}

      <div className="flex justify-between  flex-wrap">
        {responce?.meetings?.map((d, index) => {
          return (
            <div
              key={index}
              className={`flex flex-col w-[32%] rounded-md  mr-4 mb-3 px-4 py-3 relative ${moment(new Date()) > moment(d.start_time) ? "bg-yellow-300" : "bg-slate-300" }`}
            >
              <h3>{d.topic}</h3>

              <span>{moment(d.start_time).format("DD-MM-YY hh:mm A")}</span>
              <span>Meeting ID: {d.id}</span>
              <span>Duration: {d.duration}</span>

              {moment(new Date()) > moment(d.start_time) ? null : <div className="absolute right-4 top-full -mt-20">
                <button
                  type="button"
                  onClick={() => JoinHandler(d, index)}
                  className="bg-sky-600 text-white py-2 px-3 rounded-md"
                >
                  Join
                </button>
              </div> }
            </div>
          );
        })}
        
      </div>
      
    </>
  );
}

export default Rooms;

export async function getServerSideProps({req,res}) {
  const session = await getSession({req});
  

  if (!session) {
    return {
      redirect: {
        destination: process.env.NEXTAUTH_URL + '/login',
        permanent: false,
      },
    };
  }
  return { props: {session} };
}

