import Register from '../components/auth/register'
import React,{useEffect} from 'react'
import { getSession, useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
export default function RegisterPage() {
  const data = useSession()
  const router = useRouter()
  useEffect(()=>{
    if(data?.user?.email){
      router.replace("/dashboard")
    }
  },[data])

  return (
    <Register/>
  )
}


  // export async function getServerSideProps({req,res}) {
  //   const session = await getSession({req});
  
  //   if (session) {
  //     return {
  //       redirect: {
  //         destination: process.env.NEXTAUTH_URL + '/dashboard',
  //         permanent: false,
  //       },
  //     };
  //   }
  //   return { props: {session} };
  // }