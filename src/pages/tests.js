// import React, { useEffect, useState } from 'react';
// import { useSpeechSynthesis } from 'react-speech-recognition';
// import axios from 'axios';

// const GOOGLE_TRANSLATE_API_KEY = 'AIzaSyC6kGIAi_0uxSAIZP-9ld1mOd9CoHiUcdM';
// const sourceLanguage = 'en';
// const targetLanguage = 'es';

// const VoiceTranslator = () => {
//   const [text, setText] = useState('');
//   const [translation, setTranslation] = useState('');
//   const speechSynthesis = useSpeechSynthesis();

//   const fetchTranslation = async (text) => {
//     const response = await axios.get(
//       `https://translation.googleapis.com/language/translate/v2`,
//       {
//         params: {
//           key: GOOGLE_TRANSLATE_API_KEY,
//           source: sourceLanguage,
//           target: targetLanguage,
//           q: text
//         }
//       }
//     );
//     setTranslation(response.data.data.translations[0].translatedText);
//     speechSynthesis.speak(new SpeechSynthesisUtterance(response.data.data.translations[0].translatedText));
//   };

//   useEffect(() => {
//     const recognition = new window.SpeechRecognition();
//     recognition.lang = sourceLanguage;
//     recognition.start();

//     recognition.addEventListener('result', (event) => {
//       if (event.results[0].isFinal) {
//         const text = event.results[0][0].transcript;
//         setText(text);
//         fetchTranslation(text);
//       }
//     });

//     return () => {
//       recognition.stop();
//     };
//   }, []);

//   return (
//     <div>
//       <p>{text}</p>
//       <p>{translation}</p>
//     </div>
//   );
// };

// export default VoiceTranslator;



// // export async function getServerSideProps() {
// //   const translate = new Translate();
// //   const response = await translate.translate(text, {
// //     from: 'en',
// //     to: 'es',
// //   });

// //   return {
// //     props: {
// //       translation: response[0]
// //     }
// //   };
// // }


import axios from 'axios';
import React, { useEffect, useMemo, useState } from 'react';
import { useSpeechRecognition } from 'react-speech-kit';

function Example() {
  const [value, setValue] = useState('');
  const { listen, listening, stop } = useSpeechRecognition({
    onResult: (result) => {
      setValue(result);
      Jojo(result)
    },
  });

  const sourceLanguage = 'en';
  const targetLanguage = 'ur';

  async function Jojo(aa) {
    await axios.get(
      `https://translation.googleapis.com/language/translate/v2`,
      {
        params: {
          key: 'AIzaSyDMV6UuIZrXFGDdTbz7k5A5jhS04JuM9aY',
          source: sourceLanguage,
          target: targetLanguage,
          q: aa
        }
      }
    ).then((as)=> console.log(as)).catch((err)=> console.log(err));
  }

  // useMemo(()=>{
  //   Jojo(value)
  // },[value])
 
  return (
    <div>
      <textarea
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <button onMouseDown={listen} onMouseUp={stop}>
        🎤
      </button>
      {listening && <div>Go ahead I'm listening</div>}
    </div>
  );
}

export default Example