import React, { useEffect, useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import RateReviewOutlinedIcon from '@mui/icons-material/RateReviewOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
// import CateForm from "../components/Cateform";
import Offcanvas from "react-bootstrap/Offcanvas";
import axios from "axios";
import { toast } from "react-toastify";
import { getSession } from "next-auth/react";


function Users() {
  const [showoverlay, setshowOverlay] = useState(false);
  const [rawdata, setrawDate] = useState([]);
  const [Rowdata, setRowdata] = useState([]);
  const handleClose = () => setshowOverlay(false);
  const handleShow = () => {
    setshowOverlay(true)
    setRowdata([])
  };
 
  function createMarkup(c) {
    return { __html: c };
  }
  function RemoveTags(tags) {
    var htmlRegexG = /<(?:"[^"]*"['"]*|'[^']*'['"]*|[^'">])+>/g;
    return tags.replace(htmlRegexG, "");
  }

  const [Refresh, setRefresh] = useState(false);
  const columnDefs = [
    {
      field: "name",
      headerName:"name",
      filter: "agNumberColumnFilter",
      suppressMenu: true,
      suppressSizeToFit: true,
      minWidth: 120,
      maxWidth: 120,

    },
    {
      field: "email",
      headerName:"email",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      // minWidth: 70,
      suppressSizeToFit: true,
      minWidth: 300,
      maxWidth: 300,
    },
    {
      field: "password",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      // minWidth: 70,
      suppressSizeToFit: true,
      minWidth: 250,
      maxWidth: 250,
    },
    {
        field: "role",
        filter: "agTextColumnFilter",
        suppressMenu: true,
        // minWidth: 70,
        suppressSizeToFit: true,
        minWidth: 250,
        maxWidth: 250,
      },
    // {
    //   field: "icon",
    //   filter: "agTextColumnFilter",
    //   suppressMenu: true,
    //   // minWidth: 70,
    //   suppressSizeToFit: true,
    //   minWidth: 200,
    //   width: 200,
    //   cellRenderer: (params) => process.env.NODE_ENV === "development" ? <img className="w-14" src={`${process.env.REACT_APP_BaseURL}/extraimages/listing/${params.value}`} alt="img here"/> : <img src={`${process.env.REACT_APP_BaseURL}/public/extraimages/listing/${params.value}`} alt="logo here" />
    // },
    
    // {
    //   field: "status",
    //   filter: "agNumberColumnFilter",
    //   suppressMenu: true,
    //   // minWidth: 70,
    //   suppressSizeToFit: true,
    //   minWidth: 120,
    //   width: 120,
    // },
    {
      field: "Actions",
      filter: "agTextColumnFilter",
      suppressMenu: true,
      minWidth: 200,
      maxWidth: 200,
      suppressSizeToFit: true,
      cellRenderer: (params) => HandleAction(params),
    },
  ];

  const HandleAction = (params) => {

   
    const Edit = () => {
        setshowOverlay(true)
        setRowdata(params.data)
    }
    const Remove = async () => {
       const {id} = params.data
      const data = {
        id: id
      }

      await axios.delete(`${process.env.REACT_APP_BaseURL}/api/categories`, {data}).then((val)=>{
        toast.success("successfully Remove")
        setRefresh(val)
      });
    }
   
    return <>
      <button onClick={Edit} className="px-1 py-1 bg-yellow-500 inline-flex rounded-md mr-2 text-white text-sm"><RateReviewOutlinedIcon fontSize="small"/></button>
      <button onClick={Remove} className="px-1 py-1 bg-red-500 inline-flex rounded-md mr-2 text-white text-sm"><DeleteOutlinedIcon fontSize="small"/></button>
    </>
  }

  const defaultColDef = useMemo(() => {
    return {
      editable: false,
      sortable: true,
      resizable: true,
      filter: true,
      floatingFilter: true,
      flex: 1,
      width: 100,
      minWidth: 100,
    };
  }, []);

  const listingData = async () => {
    const api = await fetch('/api/allusers');
    const res = await api.json()
    setrawDate(res);
  }

  useEffect(() => {
    listingData();
      // return (()=>{
      //   listingData();
      // })
  }, [Refresh])
  return (
    <>
    <div className="p-4 grid grid-cols-12">
        <div className="column col-span-12 lg:col-span-8 mb-4">
            <h3>Manage Users</h3>
          </div>
          {/* <div className="col-span-12 lg:col-span-4">
            <div className="text-lg-end">
                <button className="btn-normal btn-green min-w-[10%] py-[10px] text-white" onClick={handleShow}>Add User</button>
            </div>
          </div> */}
      
      <Offcanvas placement="end" className='w-75' show={showoverlay} onHide={handleClose}>
        
          <Offcanvas.Header closeButton>
          </Offcanvas.Header>
          <Offcanvas.Body>
            {/* <CateForm setshowOverlay={setshowOverlay} Rowdata={Rowdata && Rowdata} setRefresh={setRefresh}/> */}
          </Offcanvas.Body>
        </Offcanvas>

        <div className="col-span-12 h-screen">
          <AgGridReact
            className="ag-theme-alpine h-screen"
            columnDefs={columnDefs}
            defaultColDef={defaultColDef}
            rowData={rawdata}
            alwaysShowHorizontalScroll={true}
            alwaysShowVerticalScroll={true}
            animateRows={true}
          />
        </div>
      </div>
    </>
  )
}
export async function getServerSideProps({req,res}) {
  const session = await getSession({req});
  

  if (!session) {
    return {
      redirect: {
        destination: process.env.NEXTAUTH_URL + '/login',
        permanent: false,
      },
    };
  }
  return { props: {session} };
}

export default Users