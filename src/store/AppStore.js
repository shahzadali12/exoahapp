import { useState, useEffect, createContext, useRef, useMemo } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import { useSession } from "next-auth/react";
import { getCookie, isTokenExpired, setCookie } from "../auth/token";
import InstanceApi from "../auth/fetchData";
export const Store = createContext(null);

function AppStore({ children }) {
  const { data } = useSession();
  const [Apiload, setApiload] = useState([]);
  const [userdata, setUser] = useState();
  const [show, setShow] = useState([]);
  const [datas, setData] = useState([]);
  const [Selected_creator, setSelected_creator] = useState([]);
  const [paymentSuccess, setPaymentuccess] = useState(false);
  const [message, setMessage] = useState("");
  const [messages, setmessages] = useState([]);
  const [logins, setlogins] = useState(false);
  const [indexing, setindexing] = useState({});
  const [paymentSlot, setpaymentSlot] = useState();
  const [slotbooked, setslotbooked] = useState(false);
  const [states, setStates] = useState({
    style: "menu",
    menuStatus: "close",
    avaliable: false,
    data: [],
  });

  async function Tokens() {
    // const gettoken = await getTokenFromLocal("ghq");

    const gettoken = await getCookie("ghq");
    const decoded = await isTokenExpired(gettoken);

    if (decoded) {
      const token = await InstanceApi(`api/middleware/control`,null,"get",null);

      setCookie("ghq", token, 1);
      // saveTokenToLocal(token);

      return await token;
    } else {
      return await gettoken;
    }
  }

  useEffect(() => {
    Tokens();
  }, []);


  const filterMenu = states?.data?.availability?.times && states?.data?.availability?.times.filter((val) => val?.checked);

  const [filtering, setFilters] = useState([]);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const [cnt, setCnt] = useState(0);
  const [prices, setslots] = useState(0);
  const [looked, setlooked] = useState([]);

  useEffect(() => {
    if (filterMenu?.length > 0) {
      setFilters(filterMenu);
    }
  }, [states?.data?.availability?.times]);

  const GetUser = async () => {
    const gettoken = await Tokens();
    const reponce = data?.user?.email && await InstanceApi(`/api/authenticated/users/${data?.user?.email}`,gettoken,"get",null);
    
    // const userid =
    //   data?.user?.email &&
    //   (await fetch(`/api/authenticated/users/${data?.user?.email}`));
    // const reponce = await userid.json();
    
    if (reponce?.info?.email) {
      await setUser(reponce);
      setShow({
        ...show,
        about: reponce?.info?.about ? false : true,
        language: reponce?.info?.languages ? false : true,
        name: reponce?.info?.name ? false : true,
        phone: reponce?.info?.phone ? false : true,
        designation: reponce?.info?.designation ? false : true,
        shortinfo: reponce?.info?.shortinfo ? false : true,
        profilePhoto: reponce?.info?.profilePhoto ? false : true,
        role: reponce?.info?.role ? false : true,
      });
    }
  };

  useMemo(() => {
    if (data?.user?.email) {
      GetUser();
    }
  }, [data?.user?.email, Apiload]);

  const [Selection, setSelection] = useState([]);

  const dataa = [
    {
      id: 100,
      name: "All",
    },
    {
      id: 1,
      name: "Fitness",
    },
    {
      id: 3,
      name: "Health",
    },
    {
      id: 4,
      name: "Crypto",
    },
    {
      id: 5,
      name: "Nutrition",
    },
    {
      id: 6,
      name: "Career",
    },
    {
      id: 7,
      name: "Travel",
    },
    {
      id: 8,
      name: "Motivation",
    },
    {
      id: 9,
      name: "Investment",
    },
    {
      id: 10,
      name: "All Creators",
    },
  ];
  
  const [category, setCategories] = useState([]);
  
  const contentArea = [
    {
      id: 1,
      name: "rachel pantano",
      category: "Health",
      dis: "Massage Therapist & Mobility Coach",
      img: "../creators/img1.png",
    },
    {
      id: 2,
      name: "Alice Liveing",
      category: "Fitness",
      dis: "Fitness Expert & Personal Trainer",
      img: "../creators/img2.png",
    },
    {
      id: 3,
      name: "Dr Joshua Wolrich",
      category: "Crypto",
      dis: "NHS Doctor & Nutritionist",
      img: "../creators/img3.png",
    },
    {
      id: 4,
      name: "Dr Julie Woodward",
      category: "Fitness",
      dis: "Oculofacial Surgeon & Professor",
      img: "../creators/img5.png",
    },
    {
      id: 5,
      name: "Damon Dominique",
      category: "Fitness",
      dis: "Video Creator",
      img: "../creators/img6.png",
    },
    {
      id: 6,
      name: "Asdghik Ashley Melkonian",
      category: "Travel",
      dis: "Traveler & Blogger",
      img: "../creators/img7.png",
    },
    {
      id: 7,
      name: "James Altucher",
      category: "Fitness",
      dis: "Investor & Entrepreneur",
      img: "../creators/img8.png",
    },
    {
      id: 8,
      name: "David Dang",
      category: "Fitness",
      dis: "Entrepreneur",
      img: "../creators/img9.png",
    },
    {
      id: 9,
      name: "Benjamin Cowen",
      category: "Fitness",
      dis: "CEO & Crypto Expert",
      img: "../creators/img10.png",
    },
    {
      id: 10,
      name: "DABITO",
      category: "Fitness",
      dis: "Author, Designer, & Artist",
      img: "../creators/img11.png",
    },
    {
      id: 11,
      name: "Dr. Ali Mattu",
      category: "Fitness",
      dis: "Clinical Psychologist",
      img: "../creators/img12.png",
    },
    {
      id: 12,
      name: "Erin Harding",
      category: "Fitness",
      dis: "Author & Home Décor Expert",
      img: "../creators/img13.png",
    },
    {
      id: 13,
      name: "Kyle Scheele",
      category: "Fitness",
      dis: "Motivational Speaker",
      img: "../creators/img14.png",
    },
    {
      id: 14,
      name: "Doug Bopst",
      category: "Fitness",
      dis: "Author, Personal Trainer, & Consultant",
      img: "../creators/img15.png",
    },
    {
      id: 15,
      name: "Jenna Hope",
      category: "Fitness",
      dis: "Nutritionist & Consultant",
      img: "../creators/img16.png",
    },
    {
      id: 16,
      name: "Clarissa",
      category: "Fitness",
      dis: "IBS Nutritionist",
      img: "../creators/img17.png",
    },
  ];

  const ApiEndpints = async (api) => {
    const responce = await axios
      .get(`/api/${api}`)
      .then((response) => response?.data)
      .catch((error) => console.log(error));
    setData(responce);
  };

  useEffect(() => {
    ApiEndpints("creators");
  }, []);

  const options = dataa.map((vel) => {
    const name = {
      label: vel.name,
      value: vel.id,
    };

    return name;
  });

  const filteredData =
    Selection &&
    datas
      .map((val) => val)
      .filter((vel) =>
        vel?.info?.category?.length > 0 ? vel?.info?.category : null
      );

  const filterByColor = (datasda, label) => {
    const filteredData = [];

    datasda.forEach((categorya) => {
      const filteredItems = categorya?.info?.category?.filter(
        (item) => item.label == label
      );

      if (filteredItems.length > 0) {
        filteredData.push(categorya);
      }
    });

    return filteredData;
  };

  function getUnique(array, key) {
    if (typeof key !== "function") {
      const property = key;
      key = function (item) {
        return item[property];
      };
    }
    return Array.from(
      array
        .reduce(function (map, item) {
          const k = key(item);
          if (!map.has(k)) map.set(k, item);
          return map;
        }, new Map())
        .values()
    );
  }
  const uniqueObjectsMap = new Map();
  const avaliable = filteredData.map((val) => val.info.category);
  // Flatten the nested array
  const flatArray = avaliable.flat();

  flatArray.forEach((obj) => {
    uniqueObjectsMap.set(obj.value, obj);
  });
  const uniqueObjects = [...uniqueObjectsMap.values()];

  const filtered =
    Selection?.label === "All"
      ? filteredData
      : Selection?.label
      ? filterByColor(filteredData, Selection?.label)
      : filteredData;

  const getAllCategories = async () => {
    const categories = await fetch(`/api/categories`);
    const allCategories = await categories.json();
    const options =
      allCategories &&
      allCategories.map((vel) => {
        const name = {
          label: vel.label,
          value: vel._id,
        };

        return name;
      });
      
    setCategories(options);
  };

  useEffect(() => {
    getAllCategories();
  }, []);

  const Timeoptions = [
    {
      value: 1,
      label: 5,
    },
    {
      value: 2,
      label: 15,
    },
    {
      value: 3,
      label: 20,
    },
    {
      value: 4,
      label: 25,
    },
    {
      value: 5,
      label: 30,
    },
  ];

  function findDuplicates(arr1, arr2) {
    const duplicates = [];

    // Iterate over each element in the first array
    for (let i = 0; i < arr1.length; i++) {
      const value = arr1[i];

      // Check if the element exists in the second array
      if (arr2 && arr2.includes(value)) {
        // If it does, add it to the duplicates array
        duplicates.push(value);
      }
    }

    return duplicates;
  }

  const uniqueValue =
    userdata?.messages &&
    Selected_creator?.messages &&
    findDuplicates(userdata?.messages, Selected_creator?.messages);

  const PaymentDetection = async () => {
    await axios
      .post(`/api/chat/chat`, {
        id: userdata?._id,
        receiver: Selected_creator?._id,
        message: message,
        date: new Date(),
        chatid: uniqueValue[0] && uniqueValue[0],
      })
      .then(async (resp) => {
        if (resp.status == 201) {
          setMessage("");
          if (uniqueValue[0]) {
            await axios
              .get(`/api/chat/${uniqueValue[0]}`)
              .then((rp) => {
                setmessages(rp.data);
              })
              .catch((error) => {
                toast.error(error);
              });

            //   setUpdateChat(true);
            // toast.success(`successfully`);
          }
        }
      })
      .catch((error) => {
        toast.error(error);
      });
  };

  const ProcessingFunction = async () => {
    const meeting = [];
    await axios.post(`${process.env.NEXTAUTH_URL}/api/zoom/create`, {
        token: states?.data?.zoom,
        topic: "Scheduled Meeting",
        duration: parseInt(indexing?.sslots),
        start_time: cnt,
      }).then((val) => {
        setlooked(val?.data);
        meeting.push(val?.data);
      });

    if (filtering?.length > 0) {
      const dataa = {
        currentuser: {
          email: userdata?.info?.email,
          meeting: {
            name: userdata?.info?.name,
            profilePhoto: userdata?.info?.profilePhoto,
            duration: indexing?.sslots,
            date: cnt,
            start: meeting[0]?.start,
            join: meeting[0]?.join,
            password: meeting[0]?.password,
            timezone: meeting[0]?.timezone,
            status: meeting[0]?.status,
            meetingid: meeting[0]?.id,
          },
        },

        info: {
          id: Selected_creator?._id,
        },
        availability: {
          break: states?.data?.availability.break,
          slotDuration: states?.data?.availability.slotDuration,
          times: filtering,
        },
      };

      await axios
        .put(`${process.env.NEXTAUTH_URL}/api/authenticated/setmeeting`, dataa)
        .then((val) => {
          if (val.status == 201) {
            //  toast.success("successfully your slot booked!");
            setslotbooked(true);
            //   if(session?.user.role !== "user"){
            //     route.push("/connect")
            //   setnofication(true)

            //   setTimeout(() => {
            setStates({ ...states, menuStatus: "close" });
            //   }, 2000);
          }
        })
        .catch((err) => {
          console.error(err);
        });
    }

  };

  return (
    <Store.Provider
      value={{
        userdata,
        show,
        setShow,
        Apiload,
        setApiload,
        Selection,
        setSelection,
        options,
        dataa,
        contentArea,
        filtered,
        Timeoptions,
        category,
        uniqueObjects,
        paymentSuccess,
        setPaymentuccess,
        Selected_creator,
        setSelected_creator,
        PaymentDetection,
        message,
        setMessage,
        findDuplicates,
        messages,
        setmessages,
        logins,
        setlogins,
        indexing,
        setindexing,
        slotbooked,
        setslotbooked,
        paymentSlot,
        setpaymentSlot,

        selectedTabIndex,
        setSelectedTabIndex,
        cnt,
        setCnt,
        prices,
        setslots,
        looked,
        setlooked,
        states,
        setStates,
        filterMenu,
        filtering,
        setFilters,
        ProcessingFunction,
      }}
    >
      <ToastContainer />
      {children}
    </Store.Provider>
  );
}

export default AppStore;
